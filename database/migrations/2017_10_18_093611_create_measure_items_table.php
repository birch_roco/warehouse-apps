<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeasureItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measure_items', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('measurement_id')->unsigned();
            $table->string('groupName', 70);
            $table->string('productType', 50);
            $table->string('productCode', 50);
            $table->string('productSize', 50);
            $table->integer('orderNo');
            $table->string('supplierName', 70);
            $table->double('length')->nullable();
            $table->double('width')->nullable();
            $table->double('height')->nullable();
            $table->double('weight')->nullable();
            $table->string('userID', 50)->nullable();
            $table->dateTime('last_measured_date')->nullable();

            $table->foreign('measurement_id')->references('id')->on('measurements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measure_items');
    }
}
