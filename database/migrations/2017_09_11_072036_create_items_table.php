<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->bigInteger('count_id')->unsigned();
            $table->string('linnworksID', 70);
            $table->string('productSKU', 70);
            $table->string('productCode', 70);
            $table->string('itemType', 70);
            $table->string('itemCode', 70);
            $table->string('itemColour', 70);
            $table->integer('orderNo');
            $table->integer('expected');
            $table->integer('count')->nullable();
            $table->dateTime('bookedIn')->nullable();
            $table->string('userID', 50)->nullable();
            $table->dateTime('last_count_date')->nullable();

            $table->foreign('count_id')->references('id')->on('counts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
