<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Count extends Model
{
    protected $table = 'counts';

    public function items() {
    	return $this->hasMany('App\Item');
    }
}