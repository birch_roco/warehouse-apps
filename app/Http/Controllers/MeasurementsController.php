<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Measurement;
use App\MeasureItem;
use Carbon\Carbon;

class MeasurementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $items = $request->items;

        // dd($items);
        $measurement = new Measurement;
        $measurement->userID = \Auth::user()->name;
        $measurement->description = $request->desciption;
        $measurement->save();

        $measurementId = $measurement->id;

        foreach ($items as $item) {
            $newItem = new MeasureItem;
            $newItem->measurement_id = $measurementId;
            $newItem->groupName = $item['groupName'];
            $newItem->productType = $item['productType'];
            $newItem->productCode = $item['productCode'];
            $newItem->productSize = $item['productSize'];
            $newItem->orderNo = $item['orderNo'];
            $newItem->supplierName = $item['supplierName'];
            $newItem->last_measured_date = $item['lastMeasured'];
            $newItem->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $measureItems = MeasureItem::where('measurement_id', $id)
                    ->orderBy('productType', 'asc')
                    ->orderBy('productCode', 'asc')
                    ->orderBy('orderNo', 'asc')
                    ->get();
        return $measureItems;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function updateGroup(Request $request) {
        $group = Measurement::find($request->id);
        $group->complete = true;
        $group->save();
    }

    public function updateItems(Request $request)
    {
        $items = $request->itemData;
        
        foreach ($items as $item) {
            $updateItem = MeasureItem::find($item['itemId']);
            
            if(isset($item['Height'])) {
                $updateItem->height = $item['Height'];
            }

            if(isset($item['Width'])) {
                $updateItem->width = $item['Width'];
            }

            if(isset($item['Length'])) {
                $updateItem->length = $item['Length'];
            }

            if(isset($item['Weight'])) {
                $updateItem->weight = $item['Weight'];
            }

            $updateItem->userID = \Auth::user()->name;
            $updateItem->last_measured_date = Carbon::now();

            $updateItem->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $name = $request->name;
        $id = $request->id;

        switch ($name) {
            case 'measureGroup':
                Measurement::destroy($id);
                break;

            case 'items':
                $items = MeasureItem::where('measurement_id', $id)
                    ->delete();
                break;
        }
    }
}
