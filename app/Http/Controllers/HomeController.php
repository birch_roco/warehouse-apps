<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = new Client();
        $res = $client->request('POST', 'https://api.linnworks.net//api/Auth/AuthorizeByApplication', [
            'form_params' => [
                'applicationId' => env('LINN_APP_ID'),
                'applicationSecret' => env('LINN_APP_SECRET'),
                'token' => env('LINN_TOKEN')
                ]
            ]);

        $authData = json_decode($res->getBody(), true);
        $returnServer = $authData['Server'];
        $returnToken = $authData['Token'];
    
        session([
            'LinnReturnServer' => $returnServer,
            'LinnReturnToken' => $returnToken
        ]);
        // dd($authData);
        // dd($returnServer, $returnToken);
    
        return view('home');
    }
}
