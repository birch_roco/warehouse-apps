<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Count;
use App\Item;
use Carbon\Carbon;

class CountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $items = $request->items;

        // dd($items);
        $count = new Count;
        $count->userID = \Auth::user()->name;
        $count->description = $request->desciption;
        $count->recurring = $request->recurring;
        $count->recurringWeeks = $request->recurringWeeks;
        $count->save();

        $countId = $count->id;

        foreach ($items as $item) {
            $newItem = new Item;
            $newItem->count_id = $countId;
            $newItem->productSKU = $item['productSKU'];
            $newItem->linnworksID = $item['linnworksID'];
            $newItem->productCode = $item['productCode'];
            $newItem->itemType = $item['itemType'];
            $newItem->itemCode = $item['itemCode'];
            $newItem->itemColour = $item['itemColour'];
            $newItem->orderNo = $item['orderNo'];
            $newItem->expected = $item['expected'];
            $newItem->last_count_date = $item['lastCounted'];
            $newItem->countRowId = $item['countRowId'];
            $newItem->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $countItems = Item::where('count_id', $id)
                    ->orderBy('itemType', 'asc')
                    ->orderBy('itemCode', 'asc')
                    ->orderBy('itemColour', 'asc')
                    ->orderBy('orderNo', 'asc')
                    ->get();
        return $countItems;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateItems(Request $request)
    {
        // dd($request->itemData);
        $updateItems = $request->itemData;
        $name = $request->name;
        // dd(Carbon::now());

        // var_dump($updateItems['countNo']);
        
        foreach ($updateItems as $item) {
            $updateItem = Item::find($item['itemID']);

            switch ($name) {
                case 'count':
                    $updateItem->count = $item['countNo'];
                    $updateItem->userID = \Auth::user()->name;
                    $updateItem->bookedIn = Carbon::now();
                    break;
                case 'expected':
                    $updateItem->expected = $item['expected'];
                    break;
            }

            $updateItem->save();
        }
    }

    public function updateCount(Request $request)
    {
        $type = $request->type;
        $count = Count::find($request->id);

        switch ($type) {
            case 'complete':
                $count->complete = true;
                break;
            case 'printed':
                $count->printed = true;
                break;
        }
        $count->save();
    }

    public function recurring() {
        $dateToday = Carbon::now();
        $cloneCountId = '';

        echo $dateToday . "\n";

        $storedCounts = Count::all()->where('recurring', true)->where('complete', true);
        foreach ($storedCounts as $storedCount) {
            $lastUpdated = $storedCount->updated_at;
            echo $lastUpdated . "\n";

            $differenceWeeks = $dateToday->diffInWeeks($lastUpdated);

            if ($differenceWeeks >= $storedCount->recurringWeeks - 1) {
                $newCount = new Count;
                $newCount->userID = 'Recurring';
                $newCount->description = $storedCount->description;
                $newCount->recurring = $storedCount->recurring;
                $newCount->recurringWeeks = $storedCount->recurringWeeks;
                $newCount->save();

                $storedCount->recurring = null;
                $storedCount->recurringWeeks = null;

                $storedItems = Item::all()->where('count_id', $storedCount->id);

                foreach ($storedItems as $storedItem) {
                    $newItem = new Item;
                    $newItem->count_id = $newCount->id;
                    $newItem->productSKU = $storedItem->productSKU;
                    $newItem->linnworksID = $storedItem->linnworksID;
                    $newItem->productCode = $storedItem->productCode;
                    $newItem->itemType = $storedItem->itemType;
                    $newItem->itemCode = $storedItem->itemCode;
                    $newItem->itemColour = $storedItem->itemColour;
                    $newItem->orderNo = $storedItem->orderNo;
                    $newItem->expected = $storedItem->expected;
                    $newItem->last_count_date = $storedItem->last_count_date;
                    $newItem->countRowId = $storedItem->countRowId;
                    $newItem->save();
                }
                $storedCount->save();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $name = $request->name;
        $id = $request->id;

        switch ($name) {
            case 'count':
                Count::destroy($id);
                break;

            case 'items':
                $items = Item::where('count_id', $id)
                    ->delete();
                break;
        }
    }
}
