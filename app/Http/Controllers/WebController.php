<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\PurchaseNotes;
use App\Count;
use App\Item;
use App\Measurement;
use App\MeasureItem;
class WebController extends Controller
{

    public function linnworksRequest($url, $data , $request) {
        $client = new Client();
        // dd($client);
        $res = $client->request(
            'POST', $url, [
            'headers' => [
                'Authorization' => $request->session()->get('LinnReturnToken')
            ],
            'form_params' => $data
        ]);

        return $res;
    }

    public function linnworksRequestNew($url, $data , $request) {
        $client = new Client(); 
        // dd($data);

        $res = $client->request(
            'POST', $url, [
            'headers' => [
                'Authorization' => $request->session()->get('LinnReturnToken')
            ],
            'form_params' => $data
        ]);

        return $res;
        // var_dump($res->getBody()->__toString());
    }


    public function barcodeGen() {
        return view('apps.barcode-generator');
    }

    public function barcodeGenNew() {
        return view('apps.barcode-generator-new');
    }

    public function stockLocation() {
        return view('apps.stock-location');
    }

    public function binRacks() {
        return view('apps.bin-rack');
    }

    public function packRates() {
        return view('apps.packer-rates');
    }

    public function stockCount() {
        $countRecords = Count::OrderBy('complete', 'ASC')->latest()->get();
        return view('apps.stock-count', ['countRecords' => $countRecords]);
    }

    public function measurements() {
        $measureRecords = Measurement::OrderBy('complete', 'ASC')->latest()->get();
        return view('apps.measurements', ['measureRecords' => $measureRecords]);
    }

    public function purchaseOrders() {
        $notes = PurchaseNotes::latest()->get();
        return view('apps.purchase-orders', ['notes' => $notes, 'myName' => 'Birch is a grasshopper']);
    }

    public function cohenStockCheck() {
        return view('apps.cohen-stock-check');
    }

    public function customQuery(Request $request){
        $requestData = $request->sqlQuery;
    
        $url ='https://eu1.linnworks.net//api/Dashboards/ExecuteCustomScriptQuery';
        $data = [
                'script' => $requestData
                ];
                
        $myvar = WebController::linnworksRequest($url, $data , $request);

        return json_decode($myvar->getBody(), true);
    }

    public function apiLinnworks(Request $request){
        $requestData = $request->userData;

        // dd($requestData);
        
        $url ='https://eu1.linnworks.net//api/'.$request->linnworksURL;
        $data = [
                $request->userInput => $requestData
                ];
                
        $myvar = WebController::linnworksRequest($url, $data , $request);
        // dd($myvar);
        return json_decode($myvar->getBody(), true);
    }

    public function apiLinnworksRequest(Request $request){

        // dd($request->dataArray);
    
        $url ='https://eu-ext.linnworks.net//api/'.$request->linnworksURL;
        $data = $request->dataArray;        
                
        $myvar = WebController::linnworksRequestNew($url, $data , $request);
        return json_decode($myvar->getBody(), true);
    }
}
