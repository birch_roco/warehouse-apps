<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $dateToday = Carbon::now();
            $cloneCountId = '';

            echo $dateToday . "\n";

            $storedCounts = Count::all()->where('recurring', true)->where('complete', true);
            foreach ($storedCounts as $storedCount) {
                $lastUpdated = $storedCount->updated_at;
                echo $lastUpdated . "\n";

                $differenceWeeks = $dateToday->diffInWeeks($lastUpdated);

                if ($differenceWeeks >= $storedCount->recurringWeeks - 1) {
                    $newCount = new Count;
                    $newCount->userID = 'Recurring';
                    $newCount->description = $storedCount->description;
                    $newCount->recurring = $storedCount->recurring;
                    $newCount->recurringWeeks = $storedCount->recurringWeeks;
                    $newCount->save();

                    $storedCount->recurring = null;
                    $storedCount->recurringWeeks = null;

                    $storedItems = Item::all()->where('count_id', $storedCount->id);

                    foreach ($storedItems as $storedItem) {
                        $newItem = new Item;
                        $newItem->count_id = $newCount->id;
                        $newItem->productSKU = $storedItem->productSKU;
                        $newItem->linnworksID = $storedItem->linnworksID;
                        $newItem->productCode = $storedItem->productCode;
                        $newItem->itemType = $storedItem->itemType;
                        $newItem->itemCode = $storedItem->itemCode;
                        $newItem->itemColour = $storedItem->itemColour;
                        $newItem->orderNo = $storedItem->orderNo;
                        $newItem->expected = $storedItem->expected;
                        $newItem->last_count_date = $storedItem->last_count_date;
                        $newItem->countRowId = $storedItem->countRowId;
                        $newItem->save();
                    }
                    $storedCount->save();
                }
            }
        })->sundays()->weekly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
