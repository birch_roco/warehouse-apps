<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeasureItem extends Model
{
    protected $table = 'measure_items';
    public $timestamps = false;

    public function count() {
    	return $this->belongsTo('App\Measurements');
    }
}
