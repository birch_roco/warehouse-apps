<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Measurement extends Model
{
    protected $table = 'measurements';

    public function items() {
    	return $this->hasMany('App\MeasureItems');
    }
}
