Bugs
====

1.0
===
1. Add new rows container is adding a new row when clicked -- FIXED
2. Alert when print is clicked, even if the paper type is checked -- FIXED
3. Accessories showing item type x 2 	e.g. Accessory: 5054732192460, Suit: 5054732189392 -- FIXED
4. Scroll bar is still showing up on the print preview (24) -- FIXED
5. Need to display the lower text in the same size font as the first line, make the barcode smaller -- FIXED