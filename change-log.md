Changes
=======
22/11/17
========
* Updated the purchase order screen
	* Shows less information on load
	* Status history is now available
	* Search bar is more interactive with the item and po being highlighted
	* Enter searches for an item
	* View items button shows/hides items for the po
	* Cusor is now a pointer when hoovering over an item group
	* Page reloads after a status/note is saved

16/11/17
========
* Added the option to make a reccuring count (needs testing with cron, only tested locally)

15/11/17
========
* Finished updating the status and not function on the Purchase Order screen
* Fixed the search bar on the Purchas eOrder screen

08/11/17
========
* Created a function to open the count sheet print in a new window
* Styled new printable count sheet

27/10/17
========
* Updated the Stock Count to fix the bug of it not opening up the modal or saving the data to the database
* Updated a ticket with Linnworks to find out why the update extended property request is not working


30/09/17
========
Stock Count
-----------
* Added focus to the text box so that it returns back after + is clicked
* Added delete button to the count sheet table and set it to create the list of items and sheet
* Added search bar to the view tab so that a user can scan the barcode on a count sheet

Bin Rack
--------
* Added validation to the update so the user knows when it has been successful


11/09/17
========
Stock Count
-----------
* Added the select all tick box to select all of the searched items
* Created the tables to store the count sheets, had a problem setting up foreign keys


10/09/17
========
AJAX
----
* Tried to work out why I can not send more than 1 key => value to request data, need to look into it more

Stock Count
-----------
* Managed to get the items that are selected to more from the search card to the create count card.
* Changed the remove option to hide so that when the items are sent back they are in the same location.
	- If the search button is pressed then the items will reapear (Glynn said that it's not a problem at the moment)


07/09/17
========

Cohen Stock Checker
-------------------
* Updated query to work with this app
* Created view and js to pass the data to the table
* Added 2nd table for zero stock
* Added data tables

Stock Count
-----------
* Added search by minimum extended property
* Added search by sku