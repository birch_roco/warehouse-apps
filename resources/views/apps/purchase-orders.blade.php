@extends('layouts.master')

@section('content')
<div class="container w-100">
	<div class="row">
		<div class="col-lg-6  mx-auto">
			<div class="input-group" id="item-search">
				<input type="text" class="form-control" placeholder="Search for...">
				<span class="input-group-btn">
					<button class="btn btn-secondary" type="button">Search</button>
				</span>
			</div>
		</div>
	</div>

	{{-- <div class="mx-auto text-center"  id="item-switch">
		<p>Show Items?</p>
		<label class="switch">
			<input type="checkbox" checked disabled>
			<span class="slider round"></span>
		</label>
	</div> --}}
	<div class="row mt-5" id="purchase-order-columns">
		<div class="col" id="default-container">
			<h4 class="text-center">Default</h4>
			<div id="default-results"></div>
		</div>
		<div class="col" id="jc-container">
			<h4 class="text-center">Jonathan Cohen</h4>
			<div id="jc-results"></div>
		</div>
		<div class="col" id="jc-to-default-container">
			<h4 class="text-center">Jonathan Cohen <i class="fal fa-angle-double-right"></i> Default</h4>
			<div id="jc-to-default-results"></div>
		</div>
	</div>
</div>

{{-- New note modal --}}
<div class="modal fade" id="new-note-modal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add a note to the purchase order</h5>
			</div>
			<div class="modal-body">
				<form>
				    <div>
					    <input type="text" id="poNumberInput" name="poNumber" hidden>
				        <p id="poNumberID"></p>
				    </div>
				    <div w-100>
				        <textarea id="tb-poNotes" rows="3" name="poNote" style="width: 100%;"></textarea>
				    </div>
				    {{-- <button class="btn btn-primary" id="add-new-note-btn">Submit</button> --}}
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="add-new-note-btn">Save</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

{{-- Change status modal --}}
<div class="modal fade" id="change-status-modal" username="{{ Auth::user()->name }}">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" pkPurchaseID=""><span class="po-id">PO16101789</span> - <span class="delivery-date">24/02/2018</span></h5>
			</div>
			<div class="modal-body">

				<div class="mb-2">
					<label class="form-check-label">
						<input class="form-check-input" reference="accurate-to-options" type="radio" name="statusRadios" id="rb-status" value="ACCURATE TO">
						Accurate to:
					</label>
				</div>
				<div class="row text-center justify-content-center accurate-to-options status-options" hidden>
					<div class="col col-3">
						<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle w-100 status-str-data" type="button" id="plus-minus-dd" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Plus / Minus
							</button>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="#">+/-</a>
								<a class="dropdown-item" href="#">+</a>
								<a class="dropdown-item" href="#">-</a>
							</div>
						</div>
					</div>
					<div class="col col-3">
						<input class="form-control status-str-data" type="text" id="accurate-date">
					</div>
					<div class="col col-3">
						<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle w-100 status-str-data" type="button" id="time-frame-dd" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Time Frame
							</button>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="#">Days</a>
								<a class="dropdown-item" href="#">Weeks</a>
								<a class="dropdown-item" href="#">Months</a>
							</div>
						</div>
					</div>
				</div>

				<div class="mt-2 mb-2">
					<label class="form-check-label">
						<input class="form-check-input" reference="not-booked-options" type="radio" name="statusRadios" id="rb-status" value="DELIVERED - NOT BOOKED IN">
						Delivered - Not Booked In
					</label>
				</div>
				<div class="w-30 text-center mx-auto not-booked-options status-options" hidden>
					<div style="width: 30%" class="mx-auto">
						<label for="di-not-booked-in" ><small>Date Delivered</small></label>
						<input id="di-not-booked-in" class="form-control delvered-date status-str-data" type="date" placeholder="Date delivered">
					</div>
				</div>

				<div class="mt-2 mb-2">
					<label class="form-check-label">
						<input class="form-check-input" reference="processing-options" type="radio" name="statusRadios" id="rb-status" value="DELIVERED - PROCESSING">
						Delivered - Processing
					</label>
				</div>
				<div class="row justify-content-center align-items-end processing-options status-options" hidden>
					<div class="col col-4 text-center">
						<label for="di-processing"><small>Date Delivered</small></label>
						<input class="form-control delvered-date status-str-data" id="di-processing" type="date" placeholder="Insert text......">
					</div>
					<div class="col col-4">
						<input class="form-control status-str-data" type="text" id="reason" placeholder="Reason ......">
					</div>
				</div>

				<div class="mt-2 mb-2">
					<label class="form-check-label">
						<input class="form-check-input" type="radio" name="statusRadios" id="rb-status" value="CONFIRMED">
						Confirmed
					</label>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="status-save-btn">Save</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="item-search-modal">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Item Delivery Date</h5>
			</div>
			<div class="modal-body">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@endsection

@section('pagespecificscripts')
	<script src="/js/app/moment.js"></script>
	<script src="/js/app/purchase-orders/purchase-order.js"></script>
@endsection