@extends('layouts.master')

@section('content')
	<!-- Nav tabs -->
	<div class="container-fluid do-not-print">
		<ul class="nav nav-tabs" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" id="create-tab" data-toggle="tab" href="#create" role="tab">Create</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" id="view-tab" data-toggle="tab" href="#view" role="tab">View</a>
			</li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content do-not-print">
			<div class="tab-pane active" id="create" role="tabpanel">
				<div class="row">
					<div class="col col-5">
						<div class="card text-center cardLeft">
							<div class="card-header">
								<div class="dropdown" id="dd-search-by">
									<button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Search by....
									</button>
									<div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="#">SKU</a>
										<a class="dropdown-item" href="#">Not Measured</a>
									</div>
								</div>

								<div class="input-group" id="search-sku">
									<input type="text" class="form-control" placeholder="Search for...">
									<span class="input-group-btn">
										<button class="btn btn-secondary" type="button">Search</button>
									</span>
								</div>
							</div>
							<div class="card-block text-left h-auto" id="search-results">
							</div>
							<div class="card-footer text-muted text-left">
								<div class="form-check" id="select-all">
									<label class="form-check-label">
										<input type="checkbox" class="form-check-input">
										Select All
									</label>
								</div>
							</div>
						</div>
					</div>

					<div class="col col-2 cardCenter d-flex flex-column justify-content-center align-items-center">
						<div class="p-2 w-100">
							<button type="button" class="btn btn-secondary w-100" id="btn-add-to-measure-sheet">Add <i class="fal fa-angle-right"></i></button>
						</div>
						<div class="p-2 w-100">
							<button type="button" class="btn btn-secondary w-100" id="btn-remove-from-measure-sheet"><i class="fal fa-angle-left"></i> Remove</button>
						</div>
					</div>

					<div class="col col-5">
						<div class="card text-center cardRight" id="create-measure-sheet">
							<div class="card-header">
								<label for="measure-description">Measurement Sheet Description</label>
							    <textarea class="form-control" id="measure-description" rows="3"></textarea>
							</div>
							<div class="card-block text-left" id="measure-sheet-items">
							</div>
							<div class="card-footer text-muted text-right">
								<button type="button" class="btn btn-primary" id="btn-save">Save</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane" id="view" role="tabpanel">
				{{-- <div class="input-group w-50 mx-auto mb-3">
					<input type="text" class="form-control" placeholder="Measurements ID...." id="measure-id-search">
				</div> --}}
				<table class="table w-100" id="measure-table">
					<thead class="thead-inverse">
						<tr><th>ID</th><th>Description</th><th>Created By</th><th>Date</th><th>Complete</th><th></th><th class="hidden-lg-down"></th><th class="hidden-lg-down"></th></tr>
					</thead>
					<tbody>
						@foreach($measureRecords as $measureRecord)
							<tr id="{{ $measureRecord->id }}">
								<td>{{ $measureRecord->id }}</td>
								<td>{{ $measureRecord->description }}</td>
								<td>{{ $measureRecord->userID }}</td>
								<td>{{ $measureRecord->created_at->format('d-m-Y') }}</td>
								<td class="measure-status text-center">{{ $measureRecord->complete }}</td>
								<td class="td-view-btn"><button type="button" class="btn btn-secondary btn-view"><i class="fal fa-list-ul"></i></button></td>

								@if(!$measureRecord->complete)
									<td class="td-print-btn hidden-lg-down"><button type="button" class="btn btn-secondary btn-print"><i class="fal fa-print"></i></button></td>

								@if(strtolower(Auth::user()->name) == 'birch' || strtolower(Auth::user()->name) == 'justin' || 	strtolower(Auth::user()->name) == 'scotty')
									<td class="td-delete-btn hidden-lg-down"><button type="button" class="btn btn-secondary btn-delete"><i class="fal fa-trash-alt"></i></button></td>
								@endif
								@else
								<td></td><td></td>	
								@endif
								
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="modal w-100" id="item-measure-modal">
		<div class="modal-dialog w-100 m-md-0" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mx-auto"></h5>
					<button type="button" class="close do-not-print" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<table class="table">
						<thead class="thead-inverse">
							<tr><th>SKU</th><th>Length (cm)</th><th>Width (cm)</th><th>Height (cm)</th><th>Weight (gr)</th><th class="hidden-md-down">User</th></tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
				<div class="modal-footer do-not-print">
					<div class="row w-100 justify-content-end do-not-print">
						<div class="col col-md-3 col-lg-2 do-not-print">
							<button type="button" class="btn btn-primary w-100 do-not-print" id="save-changes">Save changes</button>
						</div>
						<div class="col col-md-3 col-lg-2 do-not-print">
							<button type="button" class="btn btn-secondary w-100 do-not-print" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('pagespecificscripts')
	{{-- <script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jq-2.2.4/dt-1.10.15/datatables.min.js"> --}}
	<script src="/js/app/jquery-barcode.js"></script>
	<script src="/js/app/moment.js"></script>
    <script src="/js/app/measurements/measurements.js"></script>
    <script src="/js/app/printThis.js"></script>
@endsection