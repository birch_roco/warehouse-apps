@extends('layouts.master')

@section('content')
	<div class="container">
		<!-- Input group -->
		<div id="all-user-cb">
			<!-- All users checkbox -->
			<label class="custom-control custom-checkbox" id="cb-all-users">
				<input type="checkbox" class="custom-control-input">
				<span class="custom-control-indicator"></span>
				<span class="custom-control-description">All Users</span>
			</label>
		</div>
		<div class="row  align-items-end" id="user-inputs">
			<!-- User dropdown -->
			<div class="col col-6">
				<div class="dropdown" id="dd-usernames">
					<button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Username
					</button>
					<div class="dropdown-menu dropdown-menu-middle w-100" aria-labelledby="dropdownMenuButton" id="username-list">
					</div>
				</div>
			</div>
			<div class="col col-3">
				<label for="date-from" class="col-form-label">Date from...</label>
				<input type="date" class="form-control" id="date-from">
			</div>
			<div class="col col-3">
				<label for="date-to" class="col-form-label">Date to...</label>
				<input type="date" class="form-control" id="date-to">
			</div>
		</div>

		<div id="search-button" class="mt-2">
			<button type="button" class="btn btn-primary" id="btn-search">SEARCH</button>
		</div>

		<div id="results" class="mt-2" hidden>
			<table class="table" id="packer-rate-table">
				<thead class="thead-inverse">
					<tr><th>User</th><th>Date From</th><th>Date To</th><th>Total Packed</th><th>Total Hours</th><th>Packer Rate</th></tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>

@endsection

@section('pagespecificscripts')
	<script src="/js/app/moment.js"></script>
    <script src="/js/app/packer-rates/user-inputs.js"></script>
@endsection