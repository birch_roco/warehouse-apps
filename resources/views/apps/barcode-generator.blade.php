@extends('layouts.master')

@section('content')
<div class="container">
    <a href="/barcode-generator-new">New Version</a>
    <!-- TABS -->
    <div class="tab-container">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#barcode-gen" role="tab">Barcodes Generator</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#po-gen" role="tab" hidden>PO Generator</a>
            </li>
        </ul>

        <!-- Tab panes -->
            <div class="tab-content">

                <div class="tab-pane active" id="barcode-gen" role="tabpanel">
                    <div id="create-barcode-options">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">How to create barcodes...</button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Search by PO number</a>
                                <a class="dropdown-item" href="#">Manually enter barcodes</a>
                            </div>
                        </div>
                    </div>
                    <div class="mt-2" id="label-type-options" hidden>
                        <div class="tab-block">
                            <form id="label-type-form">
                                <label><input type="radio" name="rb-label-type" id="rb-label-type" value="labelPage" /> Label Page</label>
                                <span class="separator"></span>
                                <label><input type="radio" name="rb-label-type" value="labelRoll" /> Label Roll</label>
                            </form>
                        </div>
                    </div>
                    <div class="mt-1" id="number-of-labels-options" hidden>
                        <hr>
                        <form id="labels-per-page-form">
                            <label><input type="radio" name="rb-labels-per-page" value="8"> 8 labels</label> <span class="separator"></span> 
                            <label><input type="radio" name="rb-labels-per-page" value="24"> 24 labels</label> <span class="separator"></span>  
                            <label><input type="radio" name="rb-labels-per-page" value="65"> 65 labels</label>
                        </form>
                        <hr>
                    </div>
                    <div id="search-by-po-inputs" hidden>
                       <div class="input-group" id="search-by-po">
                            <input type="text" class="form-control" id="tb-barcode-po-search" placeholder="PO Number...">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" id="btn-barcode-po-search" type="button">Search</button>
                            </span>
                        </div> 
                    </div>
                    
                    <div class="manual-entry-input-group" hidden>
                        
                    </div>


                    <div class="mt-3" id="add-row-button" hidden>
                        <button type="button" class="btn btn-primary" id="add-new-row-btn">+ Add more</button>
                    </div>

                    <div class="mt-4" id="barcode-search-results" hidden>
                    </div>
                
                    <div class="mt-4" id="bc-create-print-buttons" hidden>
                        <hr>
                        <button type="button" class="btn btn-primary" id="create-btn">Create</button>
                        <button type="button" class="btn btn-primary" id="print-btn">Print</button>
                    </div>   

                    <div id="print-preview-barcodes">
                        
                    </div>
                </div>


                <div class="tab-pane" id="po-gen" role="tabpanel">
                    <div class="container">
                        <div class="row" id="po-search-inputs">
                            <div class="input-group" id="po-gen-search">
                                <input type="text" class="form-control" id="tb-po-gen-search" placeholder="PO Number...">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" id="btn-po-gen" type="button">Search</button>
                                </span>
                            </div>
                        </div>
                        <div class="row" id="po-gen-results" hidden></div>
                        <div class="row" id="po-create-print-buttons" hidden>
                            <button type="button" class="btn btn-primary" id="create-po-btn">Create</button>
                            <button type="button" class="btn btn-primary" id="print-po-btn">Print</button>
                        </div>
                        
                    </div>
                </div>
            </div>
    </div>

    <!-- END TABS -->
    


</div>
@endsection

@section('pagespecificscripts')
    <script src="/js/app/jquery-barcode.js"></script>
    <script src="/js/app/barcode-generator/user-input.js"></script>  
    <script src="/js/app/barcode-generator/print-preview.js"></script>
    <script src="/js/app/barcode-generator/create-barcode.js"></script>
    <script src="/js/app/barcode-generator/look-up.js"></script>
    <script src="/js/app/printThis.js"></script>
@endsection