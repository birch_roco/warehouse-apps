@extends('layouts.master')

@section('content')
<div class="container">
    <a href="/barcode-generator">Old Version</a>
    <div class="row  justify-content-md-center">
        <div class="col-md-4">
            <div class="dropdown" id="search-method">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 100%">How to create barcodes...</button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="width: 100%">
                    <a class="dropdown-item" href="#" barcode-input-group="name-po-search" search-type="po-number">Search by PO number</a>
                    <a class="dropdown-item" href="#" barcode-input-group="barcode-inputs" search-type="by-barcode">Manually enter barcodes</a>
                    <a class="dropdown-item" href="#" barcode-input-group="name-po-search" search-type="by-name">Search by name</a>
                </div>
            </div>
        </div>
    </div>

    <div id="input-container">
        <div class="input-group w-75 mx-auto mt-2 user-input" id="name-po-search" hidden>
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
                <button class="btn btn-secondary" type="button">Search</button>
            </span>
        </div>

        <div class="mt-2 user-input" id="barcode-inputs" hidden>
            <div class="row barcode-input-group">
                <div class="col col-5">
                    <input type="text" class="form-control" placeholder="Barcode Number" id="barcode-number-input">
                </div>
                <div class="col col-2">
                    <input type="text" class="form-control" placeholder="Qty" class="barcode-qty">
                </div>
                <div class="col col-5">
                    <span></span>
                </div>
            </div>
            <div id="add-barcode-inputs" class=" mt-1">
                <button type="button" class="btn btn-primary">+</button>
            </div>
        </div>    
    </div>
    

    <div id="search-results" class="mt-3" hidden>
        
    </div>

    <hr>

    <div class="mt-2" id="paper-type-rb" hidden>
        <div class="btn-group" data-toggle="buttons">
            <label class="btn btn-primary">
                <input type="radio" name="paper-type" id="label-sheets-rb" autocomplete="off" value="labels-sheet"> Label Sheet
            </label>
            <label class="btn btn-primary">
                <input type="radio" name="paper-type" id="label-roll-rb" autocomplete="off" labels-per-row="1" rows-per-page="1" value="labels-roll"> Label Roll
            </label>
        </div>
    </div>

    <div class="mt-2" id="labels-per-page-rb" hidden>
        <div class="btn-group" data-toggle="buttons">
            <label class="btn btn-primary">
                <input type="radio" name="labels-per-page" id="labels-per-page-8" autocomplete="off" labels-per-row="2" rows-per-page="4"> 8 Labels
            </label>
            <label class="btn btn-primary">
                <input type="radio" name="labels-per-page" id="labels-per-page-24" autocomplete="off" labels-per-row="3" rows-per-page="8"> 24 Labels
            </label>
            <label class="btn btn-primary">
                <input type="radio" name="labels-per-page" id="labels-per-page-65" autocomplete="off" labels-per-row="5" rows-per-page="13"> 65 Labels
            </label>
        </div>
    </div>

    <div id="create-barcodes" class="mt-2" hidden>
        <button type="button" class="btn btn-primary">Create</button> 
    </div>

    <div id="print-barcodes" class="mt-2" hidden>
        <button type="button" class="btn btn-primary">Print</button> 
    </div>

    <div id="print-preview-barcodes" hidden>
        
    </div>
</div>
@endsection

@section('pagespecificscripts')
    <script src="/js/app/barcode-generator/barcode-generator-new.js"></script>
    <script src="/js/app/barcode-generator/look-up.js"></script>
    <script src="/js/app/jquery-barcode.js"></script>
    <script src="/js/app/printThis.js"></script>
@endsection