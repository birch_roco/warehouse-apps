@extends('layouts.master')

@section('content')
	<div class="container w-75">
		<div id="zero-stock-table-wrapper">
			<h4>Zero Stock</h4>
			<table class="table text-left mb-5" id="cohen-zero-stock-table">
				<thead class="thead-inverse">
					<tr><th>SKU</th><th>Default</th><th>Cohen</th></tr>
				</thead>
				<tbody>
				</tbody>
			</table>	
		</div>	
		<div id="low-stock-table-wrapper">
			<h4>Low Stock</h4>
			<table class="table text-left" id="cohen-low-stock-table">
				<thead class="thead-inverse">
					<tr><th>SKU</th><th>Default</th><th>Cohen</th></tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
@endsection

@section('pagespecificscripts')
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jq-2.2.4/dt-1.10.15/datatables.min.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function() {

			var cohenStock = StockLevel.DefaultCohen();
			var tableStr = CreateStockTable(cohenStock.Results)

			$('#cohen-low-stock-table tbody').html(tableStr[0]);
			$('#cohen-zero-stock-table tbody').html(tableStr[1]);

			$('#cohen-low-stock-table, #cohen-zero-stock-table').DataTable({
				searching: false,
				paging: false,
				info: false
			});

			function CreateStockTable(itemArray) {
				var lowItemStr = '';
				var zeroItemStr = '';
				var zeroQty = 0;

				$.each(itemArray, function(i) {
					if(itemArray[i].AvailableDefault > 0) {
						lowItemStr += '<tr>';
						lowItemStr += '<td>' + itemArray[i].SKU + '</td>';
						lowItemStr += '<td>' + itemArray[i].AvailableDefault + '</td>';
						lowItemStr += '<td>' + itemArray[i].QuantityJC + '</td>';
						lowItemStr += '</tr>';
					} else {
						zeroItemStr += '<tr>';
						zeroItemStr += '<td>' + itemArray[i].SKU + '</td>';
						zeroItemStr += '<td>' + itemArray[i].AvailableDefault + '</td>';
						zeroItemStr += '<td>' + itemArray[i].QuantityJC + '</td>';
						zeroItemStr += '</tr>';
						zeroQty++;
					}

					if(zeroQty === 0) {
						$('#zero-stock-table-wrapper').hide();
					}
				});

				return [lowItemStr, zeroItemStr];
			}
		});
	</script>
@endsection