@extends('layouts.master')

@section('content')
	<div class="container">
		<div id="card-wrapper">
			<div class="card">
				<div class="card-header">
					<div class="row justify-content-between">
						<div class="col-6">
							<form class="locations-form">
				                <label><input type="radio" name="rb-location" value="default" /> Default</label>
				                <span class="separator"></span>
				                <label><input type="radio" name="rb-location" value="reFill" /> Re-Fill</label>
				            </form>
						</div>
						<div class="col-1 text-right delete-card">
							<i class="fal fa-times-circle" aria-hidden="true"></i>
						</div>
					</div>
				</div>
				<div class="card-block">
					<div class="row">
						<!-- Item name textbox -->
						<div class="col col-6 item-name">
							<div class="input-group input-group-lg">
								<input type="text" class="form-control tb-item-name" name="search-name" placeholder="Product Name...">
							</div>
						</div>
						{{--
						<div class="col-lg-2">
							<div class="dropdown">
								<button class="btn btn-secondary dropdown-toggle dd-type-btn btn-lg" type="button" name="search-type" id="dd-type-selector-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>
									Type Select
								</button>
								<div class="dropdown-menu type-selector" aria-labelledby="dd-type-selector-0" id="dd-type-select-0">
								</div>
							</div>
						</div> --}}
						<!-- Colour dropdown -->
						<div class="col-lg-3">
							<div class="dropdown">
								<button class="btn btn-secondary dropdown-toggle dd-colour-btn btn-lg" type="button" name="search-colour" id="dd-colour-selector-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>
									Colour Select
								</button>
								<div class="dropdown-menu colour-selector" aria-labelledby="dd-colour-selector-0" id="dd-colour-select-0">
								</div>
							</div>
						</div>
						<div class="btn-group col-lg-3" data-toggle="buttons">
							<button type="button" class="btn btn-danger btn-lg" id="btn-range">Range?</button>
						</div>
					</div>

					

					{{-- <div class="input-group">
						<input type="checkbox" aria-label="Checkbox for following text input">
						<p> Range?</p>
					</div> --}}

					<div class="row mt-3">
						<!-- Size dropbown start -->
						<div class="col col-3">
							<div class="dropdown m-x-0">
								<button class="btn btn-secondary dropdown-toggle dd-start-size-selector btn-lg" type="button" name="search-size-start" id="dd-start-size-selector-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>
									Size Select
								</button>
								<div class="dropdown-menu start-size-selector" aria-labelledby="dd-start-size-select-0" id="dd-start-size-select-0">
								</div>
							</div>
						</div>
						<div class="col col-3">
							<!-- Size dropdown - end -->
							<div class="dropdown hide-element">
								<button class="btn btn-secondary dropdown-toggle dd-end-size-selector btn-lg" type="button" name="search-size-end" id="dd-end-size-selector-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="display: none;">
									Size Select
								</button>
								<div class="dropdown-menu end-size-selector" aria-labelledby="dd-end-size-select-0" id="dd-end-size-select-0">
								</div>
							</div>
						</div>
					</div>
					<div class="row mt-3">
						<div class="col col-3">
							<div class="dropdown dd-stock-area">
								<button class="btn btn-secondary dropdown-toggle btn-lg" type="button" id="btn-area-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Area
								</button>
								<div class="dropdown-menu" aria-labelledby="btn-area-0">
									<a class="dropdown-item" href="#" name="blue">Blue</a>
									<a class="dropdown-item" href="#" name="green">Green</a>
									<a class="dropdown-item" href="#" name="orange">Orange</a>
									<a class="dropdown-item" href="#" name="red">Red</a>
								</div>
							</div>
						</div>
						<div class="col col-3">
							<div class="dropdown dd-stock-aisle">
								<button class="btn btn-secondary dropdown-toggle btn-lg" type="button" id="btn-aisle-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>
								Aisle No
								</button>
								<div class="dropdown-menu" aria-labelledby="btn-aisle-0">
								</div>
							</div>
						</div>
						<div class="col col-6 tb-bay-number input-group-lg">
							<input type="text" class="form-control tb-new-bay-number" name="bay-number"" aria-label="Text input with dropdown button" placeholder="Bay No.." disabled>
						</div>	
					</div>
				</div>
			</div>
		</div>
		<!-- Add new row button -->
		<div class="row mt-2">
			<div class="col">
				<button type="button" class="btn btn-primary btn-lg" id="btn-update">Update</button>
			</div>
			<div class="col text-right">
				<button type="button" class="btn btn-primary btn-lg" id="btn-add-new">+</button>
			</div>
		</div>			

	<!-- Modal for clashing items -->
	<div class="modal" id="clashingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="true" data-animation="false">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="exampleModalLabel">Clashing Items</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
				</div>
			</div>
		</div>
	</div>

	<!-- Modal for confirming bin rack changes -->
	<div class="modal" id="bin-rack-changes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header printable">
					<h4 class="modal-title" id="exampleModalLabel">Bin Rack Changes</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div id="changes-title">
					<h5>Are you happy with these changes?</h5>
				</div>
				<div class="modal-body printable">
					<table class="table table-striped">
						<thead>
							<tr><th>SKU</th><th>Location</th><th>Current Bin Rack</th><th>New Bin Rack</th></tr>
						</thead>
						<tbody>
							{{-- Insert bin rack change info --}}
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="print-btn">Print</button>
					<button type="button" class="btn btn-primary" id="confirm-btn">Confirm</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Back</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal bd-example-modal-lg" id="success-model" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content" id="success-info">
				<div class="modal-header mt-4 pb-0">
				</div>
				<div class="modal-body pt-0">
					<div class="container">
						<div class="text-center justify-content-center">
							<div id="uploaded-count">
							</div>
						</div>
					</div>
					<div class="container">
						<div id="success-img">
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" id="close-success" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('pagespecificscripts')
    <script src="/js/app/bin-rack/bin-rack.js"></script>
    <script src="/js/app/printThis.js"></script>
@endsection