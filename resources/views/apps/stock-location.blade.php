@extends('layouts.master')

@section('content')
	<div class="container">
		<div class="search-selectors mt-3">
			<form id="search-by-form">
                <label><input type="radio" name="rb-search-by" value="barcode" /> Barcode</label>
                <span class="separator"></span>
                <label><input type="radio" name="rb-search-by" value="manual" /> Manual</label>
            </form>
		</div>

		<!-- User inputs -->
		<div class="user-inputs" class="mt-3">
			<div id="barcode-search" hidden>
				<!-- INPUT: Barcode Number -->
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Barcode number..." aria-describedby="basic-addon1" id="barcode-number">
				</div>
			</div>

			<div class="row" id="manual-search" class="mt-3" hidden>
				<!-- INPUT: Product name -->
				<div class="col col-6">
					<input type="text" class="form-control" placeholder="Product name..." aria-describedby="basic-addon1" id="product-name">
				</div>
				<!-- INPUT: Colour Dropdown -->
				<div class="col col-2">
					<div class="dropdown" id="dd-colour">
						<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Colour</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="dd-colour-select">
						</div>
					</div>
				</div>
			</div>
			<div id="search-button" class="mt-3" hidden>
				<button type="button" class="btn btn-primary" id="btn-search">Search</button>
			</div>
		</div>

		<!-- Display item locations -->
		<div id="results" class="mt-4">
			<table class="table" id="location-table" hidden>
				<thead class="thead-inverse">
					<tr><th>SKU</th><th>Default Location</th><th>Re-Fill Location</th></tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>

	<!-- Modal for clashing items -->
	<div class="modal" id="clashingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="true" data-animation="false">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="exampleModalLabel">Clashing Items</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h5>Please select one of the following...</h5>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('pagespecificscripts')
    <script src="/js/app/stock-location/user-input.js"></script> 
    <script src="/js/app/stock-location/set-inputs.js"></script>  
@endsection