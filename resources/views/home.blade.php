@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 mx-auto">
            <div class="card card-default">
                <div class="card-body pl-2 pr-2">
                    <div class="row">
                        <div class="col col-md-4 text-center">
                            <a href="/barcode-generator">
                                <div class="app-links">
                                    <i class="fal fa-barcode"></i>
                                    <p>Barcode Generator</p>
                                </div>
                            </a>
                        </div>
                        <div class="col col-md-4 text-center">
                            <a href="/stock-location">
                                <div class="app-links">
                                    <i class="fal fa-map-marker"></i>
                                    <p>Stock Location</p>
                                </div>
                            </a>
                        </div>

                        <div class="col col-md-4 text-center">
                            <a href="/bin-rack-change">
                                <div class="app-links">
                                    <i class="fal fa-archive"></i>
                                    <p>Change Items Bin Rack</p>
                                </div>
                            </a>
                        </div>

                    </div> {{-- End: Row 1 --}}

                    <div class="row">
                        <div class="col col-md-4 text-center">
                            <a href="/packer-rates">
                                <div class="app-links">
                                    <i class="fal fa-chart-line"></i>
                                    <p>Packer Rates</p>
                                </div>
                            </a>
                        </div>
                        <div class="col col-md-4 text-center">
                            <a href="/stock-count">
                                <div class="app-links">
                                    <i class="fal fa-balance-scale"></i>
                                    <p>Stock Count</p>
                                </div>
                            </a>
                        </div>
                        <div class="col col-md-4 text-center">
                            <a href="/purchase-orders">
                                <div class="app-links">
                                    <i class="fal fa-calendar-exclamation"></i>
                                    <p>Purchase Orders</p>
                                </div>
                            </a>
                        </div>
                    </div> {{-- End: Row 2 --}}

                    <div class="row">
                        <div class="col col-md-4 text-center">
                            <a href="/measurements">
                                <div class="app-links">
                                    <i class="fal fa-expand-arrows-alt"></i>
                                    <p>Measurements</p>
                                </div>
                            </a>
                        </div>
                        <div class="col col-md-4 text-center">
                        </div>
                        <div class="col col-md-4 text-center">
                            <a href="/cohen-stock-check">
                                <div class="app-links">
                                    <i class="fal fa-cart-plus"></i>
                                    <p>Cohen Stock Checker</p>
                                </div>
                            </a>
                        </div>
                    </div> {{-- End: Row 3 --}}

                </div> {{-- End: card Body --}}

            </div> {{-- End: card --}}
        </div>
    </div>
</div>
@endsection
