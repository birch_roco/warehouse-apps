<body>
    <nav class="navbar navbar-inverse bg-inverse navbar-toggleable-md do-not-print">
        <div class="container do-not-print">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleContainer" aria-controls="navbarsExampleContainer" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>

            @if (Auth::guest() !== true)
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            @endif
        </div>
    </nav>

    <div class="container do-not-print">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
            @if (Route::currentRouteName() != 'Dashboard')
                <li class="breadcrumb-item active"><a href="#">{{(Route::currentRouteName())}}</a></li>
            @endif
            {{-- {{ Auth::user()->name }} --}}
        </ol>

        <h2 class="text-center mt-4 mb-4"><?php echo(Route::currentRouteName()); ?></h2>
    </div>