    </div>

    <div id="loading-overlay" hidden>
        <i class="fal fa-sync" id="loading-icon"></i>
    </div>

    <script src="/js/app/tether.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>

<?php $route = Route::current(); ?>

@if ($route->uri == 'stock-count' || $route->uri == 'bin-rack-change' || $route->uri == 'purchase-orders' || $route->uri == 'measurements' || $route->uri == 'barcode-generator' || $route->uri == 'barcode-generator-new')
    <script src="/js/app/apps-ajax.js"></script>
    <script src="/js/app/apps-queries.js"></script>
@elseif ( $route->uri == 'cohen-stock-check' )  
	<script src="/js/app/linnworks-ajax.js"></script>
    <script src="/js/app/linnworks-queries.js"></script>
@else
	<script src="/js/app/ajax-request.js"></script>
    <script src="/js/app/custom-queries.js"></script>
@endif
    @yield('pagespecificscripts')
    
</body>
</html>