// // Variables for SQL custom search
// var $sqlSelect;
// var $sqlTable;
// var $sqlSearchParameter; 
// var $sqlJoiningTables;
// var $sqlOrderBy;
// var $customQuery;
// var $queryResults = new Array();
// var $tableStr;

// function CustomQuery() {
//   $customQuery = "SELECT " + $sqlSelect + " ";
//   $customQuery += "FROM " + $sqlTable + " ";
//   $customQuery += $sqlJoiningTables + " ";
//   $customQuery += "WHERE " + $sqlSearchParameter;
// }

// function BarcodesPOSearch(poNumber) {
// 	$sqlSelect = "si.BarcodeNumber, sup.SupplierName, itsu.SupplierCode, st.Type, sn.Name, sc.Code, scr.Colour, sz.Size, pi.Delivered, pi.Quantity - pi.Delivered AS 'Due', '' AS 'DeliveredNew', '' AS 'BookedIn'";
//     $sqlTable = 'Purchase pur';
//     $sqlJoiningTables = "INNER JOIN PurchaseItem pi ON pur.pkPurchaseID = pi.fkPurchasId INNER JOIN StockItem si ON pi.fkStockItemId = si.pkStockItemID INNER JOIN ItemSupplier itsu ON pi.fkStockItemId = itsu.fkStockItemId INNER JOIN Supplier sup ON pur.fkSupplierId = sup.pkSupplierID FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyst', siep.ProperyName, siep.ProperyValue AS 'Type' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.ItemType') st ON si.pkStockItemID = st.fkeyst FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysn', siep.ProperyName, siep.ProperyValue AS 'Name' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.name') sn ON si.pkStockItemID = sn.fkeysn FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysc', siep.ProperyName, siep.ProperyValue AS 'Code' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.code') sc ON si.pkStockItemID = sc.fkeysc FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyscr', siep.ProperyName, siep.ProperyValue AS 'Colour' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.colour') scr ON si.pkStockItemID = scr.fkeyscr FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyson', siep.ProperyName, siep.ProperyValue AS 'OrderNo' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.OrderNo') son ON si.pkStockItemID = son.fkeyson FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysz', siep.ProperyName, siep.ProperyValue AS 'Size' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.Size') sz ON si.pkStockItemID = sz.fkeysz ";
//     $sqlSearchParameter = " itsu.isDefault = 1 AND pur.ExternalInvoiceNumber = '" + poNumber + "' ORDER BY st.Type, sc.Code, scr.Colour, son.OrderNo";

//     CustomQuery();
// }



function CreateTable() {
	console.log($queryResults);
	$tableStr = '<table class="table"><thead class="thead-inverse">';
	$tableStr += '<tr>'
	$tableStr += '<th>Supplier Code</th><th>Code</th><th>Colour</th><th>Size</th>';
	$tableStr += '<th>Qty</th>';
	$tableStr += '</tr></thead><tbody>';

	$.each($queryResults, function(i) {
		$tableStr += '<tr>';
		$tableStr += '<td>' + $queryResults[i].SupplierCode + '</td>';
		$tableStr += '<td>' + $queryResults[i].Code + '</td>';
		$tableStr += '<td>' + $queryResults[i].Colour + '</td>';
		$tableStr += '<td>' + $queryResults[i].Size + '</td>';
		$tableStr += '<td>' + $queryResults[i].Delivered + '</td>';
		$tableStr += '</tr>';
	});
	$tableStr += '</tbody></table>';
}

var mytoken = $('[name=csrf-token]').attr('content');

var createdBy;
var $labelType;
var $labelsPerPage;
var $poNumber;
/*********************************************
*** Search by PO Number JS *******************
*********************************************/
$(document).on('click', '#create-barcode-options div a', function(e) {
		e.preventDefault();
		var btnTxt = $(this).html();
		$('#create-barcode-options button').html(btnTxt);
		$('#label-type-options').removeAttr('hidden');
		
		if ($(this).index() === 0) {
			$createdBy = 'poNumber';
			$('#search-by-po-inputs').removeAttr('hidden');
			$('.manual-entry-input-group').prop('hidden', true);
			$('#add-row-button').prop('hidden', true);
		} else if ($(this).index() === 1) {
			$createdBy = $(this).val();
			$createdBy = 'manual';
			$('.manual-entry-input-group').removeAttr('hidden');
			$('#add-row-button').removeAttr('hidden');
			$('#search-by-po-inputs').prop('hidden', true);
		}

		console.log($createdBy);
	});

	$(document).on('change', '#label-type-options input', function() {
		$labelType = $(this).val();
		if($labelType === 'labelPage') {
			$('#number-of-labels-options').removeAttr('hidden');
		} else {
			$('#number-of-labels-options').prop('hidden', true);
		}
		console.log($labelType);
	});

	$(document).on('click', '#search-by-po-inputs button', function() {

		if($('#tb-barcode-po-search').val().length === 0) {
			alert('Please enter a PO number into the textbox');
		} else {
			$poNumber = $('#tb-barcode-po-search').val();
			BarcodesPOSearch($poNumber);
			// var data = JSON.stringify($customQuery);
			var data = $customQuery;
			//console.log(data);

			$.ajax({
				headers: {'X-CSRF-TOKEN': mytoken},
				url: '/custom-query',
				method: 'post',
				data: { 'sqlQuery': data }
			})
			.done(function(response) {
				$.each(response.Results, function( i ) {
					$queryResults.push(response.Results[i]);
				});
				// callMyFunction();
				CreateTable();
				$('#po-search-results').removeAttr('hidden');
				$('#po-search-results').html($tableStr);
				$('#bc-create-print-buttons').removeAttr('hidden');
			});
		}
	});

	$(document).on('change', '#labels-per-page-form input', function() {
		$labelsPerPage = $(this).val();
		console.log($labelsPerPage);
	});

/*********************************************
*** Search manually JS ***********************
*********************************************/

	$(document).on('click', '#add-row-button', function() {
		var createNewManualInput;
		var newElementIdNo = $('.manual-entry-input-group .row').length + 1;
		console.log(newElementIdNo);

		createNewManualInput = '<div class="row mt-1" id="manual-entry-inputs-' + newElementIdNo + '">';
		createNewManualInput += '<div class="col col-6">'
		createNewManualInput += '<input id="barcode-' + newElementIdNo + '" type="text" class="form-control barcode-number" name="BarcodeNumber" placeholder="Barcode Number..............">';
		createNewManualInput += '</div>';
		createNewManualInput += '<div class="col col-2">';
		createNewManualInput += '<input id="number-of-barcodes-' + newElementIdNo + '" type="text" class="form-control product-name" name="number" placeholder="No">';
		createNewManualInput += '</div>';
		createNewManualInput += '<div class="col col-4">';
		createNewManualInput += '<p id="barcode-ref-' + newElementIdNo + '" class="form-control-static barcode-ref"></p>';
		createNewManualInput += '</div>';
		createNewManualInput += '</div>';
		$('.manual-entry-input-group').append(createNewManualInput);
	});

	$(document).on('change', '.manual-entry-input-group input', function() {
		var barcodeNumber = $(this).val();
		var referenceId = $(this).parent().parent().index();
		console.log(referenceId);
		console.log('Barcode Number: ' + barcodeNumber);
		ReferenceData(barcodeNumber);
		var data = $customQuery;
		console.log('Query: ' + data);

		$.ajax({
				headers: {'X-CSRF-TOKEN': mytoken},
				url: '/custom-query',
				method: 'post',
				data: { 'sqlQuery': data }
			})
			.done(function(response) {
				console.log(response.Results[0].ReferenceName);
				$('.barcode-ref:eq('+referenceId+')').html(response.Results[0].ReferenceName);
			
			$('#bc-create-print-buttons').removeAttr('hidden');
		});
	});