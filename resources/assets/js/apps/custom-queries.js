
// Variables for SQL custom search
var $sqlSelect;
var $sqlTable;
var $sqlSearchParameter; 
var $sqlJoiningTables;
var $sqlOrderBy;
var $customQuery;

function CustomQuery($sqlSelect, $sqlTable, $sqlJoinTables, $sqlSearchParams) {
	$customQuery = "SELECT " + $sqlSelect + " ";
	$customQuery += "FROM " + $sqlTable + " ";
	$customQuery += $sqlJoinTables + " ";
	$customQuery += "WHERE " + $sqlSearchParams;
}

function BarcodesPOSearch(poNumber) {
	alert(poNumber); return false;
	$sqlSelect = "si.BarcodeNumber, sup.SupplierName, itsu.SupplierCode, st.Type, sn.Name, sc.Code, scr.Colour, sz.Size, pi.Delivered, pi.Quantity - pi.Delivered AS 'Due', '' AS 'DeliveredNew', '' AS 'BookedIn'";
    $sqlTable = 'Purchase pur';
    $sqlJoiningTables = "INNER JOIN PurchaseItem pi ON pur.pkPurchaseID = pi.fkPurchasId INNER JOIN StockItem si ON pi.fkStockItemId = si.pkStockItemID INNER JOIN ItemSupplier itsu ON pi.fkStockItemId = itsu.fkStockItemId INNER JOIN Supplier sup ON pur.fkSupplierId = sup.pkSupplierID FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyst', siep.ProperyName, siep.ProperyValue AS 'Type' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.ItemType') st ON si.pkStockItemID = st.fkeyst FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysn', siep.ProperyName, siep.ProperyValue AS 'Name' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.name') sn ON si.pkStockItemID = sn.fkeysn FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysc', siep.ProperyName, siep.ProperyValue AS 'Code' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.code') sc ON si.pkStockItemID = sc.fkeysc FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyscr', siep.ProperyName, siep.ProperyValue AS 'Colour' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.colour') scr ON si.pkStockItemID = scr.fkeyscr FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyson', siep.ProperyName, siep.ProperyValue AS 'OrderNo' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.OrderNo') son ON si.pkStockItemID = son.fkeyson FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysz', siep.ProperyName, siep.ProperyValue AS 'Size' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.Size') sz ON si.pkStockItemID = sz.fkeysz ";
    $sqlSearchParameter = " itsu.isDefault = 1 AND pur.ExternalInvoiceNumber = '" + poNumber + "' ORDER BY st.Type, sc.Code, scr.Colour, son.OrderNo";

    CustomQuery();
}

function ReferenceData(barcodeNumber) {
	$sqlSelect = "st.Type + ' - ' + sn.Name + ' - ' + scr.Colour + ' - ' + sz.Size AS 'ReferenceName'";
	$sqlTable = "StockItem si";
	$sqlJoiningTables = "FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyst', siep.ProperyName, siep.ProperyValue AS 'Type' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.ItemType') st ON si.pkStockItemID = st.fkeyst FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysn', siep.ProperyName, siep.ProperyValue AS 'Name' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.name') sn ON si.pkStockItemID = sn.fkeysn FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysc', siep.ProperyName, siep.ProperyValue AS 'Code' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.code') sc ON si.pkStockItemID = sc.fkeysc FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyscr', siep.ProperyName, siep.ProperyValue AS 'Colour' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.colour') scr ON si.pkStockItemID = scr.fkeyscr FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyson', siep.ProperyName, siep.ProperyValue AS 'OrderNo' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.OrderNo') son ON si.pkStockItemID = son.fkeyson FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysz', siep.ProperyName, siep.ProperyValue AS 'Size' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.Size') sz ON si.pkStockItemID = sz.fkeysz";
	$sqlSearchParameter = "si.bLogicalDelete = 0 AND si.bContainsComposites = 0 AND si.BarcodeNumber = '" + barcodeNumber + "'";

    CustomQuery();
}

function GetLocationID(locationName) {
	$sqlSelect 	= "pkStockLocationId, Location";
	$sqlTable 	= "StockLocation";
	$sqlJoiningTables = "";
	$sqlSearchParameter = "Location = '" + locationName + "'";

	CustomQuery();
}

function SearchByName(productName) {
	$sqlSelect = "st.Type, sn.Name, scr.Colour";
	$sqlTable = "StockItem si";
	$sqlJoiningTables = "FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyst', siep.ProperyName, siep.ProperyValue AS 'Type' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.ItemType') st ON si.pkStockItemID = st.fkeyst FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysn', siep.ProperyName, siep.ProperyValue AS 'Name' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.name') sn ON si.pkStockItemID = sn.fkeysn FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysc', siep.ProperyName, siep.ProperyValue AS 'Code' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.code') sc ON si.pkStockItemID = sc.fkeysc FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyscr', siep.ProperyName, siep.ProperyValue AS 'Colour' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.colour') scr ON si.pkStockItemID = scr.fkeyscr FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyson', siep.ProperyName, siep.ProperyValue AS 'OrderNo' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.OrderNo') son ON si.pkStockItemID = son.fkeyson FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysz', siep.ProperyName, siep.ProperyValue AS 'Size' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.Size') sz ON si.pkStockItemID = sz.fkeysz";
	$sqlSearchParameter = "si.bLogicalDelete = 0 AND si.bContainsComposites = 0 AND sn.Name = '" + productName + "' GROUP BY st.Type, sn.Name, scr.Colour ORDER BY st.Type, scr.Colour ASC";

	CustomQuery();
}

function SearchByNameAndType(productType, productName) {
	$sqlSelect = "st.Type, sn.Name, scr.Colour";
	$sqlTable = "StockItem si";
	$sqlJoiningTables = "FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyst', siep.ProperyName, siep.ProperyValue AS 'Type' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.ItemType') st ON si.pkStockItemID = st.fkeyst FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysn', siep.ProperyName, siep.ProperyValue AS 'Name' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.name') sn ON si.pkStockItemID = sn.fkeysn FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysc', siep.ProperyName, siep.ProperyValue AS 'Code' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.code') sc ON si.pkStockItemID = sc.fkeysc FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyscr', siep.ProperyName, siep.ProperyValue AS 'Colour' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.colour') scr ON si.pkStockItemID = scr.fkeyscr FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyson', siep.ProperyName, siep.ProperyValue AS 'OrderNo' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.OrderNo') son ON si.pkStockItemID = son.fkeyson FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysz', siep.ProperyName, siep.ProperyValue AS 'Size' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.Size') sz ON si.pkStockItemID = sz.fkeysz";
	$sqlSearchParameter = "si.bLogicalDelete = 0 AND si.bContainsComposites = 0 AND st.Type = '" + productType + "'  AND sn.Name = '" + $productName + "' GROUP BY st.Type, sn.Name, scr.Colour ORDER BY st.Type, scr.Colour ASC";

	CustomQuery();
}

function SearchByColour(productType, productName, productColour, locationID) {
	$sqlSelect = "si.pkStockItemID, si.ItemNumber, sz.Size, bin.BinRack";
	$sqlTable = "StockItem si";
	$sqlJoiningTables = "FULL OUTER JOIN (SELECT sl.fkStockItemId AS 'fkbin', sl.BinRackNumber AS 'BinRack' FROM ItemLocation sl WHERE sl.fkLocationId = '" + locationID + "') bin ON si.pkStockItemID = bin.fkbin FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyst', siep.ProperyName, siep.ProperyValue AS 'Type' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.ItemType') st ON si.pkStockItemID = st.fkeyst FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysn', siep.ProperyName, siep.ProperyValue AS 'Name' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.name') sn ON si.pkStockItemID = sn.fkeysn FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysc', siep.ProperyName, siep.ProperyValue AS 'Code' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.code') sc ON si.pkStockItemID = sc.fkeysc FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyscr', siep.ProperyName, siep.ProperyValue AS 'Colour' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.colour') scr ON si.pkStockItemID = scr.fkeyscr FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyson', siep.ProperyName, siep.ProperyValue AS 'OrderNo' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.OrderNo') son ON si.pkStockItemID = son.fkeyson FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysz', siep.ProperyName, siep.ProperyValue AS 'Size' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.Size') sz ON si.pkStockItemID = sz.fkeysz";
	$sqlSearchParameter = "si.bLogicalDelete = 0 AND si.bContainsComposites = 0 AND st.Type = '" + productType + "' AND sn.Name = '" + productName + "' AND scr.Colour = '" + productColour + "' ORDER BY son.OrderNo ASC";

	CustomQuery();
}

function GetLocationByBarcode(barcodeNumber) {
	$sqlSelect = "si.ItemNumber, si.BarcodeNumber, def.defBinRack, rf.rfBinRack";
	$sqlTable = "StockItem si";
	$sqlJoiningTables = "FULL OUTER JOIN (SELECT si.pkStockItemID AS defStockID, il.BinRackNumber AS defBinRack FROM StockItem si INNER JOIN ItemLocation il ON si.pkStockItemID = il.fkStockItemId WHERE fkLocationId = '00000000-0000-0000-0000-000000000000') def ON si.pkStockItemID = def.defStockID FULL OUTER JOIN (SELECT si.pkStockItemID AS rfStockID, il.BinRackNumber AS rfBinRack FROM StockItem si INNER JOIN ItemLocation il ON si.pkStockItemID = il.fkStockItemId WHERE fkLocationId = '3cbb5984-d3a8-4a7d-960e-88112125fdab') rf ON si.pkStockItemID = rf.rfStockID";
	$sqlSearchParameter = "si.bLogicalDelete = 0 AND si.bContainsComposites = 0 AND si.BarcodeNumber = '" + barcodeNumber + "'";

	CustomQuery();
}

function GetLocationByProductName(type, name, colour) {
	$sqlSelect = "si.ItemNumber, def.defBinRack, rf.rfBinRack";
	$sqlTable = "StockItem si";
	$sqlJoiningTables = "FULL OUTER JOIN (SELECT si.pkStockItemID AS defStockID, il.BinRackNumber AS defBinRack FROM StockItem si INNER JOIN ItemLocation il ON si.pkStockItemID = il.fkStockItemId WHERE fkLocationId = '00000000-0000-0000-0000-000000000000') def ON si.pkStockItemID = def.defStockID FULL OUTER JOIN (SELECT si.pkStockItemID AS rfStockID, il.BinRackNumber AS rfBinRack FROM StockItem si INNER JOIN ItemLocation il ON si.pkStockItemID = il.fkStockItemId WHERE fkLocationId = '3cbb5984-d3a8-4a7d-960e-88112125fdab') rf ON si.pkStockItemID = rf.rfStockID FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyst', siep.ProperyName, siep.ProperyValue AS 'Type' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.ItemType') st ON si.pkStockItemID = st.fkeyst FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysn', siep.ProperyName, siep.ProperyValue AS 'Name' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.name') sn ON si.pkStockItemID = sn.fkeysn FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysc', siep.ProperyName, siep.ProperyValue AS 'Code' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.code') sc ON si.pkStockItemID = sc.fkeysc FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyscr', siep.ProperyName, siep.ProperyValue AS 'Colour' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.colour') scr ON si.pkStockItemID = scr.fkeyscr FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyson', siep.ProperyName, siep.ProperyValue AS 'OrderNo' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.OrderNo') son ON si.pkStockItemID = son.fkeyson FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysz', siep.ProperyName, siep.ProperyValue AS 'Size' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.Size') sz ON si.pkStockItemID = sz.fkeysz";
	$sqlSearchParameter = "si.bLogicalDelete = 0 AND si.bContainsComposites = 0 AND st.Type = '" + type + "' AND sn.Name = '" + name + "' AND scr.Colour = '" + colour + "' ORDER BY son.OrderNo ASC";

	CustomQuery();
}

function GetProductNameFromId(productID) {
	$sqlSelect = "ItemNumber";
	$sqlTable = "StockItem";
	$sqlJoiningTables = "";
	$sqlSearchParameter = "pkStockItemID = '" + productID + "'";

	CustomQuery();
}

function ReturnAllLinnworksUsers() {
	$sqlSelect = "UserName";
	$sqlTable = "Users";
	$sqlJoiningTables = "";
	$sqlSearchParameter = "UserName LIKE '%@%' ORDER BY UserName ASC";

	CustomQuery();
}

function PackerRates(startDate, endDate, userName) {
	$sqlSelect = "olh.UpdatedBy, CONVERT(VARCHAR(19),olh.DateStamp, 126) AS Time";
	$sqlTable = "Order_LifeHistory olh";
	$sqlJoiningTables = "";
	$sqlSearchParameter = "olh.fkOrderHistoryTypeId = 'ORDER_PROCESSED' AND olh.DateStamp BETWEEN '" + startDate + " 00:00:01' AND '" + endDate + " 23:59:59' AND olh.UpdatedBy = '" + userName + "' ORDER BY olh.DateStamp ASC";

	CustomQuery();
}
