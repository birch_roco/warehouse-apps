var mytoken = $('[name=csrf-token]').attr('content');
var statusCode;

function AjaxRequestNew(ajaxData) {
	$.ajax({
		headers: {'X-CSRF-TOKEN': mytoken},
		url: '/api-linnworks-request',
		method: 'post',
		data: ajaxData,
		async: false,
		complete: function (data) {
			resp = data.responseText;
		},
		success: function(response){
			$ajaxStatus = 'success';
		},
		error: function(response){
			$ajaxStatus = 'errors';
		}
	});

	if(resp !== '') {
		return JSON.parse(resp);
	}
}

function AjaxRequestTest(ajaxData) {
	var statusCode;
	$.ajax({
		headers: {'X-CSRF-TOKEN': mytoken},
		url: '/api-linnworks-request',
		method: 'post',
		data: ajaxData,
		async: false,
		complete: function (data) {
			resp = data.responseText;
			statusCode = data.status;
		},
		success: function(response){
			$ajaxStatus = 'success';
		},
		error: function(response){
			$ajaxStatus = 'errors';
		}
	});

	if(resp !== '') {
		return {status: statusCode, 
				data: JSON.parse(resp)};
	} else {
		return {status: statusCode};
	}
}

function AjaxRequestNoReturn(ajaxData) {
	$.ajax({
		headers: {'X-CSRF-TOKEN': mytoken},
		url: '/api-linnworks-request',
		method: 'post',
		data: ajaxData,
		async: false,
		complete: function (data) {
			resp = data.responseText;
		},
		success: function(response){
			$ajaxStatus = 'success';
		},
		error: function(response){
			$ajaxStatus = 'errors';
		},
	});
}

function AjaxRequest(urlPath, ajaxData) {
	$.ajax({
		headers: {'X-CSRF-TOKEN': mytoken},
		url: urlPath,
		method: 'post',
		data: ajaxData,
		async: false,
		complete: function (data) {
			resp = data.responseText;
		}
	});
	// return JSON.parse(resp);
}

var StockCount = {
	AddStockCount: function(countDescription, recurring, recurringWeeks, items) {
		var $data = {'desciption': countDescription, 'recurring': recurring, 'recurringWeeks': recurringWeeks, 'items': items};
		AjaxRequest('/stock-count/create', $data);
	},
	UpdateStockCount: function(id, type) {
		var $data = {'type': type, 'id': id};
		AjaxRequest('/stock-count/update-count', $data);
		// AjaxRequestCount('/stock-count/update-count', $data);
	},
	UpdateStockItemQty: function(data) {
		var $data = {'itemData': data, 'name': 'count'};
		AjaxRequest('/stock-count/update-items', $data);
		// AjaxRequestCount('/stock-count/update-count', $data);
	},
	UpdateExpectedQty: function(data) {
		var $data = {'itemData': data, 'name': 'expected'};
		AjaxRequest('/stock-count/update-items', $data);
		// AjaxRequestCount('/stock-count/update-count', $data);
	},
	DeleteCountItems: function(id) {
		var $data = {'id': id, 'name': 'items'};
		AjaxRequest('/stock-count/delete-items', $data);
	},
	DeleteCount: function(id) {
		var $data = {'id': id, 'name': 'count'};
		AjaxRequest('/stock-count/delete-count', $data);
	}
};

var StockMeasurements = {
	AddMeasurements: function(measurementDescription, items) {
		var $data = {'desciption': measurementDescription, 'items': items};
		AjaxRequest('/measurements/create', $data);
	},
	UpdateStockItemMeasurements: function(data) {
		var $data = {'itemData': data};
		AjaxRequest('/measurements/update-items', $data);
		// AjaxRequestCount('/stock-count/update-count', $data);
	},
	UpdateMeasurementGroup: function(id) {
		var $data = {'id': id};
		AjaxRequest('/measurements/update-group', $data);
		// AjaxRequestCount('/stock-count/update-count', $data);
	},
	DeleteMeasureItems: function(id) {
		var $data = {'id': id, 'name': 'items'};
		AjaxRequest('/measurements/delete-items', $data);
	},
	DeleteMeasureGroup: function(id) {
		var $data = {'id': id, 'name': 'measureGroup'};
		AjaxRequest('/measurements/delete-group', $data);
	}
};

var Ajax = {
	// Used to send custom queries to Linnworks
	SQLQuery: function(userData) {
		return AjaxRequestNew({'dataArray': {script: userData}, 'linnworksURL': 'Dashboards/ExecuteCustomScriptQuery'});
	}
};

var ApiPurchaseOrders = {
	// Search for a purchase order using the pkPurchaseID, Use a custom query to search by poNumber
	Search: function(pkPurchaseID) {
		return AjaxRequest({'userInput': 'pkPurchaseId', 'userData': pkPurchaseID, 'linnworksURL': 'PurchaseOrder/Get_PurchaseOrder'});
	},
	GetNotes: function(pkPurchaseID) {
		return AjaxRequest({'userInput': 'pkPurchaseId', 'userData': pkPurchaseID, 'linnworksURL': 'PurchaseOrder/Get_PurchaseOrderNote'});
	},
	AddNotes: function(pkPurchaseID, note) {
		return AjaxRequestNew({'dataArray': {'pkPurchaseId': pkPurchaseID, 'Note': note}, 'linnworksURL': 'PurchaseOrder/Add_PurchaseOrderNote'});
	}
};

var Stock = {
	UpdateLevels: function(newQtyArray) {
		var itemArray = []
		$.each(newQtyArray, function(i) {
			itemArray.push({"SKU":newQtyArray[i].sku, "LocationId": "00000000-0000-0000-0000-000000000000", "Level": parseInt(newQtyArray[i].countNo)});
		});
		var itemArrayStr = JSON.stringify(itemArray);
		return AjaxRequestTest({'dataArray': {stocklevels: itemArrayStr}, 'linnworksURL': 'Stock/SetStockLevel'});
	},
	BinRack: function(itemBinRackArray, type) {
		var url;
		if(type === 'new') { url = 'Inventory/AddItemLocations'; } else if(type === 'update') { url = 'Inventory/UpdateItemLocations'; }
		AjaxRequestNew({'dataArray': {itemLocations: itemBinRackArray}, 'linnworksURL': url});
	},
	InventoryExtendedProperty: function(propertyName, extendedPropertyArray) {
		var newExtendedPropertyArray = [];
		var updateExtendedPropertyArray = [];

		$.each(extendedPropertyArray, function(i) {
			if(extendedPropertyArray[i].lastChecked === 'null') {
				newExtendedPropertyArray.push({"fkStockItemId":extendedPropertyArray[i].StockItemID, "ProperyName": propertyName, "PropertyValue":moment().format('YYYY-MM-DD'), "PropertyType":"Other"});
			} else {
				updateExtendedPropertyArray.push({"pkRowId":extendedPropertyArray[i].countRowID, "fkStockItemId":extendedPropertyArray[i].StockItemID, "ProperyName": propertyName, "PropertyValue":moment().format('YYYY-MM-DD'), "PropertyType":"Other"});
			}
		});

		if(newExtendedPropertyArray.length > 0) {
			var itemArrayStr = JSON.stringify(newExtendedPropertyArray);
			AjaxRequestNew({'dataArray': {inventoryItemExtendedProperties: itemArrayStr}, 'linnworksURL': 'Inventory/CreateInventoryItemExtendedProperties'});	
		}

		if(updateExtendedPropertyArray.length > 0) {

			var itemArrayStr = JSON.stringify(updateExtendedPropertyArray);
			AjaxRequestNew({'dataArray': {inventoryItemExtendedProperties: itemArrayStr}, 'linnworksURL': 'Inventory/UpdateInventoryItemExtendedProperties'});	
		}

		// AjaxRequestNew({'dataArray': {inventoryItemExtendedProperties: extendedPropertyArray}, 'linnworksURL': 'Inventory/CreateInventoryItemExtendedProperties'});	
	},
	UpdateInventoryItem: function(updateArray) {
		var itemArrayStr = JSON.stringify(updateArray);
		return AjaxRequestTest({'dataArray': {inventoryItem: itemArrayStr}, 'linnworksURL': 'Inventory/UpdateInventoryItem'});
	}
};