var $inputArray = [];
var $tableStr;
var $createdBy;
var $labelType = '';
var $labelsPerPage = '';
var $poNumber;

var mytoken = $('[name=csrf-token]').attr('content');

function clearBarcodeData() {
	$('.manual-entry-input-group').empty();
	$tableStr = '';
	$queryResults.length = 0;
	$('#bc-create-print-buttons, #number-of-labels-options').prop('hidden', true);
	$('#label-type-form input, #labels-per-page-form input').prop('checked', false);
	$('#barcode-search-results').empty();
	$('#print-preview-barcodes').empty();
	$inputArray.length = 0;
}

function CreateTable(type) {
	$tableStr = '<table class="table"><thead class="thead-inverse">';
	$tableStr += '<tr>'

	if (type === 'poGen') {
		$tableStr += '<th>Barcode</th>'
	}

	$tableStr += '<th>Supplier Code</th><th>Code</th><th>Colour</th><th>Size</th>';

	if (type === 'poNumber') {
		$tableStr += '<th>Qty</th>';
	} else if (type === 'poGen') {
		$tableStr += '<th>Qty</th><th>Correct</th><th>Booked In</th>'
	}

	$tableStr += '</tr></thead><tbody>';

	$.each($queryResults, function(i) {
		$tableStr += '<tr>';

		if (type === 'poGen') {
			$tableStr += '<td class="barcode-image-' + i + ' barcode-image" valign="middle"></td>'
		}

		$tableStr += '<td>' + $queryResults[i].SupplierCode + '</td>';
		$tableStr += '<td>' + $queryResults[i].Code + '</td>';
		$tableStr += '<td>' + $queryResults[i].Colour + '</td>';
		$tableStr += '<td>' + $queryResults[i].Size + '</td>';

		if (type === 'poNumber') {
			$tableStr += '<td>' + $queryResults[i].Delivered + '</td>';
		} else if (type === 'poGen') {
			$tableStr += '<td>' + $queryResults[i].Due + '</td>';
			$tableStr += '<td>' + $queryResults[i].DeliveredNew + '</td>';
			$tableStr += '<td>' + $queryResults[i].BookedIn + '</td>';
		}

		$tableStr += '</tr>';
		$qtyArray.push(parseInt($queryResults[i].Delivered));
	});
	$tableStr += '</tbody></table>';
}


/*** Search by PO Number JS ***/

$(document).on('click', '#create-barcode-options div a', function(e) {
	e.preventDefault();
	var btnTxt = $(this).html();
	$('#create-barcode-options button').html(btnTxt);
	$('#label-type-options').removeAttr('hidden');
		
	if ($(this).index() === 0) {
		clearBarcodeData();
		$createdBy = 'poNumber';
		$('#search-by-po-inputs').removeAttr('hidden');
		$('.manual-entry-input-group').prop('hidden', true);
		$('#add-row-button').prop('hidden', true);
	} else if ($(this).index() === 1) {
		var initialManualInputs = '<div class="row" id="manual-entry-inputs-0"><div class="col col-6"><input id="barcode-0" type="text" class="form-control barcode-number" name="BarcodeNumber" placeholder="Barcode Number.............."></div><div class="col col-2"><input id="number-of-barcodes-0" type="text" class="form-control product-name" name="number" placeholder="No"></div><div class="col col-4"><p id="barcode-ref-0" class="form-control-static barcode-ref"></p></div></div>';
		clearBarcodeData();
		$('.manual-entry-input-group').html(initialManualInputs);
		$createdBy = $(this).val();
		$createdBy = 'manual';
		$('.manual-entry-input-group').removeAttr('hidden');
		$('#add-row-button').removeAttr('hidden');
		$('#search-by-po-inputs').prop('hidden', true);
	}
});

$(document).on('change', '#label-type-options input', function() {
	$labelType = $(this).val();
	
	$('#label-type-options input').attr("checked", false);
	$(this).attr("checked", "checked");

	if($labelType === 'labelPage') {
		$('#number-of-labels-options').removeAttr('hidden');
	} else {
		$('#number-of-labels-options').prop('hidden', true);
	}
});

$(document).on('click', '#search-by-po-inputs button', function() {

	$('#barcode-search-results').empty();
	$tableStr = '';
	$queryResults.length = 0;

	$('#loading-overlay').removeAttr('hidden');
	if($('#tb-barcode-po-search').val().length === 0) {
		alert('Please enter a PO number into the textbox');
	} else {
		$poNumber = $('#tb-barcode-po-search').val();

		BarcodesPOSearch($poNumber);
		SQLQuery($customQuery);

		// $.ajax({
		// 	headers: {'X-CSRF-TOKEN': mytoken},
		// 	url: '/custom-query',
		// 	method: 'post',
		// 	data: { 'sqlQuery': data }
		// })
		// .done(function(response) {

		// 	return response;

			$.each($ajaxResults.Results, function( i ) {
				$queryResults.push($ajaxResults.Results[i]);
			});

			CreateTable($createdBy);
			$('#barcode-search-results').removeAttr('hidden');
			$('#barcode-search-results').html($tableStr);
			$('#bc-create-print-buttons').removeAttr('hidden');
			$('#loading-overlay').prop('hidden', true);
		// });
	}
});

$(document).on('change', '#labels-per-page-form input', function() {
	$('#labels-per-page-form input').attr("checked", false);
	$(this).attr("checked", "checked");
	$labelsPerPage = parseInt($(this).val());

});


/*** Search manually JS ***/

$(document).on('click', '#add-new-row-btn', function() {
	var createNewManualInput;
	var newElementIdNo = $('.manual-entry-input-group .row').length;

	createNewManualInput = '<div class="row mt-1" id="manual-entry-inputs-' + newElementIdNo + '">';
	createNewManualInput += '<div class="col col-6">'
	createNewManualInput += '<input id="barcode-' + newElementIdNo + '" type="text" class="form-control barcode-number" name="BarcodeNumber" placeholder="Barcode Number..............">';
	createNewManualInput += '</div>';
	createNewManualInput += '<div class="col col-2">';
	createNewManualInput += '<input id="number-of-barcodes-' + newElementIdNo + '" type="text" class="form-control product-name" name="number" placeholder="No">';
	createNewManualInput += '</div>';
	createNewManualInput += '<div class="col col-4">';
	createNewManualInput += '<p id="barcode-ref-' + newElementIdNo + '" class="form-control-static barcode-ref"></p>';
	createNewManualInput += '</div>';
	createNewManualInput += '</div>';
	$('.manual-entry-input-group').append(createNewManualInput);
});

$(document).on('change', '.manual-entry-input-group .barcode-number', function() {
	var barcodeNumber = $(this).val();
	var referenceId = $(this).parent().parent().index();
	ReferenceData(barcodeNumber);
	SQLQuery($customQuery);

	$('.barcode-ref:eq('+referenceId+')').html($ajaxResults.Results[0].ReferenceName);
		
	$('#bc-create-print-buttons').removeAttr('hidden');
});

function ClearArray() {
	$queryResults = [];
	$inputArray = [];	
	$qtyArray = [];
}

$(document).on('click', '#create-btn', function() {
	$('#print-preview-barcodes').empty();

	if($labelType === '') {
		alert('Please select your paper type!');
	} else if($labelType === 'labelPage' && $labelsPerPage === '') {
		alert('Please select the number of labels per page!');
	} else {
		if($createdBy === 'manual') {
			ClearArray();
			errors = 0;
			$('.barcode-number').each(function( i ) {
				myVal = $('#number-of-barcodes-'+i).val();
				myBarcode = $('#barcode-'+i).val();
				if(parseInt(myVal)>0 && myBarcode!==''){
					$inputArray.push({ BarcodeNumber: myBarcode, number: parseInt(myVal)});	
				}else{
					errors++;
					if(parseInt(myVal) <= 0) {
						$('#number-of-barcodes-'+i).addClass('has-error');
					} else if (myBarcode === '') {
						$('#barcode-'+i).addClass('has-error');
					}
					
				}
			});
			if(errors > 0){
				alert('Errors!!!! please fill all the inputs!!');
				return false;
			}

			CompareDataWithLinnworks();

		} else if($createdBy === 'poNumber') {
			console.table($queryResults);
			$barcodeData = $queryResults;
			console.table($barcodeData);
			createBarcodeElements();
		}
	}
});

$(document).on('click', '#print-btn', function() {
	$('#print-preview-barcodes').printThis({
		debug: true,               // show the iframe for debugging
		importStyle: false,         // import style tags
		loadCSS: 'http://warehouse-apps.dev/css/barcode.css',  // Development css
		// loadCSS: 'http://roco-warehouse.co.uk/css/app/barcode.css', // Live css
		// removeInline: true,       // remove all inline styles from print elements
		header: null,               // prefix to html
		footer: null,               // postfix to html
		base: true,                // preserve the BASE tag, or accept a string for the URL
		formValues: true,           // preserve input/form values
		canvas: false,              // copy canvas elements (experimental)
		removeScripts: false        // remove script tags from print content
	});

	// var printContent = $('#print-preview-barcodes').html();
	// var originalContent = $('body').html()
	// $('body').html(printContent);
	// window.print();
	// $('body').html(originalContent);

	// PrintPopup($('#print-preview-barcodes').html());
});

// function PrintPopup(data) {
// 	var mywindow = window.open('', 'new div', 'height=400,width=600');
// 	mywindow.document.write('<html><head><title></title>');
// 	mywindow.document.write('<link href="http://warehouse-apps.dev/css/app.css" rel="stylesheet" media="print">');
// 	mywindow.document.write('</head><body >');
// 	mywindow.document.write(data);
// 	mywindow.document.write('</body></html>');
// 	mywindow.document.close();
// 	mywindow.focus();
// 	mywindow.print();
// 	mywindow.close();

// 	return true;
// }


/*********************************************
*** PO Generator *****************************
*********************************************/

// var $printPreviewStr = '';
// var $supplierName = '';

// /** PO Search *********/

// $('#btn-po-gen').click(function() {
// 	if (($('#tb-po-gen-search').val()).length > 0) {
// 		$sqlSelect = "si.BarcodeNumber, sup.SupplierName, itsu.SupplierCode, sc.Code, scr.Colour, sz.Size, pi.Quantity - pi.Delivered AS 'Due', '' AS 'DeliveredNew', '' AS 'BookedIn'";
// 		$sqlTable = 'Purchase pur';
// 		$sqlJoiningTables = "INNER JOIN PurchaseItem pi ON pur.pkPurchaseID = pi.fkPurchasId INNER JOIN StockItem si ON pi.fkStockItemId = si.pkStockItemID INNER JOIN ItemSupplier itsu ON pi.fkStockItemId = itsu.fkStockItemId INNER JOIN Supplier sup ON pur.fkSupplierId = sup.pkSupplierID FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyst', siep.ProperyName, siep.ProperyValue AS 'Type' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.ItemType') st ON si.pkStockItemID = st.fkeyst FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysn', siep.ProperyName, siep.ProperyValue AS 'Name' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.name') sn ON si.pkStockItemID = sn.fkeysn FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysc', siep.ProperyName, siep.ProperyValue AS 'Code' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.code') sc ON si.pkStockItemID = sc.fkeysc FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyscr', siep.ProperyName, siep.ProperyValue AS 'Colour' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.colour') scr ON si.pkStockItemID = scr.fkeyscr FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyson', siep.ProperyName, siep.ProperyValue AS 'OrderNo' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.OrderNo') son ON si.pkStockItemID = son.fkeyson FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysz', siep.ProperyName, siep.ProperyValue AS 'Size' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.Size') sz ON si.pkStockItemID = sz.fkeysz ";
// 		$sqlSearchParameter = " pi.Quantity - pi.Delivered > 0 AND itsu.isDefault = 1 AND pur.ExternalInvoiceNumber = '" + $('#tb-po-gen-search').val() + "' ORDER BY st.Type, sc.Code, scr.Colour, son.OrderNo";
// 		CustomQuery();
// 		$('#btn-po-gen').addClass('disabled');
// 	} else {
// 		alert('Please enter a PO number!');
// 	}

// 	$supplierName = $queryReturn.Results[0].SupplierName;
// 	CycleResultsToTable('poGen');
// 	$('#po-gen-results').html($tableStr);

// 	$.each($queryReturn.Results, function (i) {
// 		$('.barcode-image-' + i).barcode($queryReturn.Results[i].BarcodeNumber, 'ean13', {
// 			barWidth: 1,
// 			barHeight: 30,
// 			showHRI: false,
// 			output: 'css'
// 		}); 
// 	});

// 	$('#barcode-create-print-group').css('display', 'block');
// 	$('#create-print-po-group').css('display', 'flex');
// });

// $('#create-po-btn').click(function() {
// PrintPreview('po-generator');
// $('#print-po-btn').css('display', 'inline-block');
// $('#create-po-btn').addClass('disabled');
// });

// function PoPrintPreview() {
// 	$printPreviewStr = '<div class="container" id="po-wrapper">';
// 	$printPreviewStr += '<div class="container" id="po-header">';
// 	$printPreviewStr += '<div class="row justify-content-center">';
// 	$printPreviewStr += '<p class="h1">Purchase Order for Due Stock</p>';
// 	$printPreviewStr += '</div>';
// 	$printPreviewStr += '<div class="row">';
// 	$printPreviewStr += '<p class="h4">' + $('#tb-po-gen-search').val() + ' - ' + $supplierName + '</p>';
// 	$printPreviewStr += '</div>';
// 	$printPreviewStr += '</div>';
// 	$printPreviewStr += '<div class="row justify-content-center" id="po-table">';
// 	$printPreviewStr += $tableStr;
// 	$printPreviewStr += '</div>';
// 	$printPreviewStr += '<div class="container justify-content-end" id="po-footer">';
// 	$printPreviewStr += '<div class="row justify-content-end">';
// 	$printPreviewStr += '<p>Signed by: ________________________________</p>';
// 	$printPreviewStr += '</div>';
// 	$printPreviewStr += '<div class="row justify-content-end">';
// 	$printPreviewStr += '<p>Date: __ __ / __ __ / __ __</p>';
// 	$printPreviewStr += '</div>';
// 	$printPreviewStr += '</div>';
// 	$printPreviewStr += '</div>';

// 	$('#print-preview-po-gen').html($printPreviewStr);

// 	$.each($queryReturn.Results, function (i) {
// 		$('.barcode-image-' + i).barcode($queryReturn.Results[i].BarcodeNumber, 'ean13', {
// 			barWidth: 1,
// 			barHeight: 30,
// 			showHRI: false,
// 			output: 'css'
// 		}); 
// 	});
// }