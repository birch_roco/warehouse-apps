$(document).ready(function() {
	var inputGroup;
	var searchType;
	var labelsPerRow;
	var rowsPerPage;
	var searchResults;
	var barcodeData;

	// Enter key lets the user search
	$(document).keydown(function(e) {
		if($('#name-po-search input').is(':focus') && e.which === 13) {
			e.preventDefault();

			if($('#name-po-search input').val().length > 0) {
				$('#name-po-search button').click();
			}			
		}
	});

	// Chooses the correct custom query to use depending on search type
	function searchForProducts(data, type) {
		let barcodeData = [];

		switch(type) {
			case 'po-number':
				barcodeData = BarcodeGen.SearchByPOs(data);
				break;
			case 'by-name':
				barcodeData = BarcodeGen.SearchByName(data);
				break;
			case 'by-barcode':
				barcodeData = BarcodeGen.SearchByBarcode(data);
				break;
		}

		if(type !== 'by-barcode') {
			$('#search-results').html(CreateResultsTable(barcodeData.Results));
		}
		$('#search-results').attr('hidden', false);
		$('#paper-type-rb').attr('hidden', false);

		return barcodeData.Results;
	}

	// Create the results table
	function CreateResultsTable(data) {
		let tableStr = '<table class="table"><thead class="thead-inverse"><tr>';

		if(searchType == 'by-name') {
			tableStr += '<th id="select-col" class="text-center">Select</td></th>';
		}

		tableStr += '<th>Product Code</th><th id="qty-col">Qty</th></tr></thead><tbody>';

		$(data).each(function(i) {
			tableStr += '<tr id="' + data[i].pkStockItemID + '">'
			if(searchType == 'by-name') {
				tableStr += '<td class="text-center"><input class="product-selection" type="checkbox"></td>';
			}

			tableStr += '<td>' + data[i].SupplierCode + '</td><td class="barcode-qty">';

			if(searchType == 'by-name') {
				tableStr += '<input type="text" disabled>';	
			} else {
				tableStr += data[i].Delivered;
			}

			tableStr += '</td></tr>';
		});

		tableStr += '</tbody>';
		if(searchType == 'by-name') {
			tableStr += '<tfoot><tr><td><input class="product-selection-all" type="checkbox"> Select All</td><td></td><td></td></tr></tfoot>';
		}
		tableStr += '</table>'

		return tableStr;
	}

	// Look up and shorten names to fit on the labels

	function TypeComparison(total, data) {
		console.table(data);
		// $name.length = 0;

		// $.each($barcodeData, function( i ) {
		// 	var match = 0;
		// 	var j = 0;
		// 	while(match === 0) {
		// 		if (data[i].Type === $lookup[j].Type) {
		// 			$name.push($lookup[j].Abbr);
		// 			match = 1;
		// 		} else if (j === $lookup.length-1) {
		// 			$name.push(data[i].Name + ' - ' + data[i].Type);
		// 			break;
		// 		}
		// 		j++;
		// 	}
		// });

		// createBarcodes();
	}

	// Create barcode elements
	function CreateBarcodeElements(total, data) {
		let labelsPerPage = parseInt(labelsPerRow) * parseInt(rowsPerPage);
		let labelCounter = 1;
		let barcodeElementStr = '';
		let rowCounter = 1;
		let pageCounter = 1;
		let barcodeCounter = 0;
		let barcodeId = 0;
		let productType = '';

		const labelsType = (labelsPerPage === 1) ? 'labelRoll' : 'labelPage labels-per-page-' + labelsPerPage;

		for (let i = 0; i < total; i++) {

			if(labelCounter === 1) {
				barcodeElementStr += '<div class="barcode-group ' + labelsType + '" id="page-no-' + pageCounter + '">';
				barcodeElementStr += '<div class="barcode-row">';
			}
				
			barcodeElementStr += '<div class="barcode-info barcode-id-' + barcodeId + '"><div></div>';
			productType = data[barcodeId].Type;
			$(lookup).each(function(i) {
				if(data[barcodeId].Type === lookup[i].Type) {
					// barcodeElementStr += '<p class="product-name">' + data[barcodeId].Name + ' - ' + lookup[i].Abbr + '</p>';
					productType = lookup[i].Abbr;
				}
			});
			barcodeElementStr += '<p class="product-name">' + data[barcodeId].Name + ' - ' + productType + '</p>';
			barcodeElementStr += '<p>' + data[barcodeId].Code + '-' + data[barcodeId].Colour + '-' + data[barcodeId].Size + '</p>';
			barcodeElementStr += '</div>';
				
			console.log('Row Counter: ' + rowCounter + ', Label Counter:' + labelCounter + ', Labels Per Row: ' + labelsPerRow + ', Label Counter % Labels Per Row: ' + labelCounter % labelsPerRow);
			if(labelCounter % labelsPerRow === 0 ) {
				console.log('pain');
				rowCounter ++;
				if(rowCounter < parseInt(rowsPerPage) || $('#paper-type-rb input:checked').val() === 'labels-roll') {
					barcodeElementStr += '</div>';
					barcodeElementStr += '<div class="barcode-row">';
				}
			}

			barcodeCounter ++;
			labelCounter ++;

			if(barcodeCounter === parseInt(data[barcodeId].BarcodeQty)) {
				barcodeId ++;
				barcodeCounter = 0;
			}

			if(labelCounter === labelsPerPage+1 && $('#paper-type-rb input:checked').val() === 'labels-sheet') {
				barcodeElementStr += '</div>';
				barcodeElementStr += '<div class="page-break"></div>';
				labelCounter = 1;
				rowCounter = 1;
				pageCounter ++;
			}
		}
		barcodeElementStr += '</div></div>';
		
		$('#print-preview-barcodes').html(barcodeElementStr);
		$('#print-preview-barcodes').attr('hidden', false);

		// Creates the barcode image
		$.each(data, function(i) {
			if (labelsPerPage === 8) {
				$('.barcode-id-'+i+' div').barcode(data[i].BarcodeNumber, 'ean13', {
				barWidth: 3,
				barHeight: 80,
				showHRI: false,
				output: 'css'
				}); 
			} else if (labelsPerPage === 24) {
				$('.barcode-id-'+i+' div').barcode(data[i].BarcodeNumber, 'ean13', {
				barWidth: 2,
				barHeight: 40,
				showHRI: false,
				output: 'css'
				}); 
			} else if (labelsPerPage === 65) {
				$('.barcode-id-'+i+' div').barcode(data[i].BarcodeNumber, 'ean13', {
				barWidth: 1,
				barHeight: 10,
				showHRI: false,
				output: 'css'
				}); 
			} else if (labelsPerPage === 1) {
				$('.barcode-id-'+i+' div').barcode(data[i].BarcodeNumber, 'ean13', {
				barWidth: 2,
				barHeight: 30,
				showHRI: false,
				output: 'css'
				}); 
			}
		});
	}

	// // Gets the data to create the barcodes
	// function getBarcodeData() {
	// 	let productArray = [];
	// 	switch(searchType) {
	// 		case 'po-number':
	// 			$.each('#search-results tr', function(i) {
	// 				productArray.push($(this).attr('id'));
	// 			});
	// 			break;
	// 		case 'by-name':
	// 			$.each('#search-results tr', function(i) {
	// 				productArray.push($(this).attr('id'));
	// 			});
	// 			break;
	// 		case 'by-barcode':
	// 			barcodeData = BarcodeGen.SearchByBarcode(data);
	// 			break;
	// 	}
	// }


	// Display inputs for the type of search selected
	$(document).on('click', '#search-method a', function() {
		inputGroup = $(this).attr('barcode-input-group');
		searchType = $(this).attr('search-type');

		$('#search-results, #create-barcodes').attr('hidden', true);

		$('#name-po-search input').val('');
		$(this).parents('#search-method').find('button').html($(this).html());

		$('.user-input').each(function() {
			$(this).attr('hidden', true);

			if($(this).attr('id') === inputGroup) {
				$(this).attr('hidden', false);
			}
		});
	});

	// Using the search bar
	$(document).on('click', '#name-po-search button', function() {
		if($(this).parents('#name-po-search').find('input').val().length === 0) {
			alert('Please enter a value into the search box!');
		} else {
			searchResults = searchForProducts($(this).parents('#name-po-search').find('input').val(), searchType);
		}
	});

	// individual checkboxes
	$(document).on('change', '.product-selection', function() {
		if($(this).parents('tr').find('.barcode-qty input').attr('disabled')) {
			$(this).parents('tr').find('.barcode-qty input').attr('disabled', false);
		} else {
			$(this).parents('tr').find('.barcode-qty input').attr('disabled', true);
		}
	});

	// select all checkboxes
	$(document).on('change', '.product-selection-all', function() {
		if($(this).is(':checked')) {
			$('.product-selection').prop('checked', true);
			$('.barcode-qty input').attr('disabled', false);
		} else {
			$('.product-selection').prop('checked', false);
			$('.barcode-qty input').attr('disabled', true);
		}
	});

	// Search for a product by the barcode
	$(document).on('change', '#barcode-number-input', function() {
		searchResults = searchForProducts($(this).val(), searchType);
		$(this).parents('#barcode-inputs').find('span').html(searchResults[0].SupplierCode);
	});

	// Add new row when + button is clicked
	$(document).on('click', '#add-barcode-inputs button', function() {
		let barcodeInputs;
		let inputCount = $('.barcode-input-group').length;
		let emptyInputs = 0;
		
		$('#barcode-inputs input').each(function() {
			$(this).css('border', '1px solid #00000026');

			if($(this).val() === '') {
				$(this).css('border', '1px red solid');
				emptyInputs ++;
			}
		});

		if(emptyInputs == 0) {
			barcodeInputs = '<div class="row barcode-input-group">';
			barcodeInputs += '<div class="col col-5">';
			barcodeInputs += '<input type="text" class="form-control" placeholder="Barcode Number" id="barcode-number-input">';
			barcodeInputs += '</div>';
			barcodeInputs += '<div class="col col-2 barcode-qty">';
			barcodeInputs += '<input type="text" class="form-control" placeholder="Qty" class="barcode-qty">';
			barcodeInputs += '</div>';
			barcodeInputs += '<div class="col col-5">';
			barcodeInputs += '<span></span>';
			barcodeInputs += '</div>';
			barcodeInputs += '</div>';

			$('#barcode-inputs .barcode-input-group:nth-child('+inputCount+')').after(barcodeInputs);
		}
	});

	// Choose Paper type
	$(document).on('change', '#paper-type-rb input', function() {
		$('#labels-per-page-rb').attr('hidden', true);
		$('#create-barcodes').attr('hidden', true);

		if($(this).attr('id') == 'label-sheets-rb') {
			$('#labels-per-page-rb').attr('hidden', false);
		} else if($(this).attr('id') == 'label-roll-rb') {
			$('#create-barcodes').attr('hidden', false);
		}
	});

	// Choose number of labels
	$(document).on('change', '#labels-per-page-rb', function() {
		$('#create-barcodes').attr('hidden', false);
	});

	// Create print preview
	$(document).on('click', '#create-barcodes button', function() {
		barcodeData = [];
		let barcodePrintStr;
		let barcodeTotal = 0;

		if($('#paper-type-rb input:checked').val() === 'labels-sheet') {
			labelsPerRow = $('#labels-per-page-rb input:checked').attr('labels-per-row');
			rowsPerPage = $('#labels-per-page-rb input:checked').attr('rows-per-page');
		} else {
			labelsPerRow = $('#paper-type-rb input:checked').attr('labels-per-row');
			rowsPerPage = $('#paper-type-rb input:checked').attr('rows-per-page');
		}

		// Loop through inputs or static values to get the total number of barcodes
		$('.barcode-qty').each(function(i) {
			if(searchType === 'po-number') {
				barcodeTotal += parseInt($(this).html());
			} else if(searchType === 'by-name' && $(this).parents('tr').find('.product-selection').is(':checked')) {
				barcodeTotal += parseInt($(this).find('input').val());
			}
		});

		// Loops throught the rows and 
		$('#search-results tbody tr').each(function(i) {
			if((searchType === 'by-name' && $(this).find('.product-selection').is(':checked')) || searchType === 'po-number') {
				let rowId = $(this).attr('id');

				const barcodeQty = (searchType === 'po-number') ? $('#'+rowId).find('.barcode-qty').html() : $('#'+rowId).find('.barcode-qty input').val();

				$.each(searchResults, function(i) {
					if(rowId === searchResults[i].pkStockItemID) {
						barcodeData.push({
							pkStockItemID: searchResults[i].pkStockItemID,
							BarcodeNumber: searchResults[i].BarcodeNumber,
							Type: searchResults[i].Type,
							Name: searchResults[i].Name,
							Code: searchResults[i].Code,
							Colour: searchResults[i].Colour,
							Size: searchResults[i].Size,
							BarcodeQty: barcodeQty
						});
					}
				});
			}
		});

		CreateBarcodeElements(barcodeTotal, barcodeData);
		// TypeComparison(barcodeTotal, barcodeData);

		$('#print-barcodes').attr('hidden', false);
	});

	$(document).on('click', '#print-barcodes button', function() {
		$('#print-preview-barcodes').printThis({
			debug: false,               // show the iframe for debugging
			importStyle: false,         // import style tags
			loadCSS: 'http://warehouse-apps.dev/css/barcode.css',  // Development css
			// loadCSS: 'http://roco-warehouse.co.uk/css/app/barcode.css', // Live css
			// removeInline: true,       // remove all inline styles from print elements
			header: null,               // prefix to html
			footer: null,               // postfix to html
			base: true,                // preserve the BASE tag, or accept a string for the URL
			formValues: true,           // preserve input/form values
			canvas: false,              // copy canvas elements (experimental)
			removeScripts: false        // remove script tags from print content
		});
	});
});