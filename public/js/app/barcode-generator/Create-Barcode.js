var x = 0;
var $labelType;
var $labelsPerPage;
var $noOfBarcodes = 0;
var $barcodeData = [];
var $buildPage;
var $qtyArray = [];
var $name = [];

function PageLayout(labelType, labelsPerPage) {
	if (labelType === 'labelPage') {
		if (labelsPerPage === 8) {
			$('.barcode-group').removeClass('labels-per-page-24').removeClass('labels-per-page-65').addClass('labels-per-page-8');
		} else if (labelsPerPage === 24) {
			$('.barcode-group').removeClass('labels-per-page-65').removeClass('labels-per-page-8').addClass('labels-per-page-24');
		} else if (labelsPerPage === 65) {
			$('.barcode-group').removeClass('labels-per-page-24').removeClass('labels-per-page-8').addClass('labels-per-page-65');
		} 
	}

	TypeComparison();
}

function createBarcodeElements() {
	var $pageCounter = 1;
	var $barcodeCounter = 0;
	var $labelCounter = 0;

	if ($labelType === 'labelPage') {
		if ($labelsPerPage === 8) { x = 2; } else if ($labelsPerPage === 24) { x = 3; } else if ($labelsPerPage === 65) { x = 5; } 
	} else if ($labelType === 'labelRoll') {
		x = 1;
	}

	// Adds the number input boxes together to find the total number of barcodes
	for (i=0; i<=$barcodeData.length; i++) {
		$noOfBarcodes = $noOfBarcodes + parseInt($qtyArray[i]);
	}

	// Counting the amount of inputs
	for (i=0; i<$qtyArray.length; i++) { 

	// Creates the initial div to store the barcodes in
	if (i === 0) {
		$buildPage = '<div class="barcode-group ' + $labelType + '" id="page-no-' + $pageCounter +'">';
	}

	for (j=0; j<$qtyArray[$barcodeCounter]; j++) {

	if ($labelCounter === $labelsPerPage) {
		$pageCounter++;
		$buildPage += '</div></div><div class="page-break"></div><div class="barcode-group ' + $labelType + '" id="page-no-' + $pageCounter +'">';
		$labelCounter = 0;
	}

	if ($labelCounter === 0) {
		$buildPage += '<div class="barcode-row">';
	} else if ($labelCounter % x === 0) {
		$buildPage += '</div><div class="barcode-row">';
	}

	$buildPage += '<div class="barcode-info barcode-id-' + $barcodeCounter + '"><div></div></div>'
	$labelCounter++;

	}

	$barcodeCounter++;
	}

	$('#print-preview-barcodes').html($buildPage);

	PageLayout($labelType, $labelsPerPage);
}

function createBarcodes() {
	$.each( $barcodeData, function( i ) {
		if($labelType === 'labelPage') {
			if ($labelsPerPage === 8) {
				$('.barcode-id-'+i+' div').barcode($barcodeData[i].BarcodeNumber, 'ean13', {
				barWidth: 3,
				barHeight: 80,
				showHRI: false,
				output: 'css'
				}); 
			} else if ($labelsPerPage === 24) {
				$('.barcode-id-'+i+' div').barcode($barcodeData[i].BarcodeNumber, 'ean13', {
				barWidth: 2,
				barHeight: 40,
				showHRI: false,
				output: 'css'
				}); 
			} else if ($labelsPerPage === 65) {
				$('.barcode-id-'+i+' div').barcode($barcodeData[i].BarcodeNumber, 'ean13', {
				barWidth: 1,
				barHeight: 10,
				showHRI: false,
				output: 'css'
				}); 
			} 
		} else if ($labelType === 'labelRoll') {
			$('.barcode-id-'+i+' div').barcode($barcodeData[i].BarcodeNumber, 'ean13', {
			barWidth: 2,
			barHeight: 30,
			showHRI: false,
			output: 'css'
			}); 
		}
	});

	$.each( $barcodeData, function( i ) {
		$('.barcode-id-'+i).append('<p class="product-name">'+ $name[i] + '</p>');
	});

	$.each( $barcodeData, function( i ) {
		var productCode

		productCode = $barcodeData[i].Code;
		productCode += '-' + $barcodeData[i].Colour;

		if ($barcodeData[i].Size != 'One Size'){
			productCode += '-' + $barcodeData[i].Size;
		}

		$('.barcode-id-'+i).append('<p>' + productCode + '</p>');
	});
}

function TypeComparison() {
	$name.length = 0;

	$.each($barcodeData, function( i ) {
		var match = 0;
		var j = 0;
		while(match === 0) {
			if ($barcodeData[i].Type === lookup[j].Type) {
				$name.push(lookup[j].Abbr);
				match = 1;
			} else if (j === lookup.length-1) {
				$name.push($barcodeData[i].Name + ' - ' + $barcodeData[i].Type);
				break;
			}
			j++;
		}
	});

	createBarcodes();
}