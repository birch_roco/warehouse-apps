var lookup = [
{Type: 'Dickie-Banded',	Abbr: 'Dickie-Band'},
{Type: 'Dickie-Elasticated', Abbr: 'Dickie-Elas'},
{Type: 'Dickie-Thin Band',	Abbr: 'Dickie-Thin'},
{Type: 'Hanky',	Abbr: 'Hanky'},
{Type: 'Tie-Banded', Abbr: 'Tie-Band'},
{Type: 'Tie-Classic-Full',	Abbr: 'Tie-Class-Full'},
{Type: 'Tie-Clip On', Abbr: 'Tie-Clip On'},
{Type: 'Tie-Elasticated', Abbr: 'Tie-Elas'},
{Type: 'Tie-Full', Abbr: 'Tie-Full'},
{Type: 'Tie', Abbr: 'Tie'},
{Type: 'Tie-Skny-Elasticated', Abbr: 'Tie-Skny-Elas'},
{Type: 'Tie-Skny-Full',	Abbr: 'Tie-Skny-Full'},
{Type: 'Tie-Slim-Elas',	Abbr: 'Tie-Sl-Elas'},
{Type: 'Tie-Slim-Full',	Abbr: 'Tie-Sl-Full'}
];