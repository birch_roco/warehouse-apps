var $queryResults = [];

function CompareDataWithLinnworks() {
	var $tempWhere;

	$.each($inputArray, function( i ) {
		if (i === 0) {
			$tempWhere = "si.BarcodeNumber = '" + $inputArray[i].BarcodeNumber + "' ";
		} else {
			$tempWhere += "OR si.BarcodeNumber = '" + $inputArray[i].BarcodeNumber + "' ";
		}
	});

	$sqlSelect = "si.BarcodeNumber, st.Type, sn.Name, sc.Code, scr.Colour, sz.Size ";
	$sqlTable = 'StockItem si ';
	$sqlJoiningTables = "FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyst', siep.ProperyName, siep.ProperyValue AS 'Type' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.ItemType') st ON si.pkStockItemID = st.fkeyst FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysn', siep.ProperyName, siep.ProperyValue AS 'Name' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.name') sn ON si.pkStockItemID = sn.fkeysn FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysc', siep.ProperyName, siep.ProperyValue AS 'Code' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.code') sc ON si.pkStockItemID = sc.fkeysc FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyscr', siep.ProperyName, siep.ProperyValue AS 'Colour' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.colour') scr ON si.pkStockItemID = scr.fkeyscr FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyson', siep.ProperyName, siep.ProperyValue AS 'OrderNo' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.OrderNo') son ON si.pkStockItemID = son.fkeyson FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysz', siep.ProperyName, siep.ProperyValue AS 'Size' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.Size') sz ON si.pkStockItemID = sz.fkeysz ";
	$sqlSearchParameter = $tempWhere + " ORDER BY st.Type, sc.Code, scr.Colour, son.OrderNo";

	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
	console.log($customQuery);

	SQLQuery($customQuery);

	$.each($ajaxResults.Results, function( i ) {
		$queryResults.push($ajaxResults.Results[i]);
	});

	$.each($queryResults, function( i ) {
		$.each($inputArray, function ( j ) {
			if ($queryResults[i].BarcodeNumber === $inputArray[j].BarcodeNumber) {
				$barcodeData.push({
					BarcodeNumber: $inputArray[j].BarcodeNumber,
					Type: $queryResults[i].Type,
					Name: $queryResults[i].Name,
					Code: $queryResults[i].Code,
					Colour: $queryResults[i].Colour,
					Size: $queryResults[i].Size,
				});
				$qtyArray.push(parseInt($inputArray[j].number));
			}
		});
	});

	createBarcodeElements();

	
}