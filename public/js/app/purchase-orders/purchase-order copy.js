$(document).ready(function() {
	var $previousPoNumber = '';
	var $previousGroupName = '';
	var $openPOList = [];
	var notesArray = [];
	var statusArray = [];
	var purchaseOrderID = '';
	var previousId;
	var tempNotesArray = CustomQuery("pur.pkPurchaseID, pur.ExternalInvoiceNumber, pn.pkPurchaseOrderNoteId, pn.NoteDateTime, pn.Note", "Purchase Pur", "INNER JOIN Purchase_Notes pn ON pur.pkPurchaseID = pn.fkPurchaseId", "pur.Status = 'OPEN' ORDER BY pur.ExternalInvoiceNumber ASC, pn.NoteDateTime DESC").Results;

	// Creates a array for all status updates and notes for the purchase orders
	$(tempNotesArray).each(function(i) {
		let noteStr = tempNotesArray[i].Note.split(' - ');

		alreadyHaveStatus = true;

		if(noteStr[0] == 'NOTE') {
			notesArray.push({ExternalInvoiceNumber: tempNotesArray[i].ExternalInvoiceNumber, Note: noteStr[1], User: noteStr[2], Date: tempNotesArray[i].NoteDateTime});
		}
		
		if(noteStr[0] == 'STATUS' && tempNotesArray[i].ExternalInvoiceNumber !== previousId) {
			let note = '';
			for(let j=1; j<noteStr.length-1; j++) {
				if(j > 1) {
					note += ' - ';
				}
				note += noteStr[j];
			}
			statusArray.push({ExternalInvoiceNumber: tempNotesArray[i].ExternalInvoiceNumber, Note: note, User: noteStr[noteStr.length-1]});
			previousId = tempNotesArray[i].ExternalInvoiceNumber;
		}
	});

	console.log(notesArray);
	console.log(statusArray);

	// Initialize all tooltips on the page
	$(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});
	
	// Run Ajax request to get all data for each column and save as a variable
	var $defaultPo = PurchaseOrders.ByLocation('default', 'OPEN');
	console.log($defaultPo.Results);
	$('#default-results').append(CreatePOElements($defaultPo.Results));

	var $jcPo = PurchaseOrders.ByLocation('jc', 'OPEN');
	$('#jc-results').append(CreatePOElements($jcPo.Results));

	var $jcToDef = PurchaseOrders.ByLocation('jcToDef', 'OPEN');
	$('#jc-to-default-results').append(CreatePOElements($jcToDef.Results));

	var $poNotes = PurchaseOrders.GetNotesByPONumber($openPOList).Results;
	AddNotesToPO();

	// Hide all individual items so only the item group is visable
	$('.item-group-list ul').each(function(i) {
		$(this).hide();
	});

	// Show individual items when clicked
	$('.item-group-list').on('click', function(e) {
		e.preventDefault();
		$(this).find('ul').toggle();
	});

	// Open modal to add new notes to the database
	$('.add-new-note').on('click', function() {
		$('#new-note-modal').modal('show');
		$('#new-note-modal').attr('pkpurchaseid', $(this).parents('.po-container').attr('pkpurchaseid'));
		$('#poNumberID').html($(this).parent().attr('id').toUpperCase());
		$('#poNumberInput').val($(this).parent().attr('id').toUpperCase());
	});

	// Save the note to Linnworks
	$(document).on('click', '#add-new-note-btn', function(e) {
		e.preventDefault();
		if($('#tb-poNotes').val() !== '') {
			ApiPurchaseOrders.AddNotes($(this).parents('#new-note-modal').attr('pkPurchaseID'), 'NOTE - ' + $('#tb-poNotes').val());
		} else {
			alert('Please add a note!');
		}
	});

	// Open modal to change the status
	$(document).on('click', '.change-status', function() {
		$('#change-status-modal .modal-title').attr('pkPurchaseID', $(this).parents('.po-container').attr('pkpurchaseid'));
		$('#change-status-modal .po-id').html($(this).parents('.po-container').attr('id').toUpperCase());
		$('#change-status-modal .delivery-date').html($(this).parents('.po-container').attr('expectedDate'));

		$('#change-status-modal').modal('show');
	});

	// Display notes on individual PO divs
	$('.has-note').on('click', function() {
		$(this).parent().find('.po-notes').toggle();

		if($(this).parent().find('hr').hasClass('remove-margin')) {
			$(this).parent().find('hr').removeClass('remove-margin')
		} else {
			$(this).parent().find('hr').addClass('remove-margin')
		}
	});

	// Search for an item using Ajax to return the delivery date and which location that it will delivered to
	$('#item-search button').on('click', function() {
		var itemNameArray = [];
		if($('#item-search input').val() !== '') {
			itemNameArray = $('#item-search input').val().split(' ');

			// CreateItemModal(PurchaseOrders.ByItemName(itemNameArrasy));

			$('#item-search-modal .modal-body').html(CreateItemModal(PurchaseOrders.ByItemName(itemNameArray)));
			$('#item-search-modal').modal('show');
		} else {
			alert('Please enter a product name!');
		}
	});

	// Close item modal and jump to purchase order
	jQuery(document).on('click', 'a.close-modal', function(){
		$('#item-search-modal').modal('hide');
	});

	// Switch to hide or show items on the PO lists
	$('#item-switch input').on('click', function() {
		if($('#item-switch input').prop('checked') === true) {
			$('.item-group-list, .po-horizontal-line').show();
		} else {
			$('.item-group-list, .po-horizontal-line').hide();
		}
	});
	
	$('.po-notes').hide();

	// $('#change-status-modal').modal('show');

	// Hides all the divs that are not radio button upon modal load
	$('#change-status-modal').on('shown.bs.modal', function() {
		$('.status-modal-hide').hide();
	});

	// Set dropdown text to value selected
	$('#change-status-modal .dropdown a').on('click', function(e) {
		e.preventDefault();
		$(this).parents('.dropdown').find('button').html($(this).html());
		$(this).parents('.dropdown').find('button').val($(this).html());
	});

	// Show divs when the adjacent radio is selected
	$('input[name="statusRadios"]').on('click', function() {
		$('.status-modal-hide').hide();
		$(this).parents('.row').find('.status-modal-hide').show();
	});

	// Open change status modal
	$(document).on('click', '.change-status', function() {
		$(this).parents('.po-container').find('')
	});

	// Save status change to a string to send to Linnworks
	$('#status-save-btn').on('click', function() {
		var statusString = 'STATUS - ';
		$('input[name="statusRadios"]').each(function(i) {
			if($(this).is(':checked')) {
				statusString += $(this).val();
				 $(this).parents('.row').find('.status-str-data').each(function(j) {
				 	if($(this).attr('id') === 'plus-minus-dd') {
				 		switch($(this).val()) {
				 			case '+/-':
					 			statusString += ' - PLUSMINUS';
					 			break;
				 			case '+':
					 			statusString += ' - PLUS';
					 			break;
				 			case '-':
					 			statusString += ' - MINUS';
					 			break;
				 		}
				 	} else {
					 	statusString += ' - ' + $(this).val();
					 }

					 if($(this).attr('id') === '') {
					 	$('#time-frame-dd').val();
					 }
				 });


				 statusString += ' - ' + $(this).parents('#change-status-modal').attr('username');
			}
		});
		ApiPurchaseOrders.AddNotes($(this).parents('#change-status-modal').find('.modal-title').attr('pkPurchaseID'), statusString);
		$('#change-status-modal').modal('hide');
	});	

	function CreatePOElements(data) {
		var poString = '';

		$.each(data, function (i) {

			function poHeader() {
				var timeA = moment(data[i].ISODate, moment.ISO_8601).startOf('day');
				var timeDiff = timeA.diff(moment().startOf('day'), 'days');

				$openPOList.push(data[i].ExternalInvoiceNumber);

				if(timeDiff < 0) {
					poString += '<div class="po-container alert alert-danger" id="' + data[i].ExternalInvoiceNumber.toLowerCase() + '" pkPurchaseID="' + data[i].pkPurchaseID + '" expectedDate="' + data[i].ExpectedDate + '" timeDiff="' + timeDiff + '">';
				} else if(timeDiff >= 0 && timeDiff < 7) {
					poString += '<div class="po-container alert alert-warning" id="' + data[i].ExternalInvoiceNumber.toLowerCase() + '" pkPurchaseID="' + data[i].pkPurchaseID + '" expectedDate="' + data[i].ExpectedDate + '" timeDiff="' + timeDiff + '">';
				} else {
					poString += '<div class="po-container alert alert-success" id="' + data[i].ExternalInvoiceNumber.toLowerCase() + '" pkPurchaseID="' + data[i].pkPurchaseID + '" expectedDate="' + data[i].ExpectedDate + '" timeDiff="' + timeDiff + '">';
				}

				poString += '<button type="button" class="btn btn-secondary status-icon fa fa-ban" data-toggle="tooltip" data-placement="right" title="No Status"></button> ';
				poString += '<button type="button" class="btn btn-secondary fa fa-sticky-note has-note" data-toggle="tooltip" data-placement="right" hidden></button> ';
				poString += data[i].ExpectedDate + ' ';
				poString += ' - '
				poString += data[i].SupplierName;
				poString += '<small>(' + data[i].ExternalInvoiceNumber;

				if(data[i].SupplierReferenceNumber !== '') {
					poString += ' / ' + data[i].SupplierReferenceNumber;
				}

				poString += ') </small>';
				poString += '<br><span class="add-new-note">Add Note</span> / <span class="change-status">Change Status</span>';
				poString += '<hr class="po-horizontal-line">';
				poString += '<div class="po-notes"></div>'
			}

			function itemContainer() {
				poString += '<ul class="item-group-list">';
				poString += '<li><a href="#"><strong>' + data[i].GroupName + '</strong></a></li>';
				poString += '<ul>';
			}

			if(i === 0) {
				$previousGroupName = data[i].GroupName;
				poHeader();
				itemContainer();
			} else if(data[i].ExternalInvoiceNumber !== $previousPoNumber) {
				$previousGroupName = data[i].GroupName;
				poString += '</div>';
				poHeader();
				itemContainer();
			}

			if(data[i].GroupName !== $previousGroupName) {
				poString += '</ul></ul>';
				itemContainer();
			}

			poString += '<li>' + data[i].ItemNumber + ' (' + data[i].Quantity + ')</li>';


			$previousGroupName = data[i].GroupName;
			$previousPoNumber = data[i].ExternalInvoiceNumber;
		});

		poString += '</ul></ul></div>';

		$('#item-switch input').attr('disabled', false);
		
		return poString;
	}

	function AddNotesToPO() {
		$('.po-container').each(function(i) {
			var poID = $(this).attr('id');

			$.each(notesArray, function(j) {
				let poNumber = notesArray[j].ExternalInvoiceNumber.toLowerCase();

				if(poNumber == poID) {
					$('#'+poID).find('.has-note').addClass('fa-sticky-note');
					$('#'+poID).find('.has-note').attr('hidden', false);
					var notesStr = '<div class="note-container">';
					notesStr += '<p>' + notesArray[j].Date.split(' ')[0] + ' - ' + notesArray[j].Note + '</p>';
					notesStr += '</div>';

					$('#'+poID).find('.po-notes').append(notesStr);
				}
			});

			$.each(statusArray, function(j) {
				let poNumber = statusArray[j].ExternalInvoiceNumber.toLowerCase();

				if(poNumber == poID) {
					let noteArray = statusArray[j].Note.split(' - ');
					let statusType = noteArray[0].toLowerCase();
					let buffer;
					$('#'+poID+' .status-icon').removeClass('fa fa-ban');

					switch(statusType) {
						case 'accurate to':
							$('#'+poID+' .status-icon').addClass('fa fa-random text-primary');
							if(noteArray[1] === 'PLUSMINUS') { buffer = '+/-'; } else if(noteArray[1] === 'PLUS') { buffer = '+'; } else if(noteArray[1] === 'MINUS') { buffer = '-'; }
							$('#'+poID+' .status-icon').attr('title', 'Accurate to: ' + buffer + ' ' + noteArray[2] + ' ' + noteArray[3]);
							break;
						case 'delivered':
							$('#'+poID+' .status-icon').addClass('fa fa-truck');
							if(noteArray[1] === 'NOT BOOKED IN') { 
								$('#'+poID+' .status-icon').addClass('text-danger');
								$('#'+poID+' .status-icon').attr('title', 'Delivered - Not booked in - ' + noteArray[2]);
							 } else if(noteArray[1] === 'PROCESSING') {
							 	$('#'+poID+' .status-icon').addClass('text-warning');
							 	$('#'+poID+' .status-icon').attr('title', 'Delivered - Processing: ' + noteArray[3] + ' - ' + noteArray[2]);
							 }
							break;
						case 'confirmed':
							$('#'+poID+' .status-icon').addClass('fa fa-star text-success');
							$('#'+poID+' .status-icon').attr('title', '100% confirmed');
							break;
					}
				}
			});
		});

		$('.po-notes').append('<hr class="po-horizontal-line">');
	}
});
	
function CreateItemModal(itemArray) {
	var location = '';
	var $itemListStr;

	$.each(itemArray.Results, function(i) {
		if($(this).index() === 0) {
			$itemListStr += '<h3>' + itemArray.Results[i].Location + '</h3>';
			$itemListStr += '<table class="table">';
			$itemListStr += '<thead class="thead-inverse">';
			$itemListStr += '<tr>';
			$itemListStr += '<th>PO Number</th>';
			$itemListStr += '<th>Item Name</th>';
			$itemListStr += '<th>Qty</th>';
			$itemListStr += '<th>Expected Date</th>';
			$itemListStr += '</tr>';
			$itemListStr += '</thead>';
			$itemListStr += '<tbody>';
		} else if(itemArray.Results[i].Location !== location) {
			$itemListStr += '</tbody>';
			$itemListStr += '</table>';
			$itemListStr += '<h3>' + itemArray.Results[i].Location + '</h3>';
			$itemListStr += '<table class="table">';
			$itemListStr += '<thead class="thead-inverse">';
			$itemListStr += '<tr>';
			$itemListStr += '<th>PO Number</th>';
			$itemListStr += '<th>Item Name</th>';
			$itemListStr += '<th>Qty</th>';
			$itemListStr += '<th>Expected Date</th>';
			$itemListStr += '</tr>';
			$itemListStr += '</thead>';
			$itemListStr += '<tbody>';
		}

		$itemListStr += '<tr>';
		$itemListStr += '<td><a class="close-modal" href="#' + itemArray.Results[i].ExternalInvoiceNumber.toLowerCase() + '">' + itemArray.Results[i].ExternalInvoiceNumber + '</a></td>';
		$itemListStr += '<td>' + itemArray.Results[i].ItemNumber + '</td>';
		$itemListStr += '<td>' + itemArray.Results[i].Quantity + '</td>';
		$itemListStr += '<td>' + itemArray.Results[i].ExpectedDate + '</td>';
		$itemListStr += '</tr>';

		location = itemArray.Results[i].Location;
	});

	$itemListStr += '</tbody>';
	$itemListStr += '</table>';

	return $itemListStr;
}