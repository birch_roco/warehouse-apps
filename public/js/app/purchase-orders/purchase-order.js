$(document).ready(function() {
	var $previousPoNumber = '';
	var $previousGroupName = '';
	var $openPOList = [];
	var notesArray = [];
	var currentStatusArray = [];
	var statusArray = [];
	var purchaseOrderID = '';
	var previousId;
	var tempNotesArray = CustomQuery("pur.pkPurchaseID, pur.ExternalInvoiceNumber, pn.pkPurchaseOrderNoteId, pn.NoteDateTime, pn.Note", "Purchase Pur", "INNER JOIN Purchase_Notes pn ON pur.pkPurchaseID = pn.fkPurchaseId", "pur.Status = 'OPEN' ORDER BY pur.ExternalInvoiceNumber ASC, pn.NoteDateTime DESC").Results;

	// Enter key lets the user search
	$(document).keydown(function(e) {
		if($('#item-search input').is(':focus') && e.which === 13) {
			e.preventDefault();

			if($('#item-search input').val().length > 0) {
				$('#item-search button').click();
			}			
		}
	});

	// Creates a array for all status updates and notes for the purchase orders
	$(tempNotesArray).each(function(i) {
		let noteStr = tempNotesArray[i].Note.split(' - ');

		alreadyHaveStatus = true;

		if(noteStr[0] == 'NOTE') {
			notesArray.push({ExternalInvoiceNumber: tempNotesArray[i].ExternalInvoiceNumber, Note: noteStr[1], User: noteStr[2], Date: tempNotesArray[i].NoteDateTime});
		} else if(noteStr[0] == 'STATUS' && tempNotesArray[i].ExternalInvoiceNumber !== previousId) {
			let note = '';
			for(let j=1; j<noteStr.length-1; j++) {
				if(j > 1) {
					note += ' - ';
				}
				note += noteStr[j];
			}
			currentStatusArray.push({ExternalInvoiceNumber: tempNotesArray[i].ExternalInvoiceNumber, Note: note, User: noteStr[noteStr.length-1]});
			previousId = tempNotesArray[i].ExternalInvoiceNumber;
		} 

		if(noteStr[0] == 'STATUS') {
			let note = '';
			for(let j=1; j<noteStr.length-1; j++) {
				if(j > 1) {
					note += ' - ';
				}
				note += noteStr[j].charAt(0).toUpperCase() + noteStr[j].slice(1).toLowerCase();
			}
			statusArray.push({ExternalInvoiceNumber: tempNotesArray[i].ExternalInvoiceNumber, Note: note, User: noteStr[noteStr.length-1], Date: tempNotesArray[i].NoteDateTime});
		}
	});

	// Initialize all tooltips on the page
	$(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});
	
	// Run Ajax request to get all data for each location and create the elements
	CreatePOElements(PurchaseOrders.Open().Results);
	$('.alert').css('border', '1px solid #cac7c7');

	// Get all open order notes and append to the PO
	AddNotesToPO(PurchaseOrders.GetNotesByPONumber($openPOList).Results);

	// Open modal to add new notes to the database
	$('.add-new-note').on('click', function() {
		$('#new-note-modal').modal('show');
		$('#new-note-modal').attr('pkpurchaseid', $(this).parents('.po-container').attr('pkpurchaseid'));
		$('#poNumberID').html($(this).parents('.po-container').attr('id').toUpperCase());
		$('#poNumberInput').val($(this).parents('.po-container').attr('id').toUpperCase());
	});

	// Save the note to Linnworks
	$(document).on('click', '#add-new-note-btn', function(e) {
		e.preventDefault();
		if($('#tb-poNotes').val() !== '') {
			ApiPurchaseOrders.AddNotes($(this).parents('#new-note-modal').attr('pkPurchaseID'), 'NOTE - ' + $('#tb-poNotes').val());
			$('#new-note-modal').modal('hide');
			location.reload();
		} else {
			alert('Please add a note!');
		}
	});

	// Open modal to change the status
	$(document).on('click', '.change-status', function() {
		$('#change-status-modal .modal-title').attr('pkPurchaseID', $(this).parents('.po-container').attr('pkpurchaseid'));
		$('#change-status-modal .po-id').html($(this).parents('.po-container').attr('id').toUpperCase());
		$('#change-status-modal .delivery-date').html($(this).parents('.po-container').attr('expectedDate'));

		$('#change-status-modal').modal('show');
	});

	// Display notes on individual PO divs
	$('.has-note').on('click', function() {
		$(this).parent().find('.po-notes').toggle();

		if($(this).parent().find('hr').hasClass('remove-margin')) {
			$(this).parent().find('hr').removeClass('remove-margin')
		} else {
			$(this).parent().find('hr').addClass('remove-margin')
		}
	});

	// Display the list of status changes
	$('.status-notes-list').on('click', function() {
		$(this).parent().find('.po-status-list').toggle();

		if($(this).parent().find('hr').hasClass('remove-margin')) {
			$(this).parent().find('hr').removeClass('remove-margin')
		} else {
			$(this).parent().find('hr').addClass('remove-margin')
		}
	});

	// Search for an item using Ajax to return the delivery date and which location that it will delivered to
	$('#item-search button').on('click', function() {
		var itemNameArray = [];
		if($('#item-search input').val() !== '') {
			itemNameArray = $('#item-search input').val().split(' ');

			// CreateItemModal(PurchaseOrders.ByItemName(itemNameArray));
			let searchItems = PurchaseOrders.ByItemName(itemNameArray);

			if(searchItems.Results.length > 0) {
				$('#item-search-modal .modal-body').html(CreateItemModal(searchItems));
				$('#item-search-modal').modal('show');
			} else {
				alert('Your search has returned no results!!');
			}

			
		} else {
			alert('Please enter a product name!');
		}
	});

	// Close item modal and jump to purchase order
	$(document).on('click', 'a.close-modal', function(){
		$('.alert').css('border', '1px solid #cac7c7');
		$('.item-list li').css('border', 'none');
		$('.item-container').hide();
		let poId = $(this).html().toLowerCase();
		let linnworksSku = $(this).parents('tr').find('.linnworks-sku').html();

		$('#' + poId + ' .check-items').click();
		$('#' + poId).css('border', '2px solid #3097D1');
		$("#" + poId + " li[linnworksSKU='" + linnworksSku + "']").css("border", "2px solid #3097D1");
		$("#" + poId + " li[linnworksSKU='" + linnworksSku + "']").parents("ul").show();
		$('#item-search-modal').modal('hide');
	});
	
	// Hide notes and item divs
	$('.po-notes, .po-status-list, .item-container').hide();

	// Set dropdown text to value selected
	$('#change-status-modal .dropdown a').on('click', function(e) {
		e.preventDefault();
		$(this).parents('.dropdown').find('button').html($(this).html());
		$(this).parents('.dropdown').find('button').val($(this).html());
	});

	// Show divs when the adjacent radio is selected
	$('input[name="statusRadios"]').on('click', function() {
		$('.status-options').attr('hidden', true);
		$('.' + $(this).attr('reference')).attr('hidden', false);
	});

	// Open change status modal
	$(document).on('click', '.change-status', function() {
		$(this).parents('.po-container').find('')
	});

	// Get the items for a purchase order
	$(document).on('click', '.check-items', function() {
		let itemContainer = $(this).parents('.po-container').find('.item-container');

		if (itemContainer.is(':empty')){
			itemContainer.html(CreateItemList($(this).parents('.po-container').attr('id')));
		}

		$(this).parents('.po-container').find('.item-container').toggle();
		itemContainer.find('.item-list').hide();
	});

	// Show items when group name is clicked
	$(document).on('click', '.item-group-list', function() {
		$(this).find('.item-list').toggle();
	});

	// Save status change to a string to send to Linnworks
	$('#status-save-btn').on('click', function() {
		var statusString = 'STATUS - ';
		$('input[name="statusRadios"]').each(function(i) {
			if($(this).is(':checked')) {
				statusString += $(this).val();

				let statusOptions = '.' + $(this).attr('reference');

				 $(statusOptions).find('.status-str-data').each(function(j) {
				 	if($(this).attr('id') === 'plus-minus-dd') {
				 		switch($(this).val()) {
				 			case '+/-':
					 			statusString += ' - PLUSMINUS';
					 			break;
				 			case '+':
					 			statusString += ' - PLUS';
					 			break;
				 			case '-':
					 			statusString += ' - MINUS';
					 			break;
				 		}
				 	} else {
					 	statusString += ' - ' + $(this).val();
					 }

					 if($(this).attr('id') === '') {
					 	$('#time-frame-dd').val();
					 }
				 });


				 statusString += ' - ' + $(this).parents('#change-status-modal').attr('username');
			}
		});

		ApiPurchaseOrders.AddNotes($(this).parents('#change-status-modal').find('.modal-title').attr('pkPurchaseID'), statusString);
		$('#change-status-modal').modal('hide');
		location.reload();
	});	

	function CreatePOElements(data) {
		var poString = '';

		$.each(data, function (i) {
			var timeA = moment(data[i].ISODate, moment.ISO_8601).startOf('day');
			var timeDiff = timeA.diff(moment().startOf('day'), 'days');
			$openPOList.push(data[i].ExternalInvoiceNumber);

			poString = '<div class="po-container alert ';

			if(timeDiff < 0) {
				poString += 'alert-danger"';
			} else if(timeDiff >= 0 && timeDiff < 7) {
				poString += 'alert-warning"';
			} else {
				poString += 'alert-success"';
			}

			poString += 'id="' + data[i].ExternalInvoiceNumber.toLowerCase() + '" pkPurchaseID="' + data[i].pkPurchaseID + '" expectedDate="' + data[i].ExpectedDate + '" timeDiff="' + timeDiff + '">';

			poString += '<button type="button" class="btn btn-secondary status-icon fa fa-ban" data-toggle="tooltip" data-placement="right" title="No Status"></button> ';
			poString += '<button type="button" class="btn btn-secondary fa fa-sticky-note has-note" data-toggle="tooltip" data-placement="right" hidden></button> ';
			poString += data[i].ExpectedDate + ' ';
			poString += ' - '
			poString += data[i].SupplierName;
			poString += '<small>(' + data[i].ExternalInvoiceNumber;

			if(data[i].SupplierReferenceNumber !== '') {
				poString += ' / ' + data[i].SupplierReferenceNumber;
			}

			poString += ') </small>';
			poString += '<div class="text-center mt-2"><span class="add-new-note">Add Note</span> / <span class="change-status">Change Status</span> / <span class="check-items">View Items</span></div>';
			poString += '<hr class="po-horizontal-line">';
			poString += '<div class="po-notes"></div>'
			poString += '<div class="po-status-list"></div>'
			poString += '<div class="item-container"></div>'

			if(data[i].Location === 'Default' && data[i].SupplierName !== 'Jonathan Cohen') {
				$('#default-results').append(poString);	
			} else if(data[i].Location === 'Default' && data[i].SupplierName === 'Jonathan Cohen') {
				$('#jc-to-default-results').append(poString);
			} else {
				$('#jc-results').append(poString);
			}			
		});
		
		return poString;
	}

	function CreateItemList(poNumber) {
		// Ajax request to get the items
		let poItems = PurchaseOrders.ItemsByPONumber(poNumber).Results;
		let itemStr = '';
		let previousGroupName = '';

		$(poItems).each(function(i) {
			if(poItems[i].GroupName !== previousGroupName) {
				previousGroupName = poItems[i].GroupName;

				if(i > 0) {
					itemStr += '</ul></ul>';
				}
				itemStr += '<ul class="item-group-list"><li href="#"><strong>' + poItems[i].GroupName + '</strong></li><ul class="item-list">'

			}

			itemStr += '<li linnworksSKU="' + poItems[i].ItemNumber + '">' + poItems[i].ItemNumber + ' (' + poItems[i].Quantity + ')</li>';
		});

		itemStr += '</ul></ul>';
		return itemStr;
	}

	function AddNotesToPO() {
		$('.po-container').each(function(i) {
			var poID = $(this).attr('id');

			$.each(notesArray, function(j) {
				let poNumber = notesArray[j].ExternalInvoiceNumber.toLowerCase();

				if(poNumber == poID) {
					$('#'+poID).find('.has-note').addClass('fa-sticky-note');
					$('#'+poID).find('.has-note').attr('hidden', false);
					var notesStr = '<div class="note-container">';
					notesStr += '<p>' + notesArray[j].Date.split(' ')[0] + ' - ' + notesArray[j].Note + '</p>';
					notesStr += '</div>';

					$('#'+poID).find('.po-notes').append(notesStr);
				}
			});

			$.each(statusArray, function(j) {
				let poNumber = statusArray[j].ExternalInvoiceNumber.toLowerCase();

				if(poNumber == poID) {
					$('#'+poID).find('.status-icon').addClass('status-notes-list');
					var notesStr = '<div class="note-container">';
					notesStr += '<p>' + statusArray[j].Date.split(' ')[0] + ' - ' + statusArray[j].Note + '</p>';
					notesStr += '</div>';

					$('#'+poID).find('.po-status-list').append(notesStr);
				}
			});

			$.each(currentStatusArray, function(j) {
				let poNumber = currentStatusArray[j].ExternalInvoiceNumber.toLowerCase();

				if(poNumber == poID) {
					let noteArray = currentStatusArray[j].Note.split(' - ');
					let statusType = noteArray[0].toLowerCase();
					let buffer;
					$('#'+poID+' .status-icon').removeClass('fa fa-ban');

					switch(statusType) {
						case 'accurate to':
							$('#'+poID+' .status-icon').addClass('fa fa-random text-primary');
							if(noteArray[1] === 'PLUSMINUS') { buffer = '+/-'; } else if(noteArray[1] === 'PLUS') { buffer = '+'; } else if(noteArray[1] === 'MINUS') { buffer = '-'; }
							$('#'+poID+' .status-icon').attr('title', 'Accurate to: ' + buffer + ' ' + noteArray[2] + ' ' + noteArray[3]);
							break;
						case 'delivered':
							$('#'+poID+' .status-icon').addClass('fa fa-truck');
							if(noteArray[1] === 'NOT BOOKED IN') { 
								$('#'+poID+' .status-icon').addClass('text-danger');
								$('#'+poID+' .status-icon').attr('title', 'Delivered - Not booked in - ' + noteArray[2]);
							 } else if(noteArray[1] === 'PROCESSING') {
							 	$('#'+poID+' .status-icon').addClass('text-warning');
							 	$('#'+poID+' .status-icon').attr('title', 'Delivered - Processing: ' + noteArray[3] + ' - ' + noteArray[2]);
							 }
							break;
						case 'confirmed':
							$('#'+poID+' .status-icon').addClass('fa fa-star text-success');
							$('#'+poID+' .status-icon').attr('title', '100% confirmed');
							break;
					}
				}
			});
		});

		$('.po-notes').append('<hr class="po-horizontal-line">');
	}
});
	
function CreateItemModal(itemArray) {
	var location = '';
	var $itemListStr = '';

	$.each(itemArray.Results, function(i) {
		if(i === 0) {
			$itemListStr += '<h3>' + itemArray.Results[i].Location + '</h3>';
			$itemListStr += '<table class="table">';
			$itemListStr += '<thead class="thead-inverse">';
			$itemListStr += '<tr>';
			$itemListStr += '<th>PO Number</th>';
			$itemListStr += '<th>Item Name</th>';
			$itemListStr += '<th>Qty</th>';
			$itemListStr += '<th>Expected Date</th>';
			$itemListStr += '</tr>';
			$itemListStr += '</thead>';
			$itemListStr += '<tbody>';
		} else if(itemArray.Results[i].Location !== location) {
			$itemListStr += '</tbody>';
			$itemListStr += '</table>';
			$itemListStr += '<h3>' + itemArray.Results[i].Location + '</h3>';
			$itemListStr += '<table class="table">';
			$itemListStr += '<thead class="thead-inverse">';
			$itemListStr += '<tr>';
			$itemListStr += '<th>PO Number</th>';
			$itemListStr += '<th>Item Name</th>';
			$itemListStr += '<th>Qty</th>';
			$itemListStr += '<th>Expected Date</th>';
			$itemListStr += '</tr>';
			$itemListStr += '</thead>';
			$itemListStr += '<tbody>';
		}

		$itemListStr += '<tr>';
		$itemListStr += '<td><a class="close-modal" href="#' + itemArray.Results[i].ExternalInvoiceNumber.toLowerCase() + '">' + itemArray.Results[i].ExternalInvoiceNumber + '</a></td>';
		$itemListStr += '<td class="linnworks-sku">' + itemArray.Results[i].ItemNumber + '</td>';
		$itemListStr += '<td>' + itemArray.Results[i].Quantity + '</td>';
		$itemListStr += '<td>' + itemArray.Results[i].ExpectedDate + '</td>';
		$itemListStr += '</tr>';

		location = itemArray.Results[i].Location;
	});

	$itemListStr += '</tbody>';
	$itemListStr += '</table>';

	return $itemListStr;
}