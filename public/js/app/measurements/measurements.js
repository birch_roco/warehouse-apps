$(document).ready(function() {
	var measurementsCompleted;

	// Checks which tab was last open using local storage
	var activeTab = localStorage.getItem('ActiveTabID');

	if(activeTab === 'view') {
		$('#create-tab, #create').removeClass('active');
		$('#view-tab, #view').addClass('active');
	} else if(activeTab === 'create') {
		$('#view-tab, #view').removeClass('active');
		$('#create-tab, #create').addClass('active');
	}
	localStorage.setItem('ActiveTabID' , 'create');

	// Sets group status to icon
	$('#measure-table .measure-status').each(function(i) {
		if($(this).html() === '1') {
			$(this).html('<i class="fal fa-check-circle text-success"></i>');
		} else if($(this).html() === '0') {
			$(this).html('<i class="fal fa-times-circle text-danger"></i>');
		}
	});

	// Search by dropdown change
	$('#dd-search-by div a').on('click', function(e) {
		e.preventDefault();
		$('#search-results').empty();

		if($(this).index() === 0) {
			$('#search-sku').show();
		} else {
			$('#search-sku').hide();
			var itemsNotMeasured = ItemSearch.NotMeasured();
			CreateItemSelectionElements(itemsNotMeasured.Results);
		}
	});

	// Search for items using the text box
	$('#search-sku button').on('click', function() {
		var searchStr = $('#search-sku input').val();
		if(searchStr === '') {
			alert('Please enter a value in the search bar!!');
		} else {
			var searchArray = searchStr.split(' ');
			var itemsSearchBarResults = ItemSearch.BySkuUsingSearchBar(searchArray, 'measurment');

			if(itemsSearchBarResults.Results.length === 0) {
				alert('No search results!!!');
			} else {
				console.log(itemsSearchBarResults.Results);

				CreateItemSelectionElements(itemsSearchBarResults.Results);
			}
		}
	});

	// Select all search results
	$('#select-all input').on('click', function() {
		if($(this).is(':checked')) {
			$('#search-results .item-container input').attr('checked', true);
		} else {
			$('#search-results .item-container input').attr('checked', false);
		}
	});

	// Move items to the measurement sheet screen when the add button is clicked
	$('#btn-add-to-measure-sheet').on('click', function() {
		$('#search-results .item-container').each(function(i) {
			if($(this).find('input').is(':checked')) {
				var storedElement = $(this)[0].outerHTML;
				$(this).hide();
				$('#measure-sheet-items').append(storedElement);
			}
		});
		$('#measure-sheet-items input').attr('checked', false);
	});

	// Move items to the item sheet screen when the remove button is clicked
	$('#btn-remove-from-measure-sheet').on('click', function() {
		$('#measure-sheet-items .item-container').each(function(i) {
			if($(this).find('input').is(':checked')) {
				var divClass = $(this).attr('class');
				var classArray = divClass.split(' ');
				divClass = classArray[2];
				$('.' + divClass).show();
				$(this).remove();
			}
		});
	});

	// Save the measurement sheet and items
	$('#btn-save').on('click', function() {
		var itemCheck = $('#measure-sheet-items').find('.item-container').length;
		var itemArray = [];

		if (itemCheck === 0 || !$('#measure-description').val()) {
			alert('Please check the description has a value and that you have selected some items!!');
		} else {
			$('#measure-sheet-items .item-container').each(function(i) {
				itemArray.push({groupName: $(this).attr('group-name'), productType: $(this).attr('product-type'), productCode: $(this).attr('product-code'), productSize: $(this).attr('product-size'), orderNo: $(this).attr('orderNo'), supplierName: $(this).attr('supplier-name'), lastMeasured: $(this).attr('last-measured')});
			});
			StockMeasurements.AddMeasurements($('#measure-description').val(), itemArray);
		}
		localStorage.setItem('ActiveTabID' , 'create');
		location.reload();
	});

	// View the count sheet items when a user clicks the view button
	$('.td-view-btn button').on('click', function() {
		var measurementId = $(this).parent().parent().attr('id');
		$('#item-measure-modal h5').html(measurementId);

		createItemList(measurementId);		

		$('#item-measure-modal').modal('show');
	});

	// Check to see if any of the quantities have been entered and update them in the database and in Linnworks
	$('#save-changes').on('click', function() {
		var searchArray = [];
		var itemArray = [];
		var emptyInputs = $('#item-measure-modal tbody').find('input').length;

		console.log(emptyInputs);

		$('#item-measure-modal tbody tr.item-row').each(function(i) {
			var inputHasValues = 0;
			var length = null;
			var width = null;
			var height = null;
			var weight = null;

			$(this).find('input').each(function(i) {
				if($(this).val() !== '') {
					inputHasValues++;
					emptyInputs--;
				}
			});

			if($(this).find('input').length > 0 && inputHasValues > 0) {
				searchArray.push({itemId: $(this).attr('itemid'), productGroup: $(this).attr('groupId'), productType: $(this).attr('productType'), productCode: $(this).attr('productCode'), productSize: $(this).attr('productSize'), supplierName: $(this).attr('supplierName'), lastChecked: $(this).attr('lastmeasured')});
				
				var productGroupName = $(this).attr('groupId');
				var indexNo;

				$(this).find('input').each(function(i) {

					$(searchArray).each(function(j) {
						if(searchArray[j].productGroup === productGroupName) {
							indexNo = j;
						}
					});

					var objectName = $(this).parent().attr('data-id');
					var objectValue = parseFloat($(this).val());

					if($(this).val() !== '') {
						searchArray[indexNo][objectName.charAt(0).toUpperCase() + objectName.slice(1)] = objectValue;
					} else {
						searchArray[indexNo][objectName.charAt(0).toUpperCase() + objectName.slice(1)] = null;
					}
				});
			}
		});

		if(searchArray.length > 0) {
			itemArray = ItemSearch.GetItemInfoByCustomSearch(searchArray).Results;
		}

		$(searchArray).each(function(i) {
			var groupId = $(searchArray)[i].productGroup;
			
			$(itemArray).each(function(j) {
				// delete itemArray[j].Weight;
				if(itemArray[j].SizeGroup === groupId) {
					if(searchArray[i].Length !== null) {
						itemArray[j].Width = searchArray[i].Length;
					}
					if(searchArray[i].Width !== null) {
						itemArray[j].Depth = searchArray[i].Width;
					}
					if(searchArray[i].Height !== null) {
						itemArray[j].Height = searchArray[i].Height;
					}
					console.log(searchArray[i].Weight);
					if(searchArray[i].Weight !== null) {
						itemArray[j].Weight = searchArray[i].Weight;
					}
					itemArray[j].lastChecked = searchArray[i].lastChecked;

					delete itemArray[j].SizeGroup;
					delete itemArray[j].DimDepth;
					delete itemArray[j].DimHeight;
					delete itemArray[j].DimWidth;
					delete itemArray[j].LastMeasuredDate;
				}
			});
		});
		// console.log('######## SearchArray ###########');
		// console.log(searchArray);
		// console.log('######## ItemArray ###########');
		// console.log(itemArray);
		
		if(itemArray.length > 0) {
			var linnworksRequest;
			var errorCount = 0;

			$.each(itemArray, function(j) {
				linnworksRequest = Stock.UpdateInventoryItem(itemArray[j]);

				if(linnworksRequest.status === 200) {
					Stock.InventoryExtendedProperty('LastMeasured', [itemArray[j]]);	
				} else {
					errorCount++;
				}
			});

			if(errorCount === 0) {
				StockMeasurements.UpdateStockItemMeasurements(searchArray);
			}			
		}

		console.log(emptyInputs);

		if(emptyInputs === 0) {
			StockMeasurements.UpdateMeasurementGroup(parseInt($(this).parents('#item-measure-modal').find('.modal-title').html()));
			localStorage.setItem('ActiveTabID' , 'view');
			location.reload();
		}

		$('#item-measure-modal').modal('hide');


		// var count = 0;
		// var inputCount = 0;
		// var staticCount = 0;
		// var objectName;
		// var objectValue;
		// var linnUpdateArray = [];
		// var dataUpdateArray = [];
		// var emptyInputs = $('#item-measure-modal .modal-body input').length;


		// $('#item-measure-modal tbody tr.item-row').each(function(i) {
		// 	var inputsWithValues = 0;
		// 	count = 0;
		// 	updateArray = [];
		// 	linnUpdateArray = [];
		// 	dataUpdateArray = [];

		// 	// if($(this).find('input').length > 0) {
		// 	// 	$(this).find('input').each(function(j) {
		// 	// 		if($(this).val() !== '') {
		// 	// 			inputsWithValues++;
		// 	// 		}
		// 	// 	});

		// 	// 	if(inputsWithValues > 0) {

		// 	// 	}
		// 	// }

		// 	if($(this).find('input').length > 0) {
		// 		var itemInfo = ItemSearch.GetItemInfoByCustomSearch($(this).attr('productType'), $(this).attr('productCode'), $(this).attr('productSize'), $(this).attr('supplierName'));

		// 		$.each(itemInfo.Results, function(j) {
		// 			linnUpdateArray.push({'StockItemId': itemInfo.Results[j].pkStockItemID, 'ItemNumber': itemInfo.Results[j].ItemNumber, 'ItemTitle': itemInfo.Results[j].ItemTitle, 'RetailPrice': parseFloat(itemInfo.Results[j].RetailPrice), 'Height': itemInfo.Results[j].DimHeight, 'Width': itemInfo.Results[j].DimWidth, 'Depth': itemInfo.Results[j].DimDepth, 'Weight': itemInfo.Results[j].Weight, 'lastChecked': itemInfo.Results[j].LastMeasuredDate});
		// 		});

		// 		dataUpdateArray.push({'itemId': $(this).attr('itemid'), 'Height': null, 'Width': null, 'Length': null, 'Weight': null});

		// 		$(this).find('td.measurement-cell').each(function(j) {
		// 			if($(this).hasClass('user-input') && $(this).find('input').val() !== '') {
		// 				linnObjectName = $(this).attr('class').split(' ')[2];
		// 				dataObjectName = $(this).attr('data-id');
		// 				objectValue = $(this).find('input').val();
		// 				emptyInputs--;
		// 			} 
		// 			else if(!$(this).hasClass('user-input')) {
		// 				linnObjectName = $(this).attr('class').split(' ')[1];
		// 				dataObjectName = $(this).attr('data-id');
		// 				objectValue = $(this).html();
		// 			}

		// 			// console.log('Row ID: ' + $(this).parents('tr').index() + ', Input Name: ' + $(this).attr('data-id') + ', Input Value: ' + $(this).find('input').val());

		// 			if(objectValue !== '') {
		// 				console.log('Row ID: ' + $(this).parents('tr').index() + ', Input Name: ' + $(this).attr('data-id') + ', Input Value: ' + $(this).find('input').val());
		// 				count++;
		// 				dataUpdateArray[0][dataObjectName.charAt(0).toUpperCase() + dataObjectName.slice(1)] = parseFloat(objectValue);
		// 				console.log(objectValue);
		// 				console.log(dataUpdateArray);
		// 				$.each(linnUpdateArray, function(l) {
		// 					linnUpdateArray[l][linnObjectName.charAt(0).toUpperCase() + linnObjectName.slice(1)] = parseFloat(objectValue);	
		// 				});
		// 			}
		// 		});

		// 		// console.log(linnUpdateArray);
		// 		// console.log(dataUpdateArray);

		// 		if(count > 0) {
		// 			var linnworksRequest;

		// 			$.each(linnUpdateArray, function(j) {
		// 				linnworksRequest = Stock.UpdateInventoryItem(linnUpdateArray[j]);
		// 			});

		// 			if(linnworksRequest.status === 200) {
		// 				StockMeasurements.UpdateStockItemMeasurements(dataUpdateArray);
		// 				Stock.InventoryExtendedProperty('LastMeasured', updateArray);
		// 			}
		// 		}
		// 	}
		// });

		// if(emptyInputs === 0) {
		// 	StockMeasurements.UpdateMeasurementGroup(parseInt($(this).parents('#item-measure-modal').find('.modal-title').html()));
		// 	localStorage.setItem('ActiveTabID' , 'view');
		// 	// location.reload();
		// }
		// $('#item-measure-modal').modal('hide');
	});

	// Open a confirm dialog and delete the count sheet and relevent items on confirm
	$('.td-delete-btn button').on('click', function() {
		var deleteCount = confirm('Are you sure that you want to delete this count?');

		if(deleteCount === true) {
			var groupId = $(this).parents('tr').attr('id')
			StockMeasurements.DeleteMeasureItems(groupId);
			StockMeasurements.DeleteMeasureGroup(groupId);
			localStorage.setItem('ActiveTabID' , 'view');
			location.reload();
		}
	});

	/********************
	** Functions ********
	********************/

	// Create search results on create page
	function CreateItemSelectionElements(itemArray) {
		let itemStr = '';
		
		$.each(itemArray, function(i) {
			itemStr += '<div class="form-check item-container item-container-' + i + '" group-name="' + itemArray[i].SizeGroup + '" orderNo="' + itemArray[i].OrderNo + '" last-measured="' + itemArray[i].LastMeasuredDate + '" supplier-name="' + itemArray[i].SupplierName + '" product-type="' + itemArray[i].Type + '" product-code="' + itemArray[i].Code + '" product-size="' + itemArray[i].Size + '">';
			itemStr += '<label class="form-check-label">';
			itemStr += '<input type="checkbox" class="form-check-input">';
			itemStr += ' ' + itemArray[i].SizeGroup + ' (' + itemArray[i].SupplierName + ')';
			itemStr += '</label>';
			itemStr += '</div>';
		});

		$('#search-results').html(itemStr);
	}


	function createItemList(measurementId) {
		// getItemsComplete = false;

		$.get( '/measurements/get-items/' + measurementId, function(data) {
			$('#item-measure-modal .modal-body tbody').html(CreateItemMeasureModal(data));
		});
	}

	// Create the item modal
	function CreateItemMeasureModal(data) {

		$('#save-changes').show();

		var itemModalStr = '';
		var counter = data.length * 4;
		measurementsCompleted = false;

		$.each(data, function(i) {
			itemModalStr += '<tr class="item-row" groupId="' + data[i].groupName + '" itemID="' + data[i].id + '" productType="' + data[i].productType + '" productCode="' + data[i].productCode + '" productSize="' + data[i].productSize + '" supplierName="' + data[i].supplierName + '" lastMeasured="' + data[i].last_measured_date + '">';
			itemModalStr += '<td class="item-group">' + data[i].groupName + '</td>';

			if (data[i].length !== null) {
				itemModalStr += '<td class="measurement-cell width" data-id="length">' + data[i].length + '</td>';
				counter--;
			} else {
				itemModalStr += '<td class="user-input measurement-cell width" data-id="length"><input type="number" class="form-control"></td>';
			}

			if (data[i].width !== null) {
				itemModalStr += '<td class="measurement-cell depth" data-id="width">' + data[i].width + '</td>';
				counter--;
			} else {
				itemModalStr += '<td class="user-input measurement-cell depth" data-id="width"><input type="number" class="form-control"></td>';
			}

			if (data[i].height !== null) {
				itemModalStr += '<td class="measurement-cell height" data-id="height">' + data[i].height + '</td>';
				counter--;
			} else {
				itemModalStr += '<td class="user-input measurement-cell height" data-id="height"><input type="number" class="form-control"></td>';
			}

			if (data[i].weight !== null) {
				itemModalStr += '<td class="measurement-cell weight" data-id="weight">' + data[i].weight + '</td>';
				counter--;
			} else {
				itemModalStr += '<td class="user-input measurement-cell weight" data-id="weight"><input type="number" class="form-control"></td>';
			}

			if (data[i].userID !== null) {
				itemModalStr += '<td class="measure-userId hidden-md-down">' + data[i].userID + '</td>';
			} else {
				itemModalStr += '<td class="measure-userId hidden-md-down">-</td>';
			}
			itemModalStr += '</tr>';
		});

		if(counter === 0) {
			$('#save-changes').hide();

			measurementsCompleted = true;
		}

		return itemModalStr;
	}

});
// $(document).ready(function() {
// 	var $searchResults;
// 	var expectedTotal;
// 	var countedTotal;
// 	var countCompleted;
// 	var printSheet;

// 	//function to call if you want to print the count sheet
// 	var onPrintFinished=function(printed, countID){
// 		$('#item-count-modal').modal('hide');
// 		StockCount.UpdateStockCount(countID, 'printed');
// 		localStorage.setItem('ActiveTabID' , 'view');
// 		location.reload();
// 	}

// 	// Hides searchbar on load
// 	$('#search-sku, #print-count-sheet').hide();

// 	// Checks which tab was last open using local storage
// 	var activeTab = localStorage.getItem('ActiveTabID');

// 	// Acts the same as clicking the plus if an input is selected and the user hits enter
// 	$('#item-count-modal').keydown(function(e) {
// 		if(e.which === 13) {
// 			e.preventDefault();
// 			$('.count-qty input').each(function(i){
// 				if($(this).is(':focus')){
// 					$(this).parents('tr').find('.add-count').trigger('click');
// 				}
// 			});
// 		}
// 	});

// 	if(activeTab === 'view') {
// 		$('#create-tab, #create').removeClass('active');
// 		$('#view-tab, #view').addClass('active');
// 	} else if(activeTab === 'create') {
// 		$('#view-tab, #view').removeClass('active');
// 		$('#create-tab, #create').addClass('active');
// 	}
// 	localStorage.setItem('ActiveTabID' , 'create');

// 	// Search table when user changes value in textbox
// 	$('#count-id-search').on('keyup', function() {
// 		$('#count-table tbody tr').show();
// 		$('#count-table tbody tr').each(function(i) {
// 			if($(this).attr('id') === $('#count-id-search').val()) {
// 				$('#count-table tbody tr:not(#'+ $(this).attr('id') +')').hide();
// 				$(this).show();
// 			}
// 		});
// 	});

// 	// Change complete column on count table to a icon
// 	$('#count-table .count-status').each(function(i) {
// 		if($(this).html() === '1') {
// 			$(this).html('<i class="fal fa-check-circle text-success"></i>');
// 		} else if($(this).html() === '0') {
// 			$(this).html('<i class="fal fa-times-circle text-danger"></i>');
// 		}
// 	});



// 	// Print the count sheet
// 	$('.td-print-btn button').on('click', function() {
// 		printSheet = true;

// 		var countId = $(this).parents('tr').attr('id');
// 		$('#item-count-modal h5').html(countId);

// 		createItemList(countId);
// 	});

// 	// Open a confirm dialog and delete the count sheet and relevent items on confirm
// 	$('.td-delete-btn button').on('click', function() {
// 		var deleteCount = confirm('Are you sure that you want to delete this count?');

// 		if(deleteCount === true) {
// 			var countId = $(this).parents('tr').attr('id')
// 			StockCount.DeleteCountItems(countId);
// 			StockCount.DeleteCount(countId);
// 			localStorage.setItem('ActiveTabID' , 'view');
// 			location.reload();
// 		}
// 	});



// 	$('#btn-save').on('click', function() {
// 		var itemCheck = $('#count-sheet-items').find('.item-container').length;
// 		var itemArray = [];

// 		if (itemCheck === 0 || !$('#count-description').val()) {
// 			alert('Please check the description has a value and that you have selected some items!!');
// 		} else {
// 			$('#count-sheet-items .item-container').each(function(i) {
// 				itemArray.push({linnworksID: $(this).attr('linnworksid'), productSKU: $(this).attr('sku'), productCode: $(this).attr('productCode'), itemType: $(this).attr('itemType'), itemCode: $(this).attr('itemCode'), itemColour: $(this).attr('itemColour'), orderNo: $(this).attr('orderNo'), expected: $(this).attr('expected'), lastCounted: $(this).attr('last-counted')});
// 			});

// 			StockCount.AddStockCount($('#count-description').val(), itemArray);
// 		}
// 		localStorage.setItem('ActiveTabID' , 'create');
// 		location.reload();
// 	});

// 	// Allow the user to count the item more than once
// 	$(document).on('click', '#item-count-modal .count-qty .add-count', function() {
// 		var countTotal;
// 		var numberInput = $(this).parent().find('input');
// 		if(numberInput.val() !== '' && !isNaN(parseInt(numberInput.val()))) {
// 			$(this).parents('tr').find('.count-tally span').append('<i>' + numberInput.val() + '</i>, ');
// 			countTotal = parseInt($(this).parents('tr').find('.count-tally .total').html()) + parseInt(numberInput.val());
// 			numberInput.val('');
// 			$(this).parent().find('input').focus();
// 			$(this).parents('tr').find('.count-tally .total').html(countTotal);
// 		} else {
// 			alert('Please enter a valid number!');
// 		}
// 	});




// 	function CreatePrintSheet(data) {
// 		var printStr = '';
// 		var countId = data[0].count_id.toString();

// 		$('#print-count-sheet #barcode').barcode(countId, 'code39', {
// 			barWidth: 2,
// 			barHeight: 30,
// 			showHRI: false,
// 			output: 'css'
// 		}); 
		
// 		$.each(data, function(i) {
// 			if(!data[i].count) {
// 				printStr += '<tr>';
// 				printStr += '<td>' + data[i].productCode + '</td>';
// 				printStr += '<td></td>';
// 				printStr += '</tr>';
// 			}
// 		});

// 		return printStr;
// 	}





// 	// Refreshes the expected if it has not been counted yet
// 	function RefreshExpected() {
// 		var itemArrayExpected = [];
// 		var updateExpectedArray = [];
// 		$('#item-count-modal tbody tr.item-row').each(function(i) {
// 			itemArrayExpected.push($(this).attr('linnworksid'));
// 		});
		
// 		var expectedUpdate = StockItemInfo.GetLevelsByStockItemID(itemArrayExpected);

// 		$.each(expectedUpdate.Results, function(i) {
// 			$('#item-count-modal tbody tr').each(function(j) {
// 				if(expectedUpdate.Results[i].pkStockItemID === $(this).attr('linnworksid') && $(this).find('.count-qty input').length > 0) {
// 					$(this).find('.expected-quantity').html(expectedUpdate.Results[i].Expected);
// 					updateExpectedArray.push({itemID: $(this).attr('itemID'), expected: expectedUpdate.Results[i].Expected});
// 				}
// 			});
// 		});

// 		if(!countCompleted) {
// 			StockCount.UpdateExpectedQty(updateExpectedArray);
// 		}	
// 	}
// });