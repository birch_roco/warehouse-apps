var mytoken = $('[name=csrf-token]').attr('content');
var $ajaxResults = [];

var Ajax = {
	SQLQuery: function(userData) {
		var $data = {'userInput': 'script', 'userData': userData, 'linnworksURL': 'Dashboards/ExecuteCustomScriptQuery'};
		return AjaxRequest2($data);
	}
};

function SQLQuery(userData) {
	var $data = {'userInput': 'script', 'userData': userData, 'linnworksURL': 'Dashboards/ExecuteCustomScriptQuery'};
	AjaxRequest($data);
}

function PurchaseOrderSearch(userData) {
	var $data = {'userInput': 'pkPurchaseId', 'userData': userData, 'linnworksURL': 'PurchaseOrder/Get_PurchaseOrder'};
	AjaxRequest2($data);
}

function AjaxRequest(ajaxData) {
	// console.log(ajaxURL + ' ' + ajaxMethod + ' ' + ajaxData);
	$ajaxResults = [];
	$('#loading-overlay').attr('hidden', false);

	$.ajax({
		headers: {'X-CSRF-TOKEN': mytoken},
		url: '/api-linnworks',
		method: 'post',
		data: ajaxData,
		async: false
	}).done(function(response) {
		$ajaxResults = response;
		// $.each(response.Results, function( i ) {
		// 	$ajaxResults.push(response.Results[i]);
		// });	

		$('#loading-overlay').attr('hidden', true);
	});
}

function AjaxRequest2(ajaxData) {
	$.ajax({
		headers: {'X-CSRF-TOKEN': mytoken},
		url: '/api-linnworks',
		method: 'post',
		data: ajaxData,
		async: false,
		complete: function (data) {
			resp = data.responseText;
		}
	});
	console.log(JSON.parse(resp));
	return JSON.parse(resp);
}

