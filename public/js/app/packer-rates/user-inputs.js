$(document).ready(function() {

	var $userName;
	var $userArray = [];
	var $dateFrom;
	var $dateTo;
	var $total = 0;

	function CheckDiffernceBetweenTime(timeA, timeB) {
		var a = moment(timeA, moment.ISO_8601);
		var b = moment(timeB, moment.ISO_8601);

		// console.log('Order Packed: ' + timeA);
		// console.log(b.diff(a, 'minute'));

		if (b.diff(a, 'min') > 300) {
			$total += b.diff(a, 'second');
		} 
	}

	// Get all users that have the role packer when page has loaded
	ReturnAllPackers();
	SQLQuery($customQuery);

	// Create user dropdown items
	$.each($ajaxResults.Results, function( i ) {
		var txt = $ajaxResults.Results[i].EmailAddress;
		var user = txt.split('@');
		var usernameListItem = '<a class="dropdown-item" href="#" name="' + $ajaxResults.Results[i].EmailAddress + '">'+ user[0] +'</a>';
		$('#username-list').append(usernameListItem);
		$userArray.push($ajaxResults.Results[i].EmailAddress);
	});



	// Check if all users are selected
	$(document).on('change', '#cb-all-users input', function() {

		$('#date-from-group').alert()

		if($(this).is(':checked')) {
			$('#dd-usernames button').attr('disabled', true);
		} else {
			$('#dd-usernames button').attr('disabled', false);
		}
	});

	// When a user is selected and to variable and change button html
	$(document).on('click', '#username-list a', function(e) {
		e.preventDefault();
		$('#dd-usernames button').html($(this).html());
		$userName = $(this).attr('name');
		$('#dd-usernames button').val($(this).html());
	});

	// Set date from and date to, set to a variable
	$(document).on('change', '#date-from', function() {
		$dateFrom = $('#date-from').val();
	});
	$(document).on('change', '#date-to', function() {
		$dateTo = $('#date-to').val();
	});

	// Search for packer rates
	$(document).on('click', '#btn-search', function() {

		if($('#date-from').val() !== '' && $('#date-to').val() !== '') {
			if ($('#cb-all-users input').is(':checked')) {
				$.each($userArray, function( i ) {
					CalculatePackerRate($userArray[i], true);
				});
			} else if($('#dd-usernames button').val() !== '') {
				CalculatePackerRate($userName, false);
			} else {
				alert('Please select a user or tick all users!');
			}
		} else {
			alert('Please fill in the date range!');
		}
	});


	function CalculatePackerRate(userName, multi) {

		var j = 0;
		var totalHours = 0;
		var tableStr = '';

		$('#results').attr('hidden', false);

		$total = 0;

		PackerRates($dateFrom, $dateTo, userName);

		var $data = { 'sqlQuery': $customQuery };
		SQLQuery($customQuery);
		
		if($ajaxResults.Results.length > 0) {
			for (i=1; i<$ajaxResults.Results.length; i++) {
				CheckDiffernceBetweenTime($ajaxResults.Results[j].Time, $ajaxResults.Results[i].Time);
				j++
			}
			totalHours = $total / 3600;
		}
		
		if ($ajaxResults.Results.length > 0 && totalHours.toFixed(2) > 0) {
			// Create table string to append to the DOM
			tableStr = '<tr>';
			tableStr += '<td>' + userName + '</td>';
			tableStr += '<td>' + $dateFrom + '</td>';
			tableStr += '<td>' + $dateTo + '</td>';
			tableStr += '<td>' + $ajaxResults.Results.length + '</td>';
			tableStr += '<td>' + totalHours.toFixed(2) + '</td>';
			tableStr += '<td>' + ($ajaxResults.Results.length / totalHours).toFixed(2) + '</td>';

			$('#packer-rate-table tbody').append(tableStr);
			$('#results').removeClass('hide-elements');
		} else if (!multi && $ajaxResults.Results.length === 0) {
			alert('This user has not packed any items within this time frame!');
		}
	}
});