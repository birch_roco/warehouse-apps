
// Variables for SQL custom search
var $sqlSelect;
var $sqlTable;
var $sqlSearchParameter; 
var $sqlJoiningTables;
var $sqlOrderBy;

function CustomQuery($sqlSelect, $sqlTable, $sqlJoinTables, $sqlSearchParams) {
	var $customQuery = "SELECT " + $sqlSelect + " ";
	$customQuery += "FROM " + $sqlTable + " ";
	$customQuery += $sqlJoinTables + " ";
	$customQuery += "WHERE " + $sqlSearchParams;

	return Ajax.SQLQuery($customQuery);
}

var ItemSearch =
{
	BySkuUsingSearchBar: function(itemSearchArray) {
		var sqlSearchParameter = '';
		$.each(itemSearchArray, function(i) { sqlSearchParameter += " AND si.ItemNumber LIKE '%" + itemSearchArray[i] + "%'"; });
		return CustomQuery("si.ItemNumber", "StockItem si", "", "si.bLogicalDelete = 0 AND si.bContainsComposites = 0" + sqlSearchParameter + " ORDER BY si.ItemNumber ASC")
	},
	ByCountMinimum: function() {
		return CustomQuery("si.ItemNumber, sl.Quantity", "StockItem si", "INNER JOIN StockItem_ExtendedProperties siep ON si.pkStockItemID = siep.fkStockItemId INNER JOIN StockLevel sl ON si.pkStockItemID = sl.fkStockItemId", "si.bLogicalDelete = 0 AND si.bContainsComposites = 0 AND siep.ProperyName = 'CountMinLevel' AND (siep.ProperyValue > sl.Quantity) ORDER BY si.ItemNumber ASC");
	}
};

var StockLevel =
{
	DefaultCohen: function() {
		return CustomQuery("JC.SKU, JC.unitCost, JC.QuantityJC, (Def.QuantityDefault - Def.InOrderBook) AS 'AvailableDefault', '' AS Quantity", "(SELECT vsl.ItemNumber AS 'SKU', si.PurchasePrice AS 'unitCost', vsl.Quantity AS 'QuantityJC' FROM View_StockLevel vsl INNER JOIN StockItem si ON vsl.pkStockItemID = si.pkStockItemID INNER JOIN ItemSupplier isup ON vsl.pkStockItemID = isup.fkStockItemID INNER JOIN Supplier sup ON isup.fkSupplierId = sup.pkSupplierID WHERE sup.SupplierName = 'Jonathan Cohen'  AND vsl.Location = 'Jonathan Cohen') JC", "LEFT JOIN (SELECT vsl.ItemNumber AS 'SKU', vsl.Quantity AS 'QuantityDefault', vsllob.InOrderBook AS 'InOrderBook' FROM View_StockLevel vsl INNER JOIN StockItem si ON vsl.pkStockItemID = si.pkStockItemID INNER JOIN ItemSupplier isup ON vsl.pkStockItemID = isup.fkStockItemID INNER JOIN Supplier sup ON isup.fkSupplierId = sup.pkSupplierID INNER JOIN View_StockLevelLessOrderBook vsllob ON si.pkStockItemID = vsllob.pkstockitemid WHERE sup.SupplierName = 'Jonathan Cohen'  AND vsl.Location = 'Default') Def ON Def.SKU = JC.SKU", "(Def.QuantityDefault - Def.InOrderBook) <= 5 AND JC.QuantityJC > 0 ORDER BY JC.SKU ASC");
	}
};

var PurchaseOrders = {
	ByLocation: function(location, status) {
		if(location === 'default') { addtionalInfo = "AND sl.Location = 'default' AND sup.SupplierName <> 'Jonathan Cohen'"; } 
		else if(location === 'jc') { addtionalInfo = "AND sl.Location = 'Jonathan Cohen'"; } 
		else if(location === 'jcToDef') { addtionalInfo = "AND sl.Location = 'default' AND sup.SupplierName = 'Jonathan Cohen'"; }

		return CustomQuery("pur.ExternalInvoiceNumber, pur.SupplierReferenceNumber, CONVERT(varchar, pur.QuotedDeliveryDate, 103) as ExpectedDate, CONVERT(varchar, pur.QuotedDeliveryDate, 127) as ISODate, sup.SupplierName, sl.Location, si.ItemNumber, pi.Quantity, st.Type, sn.Name, sc.Code, scr.Colour, sz.Size, (st.Type + ' - ' + sc.Code + ' - ' + scr.Colour) AS 'GroupName'", "Purchase Pur", "INNER JOIN PurchaseItem pi ON pur.pkPurchaseID = pi.fkPurchasId INNER JOIN StockItem si ON pi.fkStockItemId = si.pkStockItemID INNER JOIN StockLocation sl ON pur.fkLocationId = sl.pkStockLocationId INNER JOIN Supplier sup ON pur.fkSupplierID = sup.pkSupplierID FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyst', siep.ProperyName, siep.ProperyValue AS 'Type' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.ItemType') st ON si.pkStockItemID = st.fkeyst FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysn', siep.ProperyName, siep.ProperyValue AS 'Name' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.name') sn ON si.pkStockItemID = sn.fkeysn FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysc', siep.ProperyName, siep.ProperyValue AS 'Code' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.code') sc ON si.pkStockItemID = sc.fkeysc FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyscr', siep.ProperyName, siep.ProperyValue AS 'Colour' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.colour') scr ON si.pkStockItemID = scr.fkeyscr FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyson', siep.ProperyName, siep.ProperyValue AS 'OrderNo' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.OrderNo') son ON si.pkStockItemID = son.fkeyson FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysz', siep.ProperyName, siep.ProperyValue AS 'Size' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.Size') sz ON si.pkStockItemID = sz.fkeysz", "pur.Status = '" + status + "' " + addtionalInfo + " ORDER BY pur.QuotedDeliveryDate, pur.ExternalInvoiceNumber, sup.SupplierName, st.Type, sc.Code, scr.Colour, son.OrderNo ASC");
	},

	ByItemName: function(itemSearchArray) {
		var sqlSearchParameter = '';
		$.each(itemSearchArray, function(i) {
			sqlSearchParameter += " AND si.ItemNumber LIKE '%" + itemSearchArray[i] + "%'";
		});
		return CustomQuery("si.ItemNumber, pi.Quantity, pur.ExternalInvoiceNumber, sl.Location, sup.SupplierName, CONVERT(varchar, pur.QuotedDeliveryDate, 103) as ExpectedDate", "Purchase Pur", "INNER JOIN PurchaseItem pi ON pur.pkPurchaseID = pi.fkPurchasId INNER JOIN StockItem si ON pi.fkStockItemId = si.pkStockItemID INNER JOIN StockLocation sl ON pur.fkLocationId = sl.pkStockLocationId INNER JOIN Supplier sup ON pur.fkSupplierID = sup.pkSupplierID", "pur.Status = 'OPEN' " + sqlSearchParameter + " ORDER BY sl.Location, pur.QuotedDeliveryDate ASC");
	}
}






// function OpenPurchaseOrdersLocations() {
// 	$sqlSelect = "sl.Location";
// 	$sqlTable = "purchase pur";
// 	$sqlJoiningTables = "INNER JOIN StockLocation sl ON pur.fkLocationId = sl.pkStockLocationId";
// 	$sqlSearchParameter = "pur.Status = 'OPEN' GROUP BY sl.Location ORDER BY sl.Location ASC";

// 	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
// }

// function OpenPurchaseOrdersSuppliers() {
// 	$sqlSelect = "sup.SupplierName";
// 	$sqlTable = "purchase pur";
// 	$sqlJoiningTables = "INNER JOIN Supplier sup ON pur.fkSupplierID = sup.pkSupplierID";
// 	$sqlSearchParameter = "pur.Status = 'OPEN' GROUP BY sup.SupplierName ORDER BY sup.SupplierName ASC";

// 	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
// }

