$(document).ready(function() {
	var $searchResults;
	var expectedTotal;
	var countedTotal;
	var countCompleted;
	var printSheet;
	var searchType;

	$(document).on('click', '#recurringCount', function() {
		$.ajax({
			url: '/stock-count/recurring-counts',
			headers: {'X-CSRF-TOKEN': mytoken}
		});
	});

	// printList();

	//function to call if you want to print the count sheet
	// var onPrintFinished=function(printed, countID){
	// 	$('#item-count-modal').modal('hide');
	// 	StockCount.UpdateStockCount(countID, 'printed');
	// 	localStorage.setItem('ActiveTabID' , 'view');
	// 	location.reload();
	// }

	// Hides searchbar on load
	$('#search-sku, #print-count-sheet').hide();

	// Checks which tab was last open using local storage
	var activeTab = localStorage.getItem('ActiveTabID');

	// Acts the same as clicking the plus if an input is selected and the user hits enter
	$('#item-count-modal').keydown(function(e) {
		if(e.which === 13) {
			e.preventDefault();
			$('.count-qty input').each(function(i){
				if($(this).is(':focus')){
					$(this).parents('tr').find('.add-count').trigger('click');
				}
			});
		}
	});

	if(activeTab === 'view') {
		$('#create-tab, #create').removeClass('active');
		$('#view-tab, #view').addClass('active');
	} else if(activeTab === 'create') {
		$('#view-tab, #view').removeClass('active');
		$('#create-tab, #create').addClass('active');
	}
	localStorage.setItem('ActiveTabID' , 'create');

	// Search table when user changes value in textbox
	$('#count-id-search').on('keyup', function() {
		$('#count-table tbody tr').show();
		$('#count-table tbody tr').each(function(i) {
			if($(this).attr('id') === $('#count-id-search').val()) {
				$('#count-table tbody tr:not(#'+ $(this).attr('id') +')').hide();
				$(this).show();
			}
		});
	});

	// Change complete column on count table to a icon
	$('#count-table .count-status').each(function(i) {
		if($(this).html() === '1') {
			$(this).html('<i class="fal fa-check-circle text-success"></i>');
		} else if($(this).html() === '0') {
			$(this).html('<i class="fal fa-times-circle text-danger"></i>');
		}
	});

	// View the count sheet items when a user clicks the view button
	$('.td-view-btn button').on('click', function() {
		var countId = $(this).parent().parent().attr('id');
		$('#item-count-modal h5').html(countId);

		createItemList(countId);		

		// $('#item-count-modal').modal('show');
	});

	$('.td-print-btn button').on('click', function() {

		var countId = $(this).parent().parent().attr('id');

		$.get( '/stock-count/get-items/' + countId, function(data) {
			nWin($('head').html(), CreatePrintSheet(data));
		});	

		StockCount.UpdateStockCount(countId, 'printed');

		// $('#10047').css('background-color', '#9ad3f5');

		function nWin(head, html) {
			var w = window.open();
			$(w.document.head).html(head);
			$(w.document.body).html(html);

			// $('#count-print-id').barcode(countId, 'code39', {
			// 	barWidth: 2,
			// 	barHeight: 30,
			// 	showHRI: false,
			// 	output: 'css'
			// }); 
		}

		// var countId = $(this).parent().parent().attr('id');
		// StockCount.UpdateStockCount(countId, 'printed');
		
		// $('body *').hide();
		// $('#print-count-sheet, #print-count-sheet *').show();

	});

	// Open a confirm dialog and delete the count sheet and relevent items on confirm
	$('.td-delete-btn button').on('click', function() {
		var deleteCount = confirm('Are you sure that you want to delete this count?');

		if(deleteCount === true) {
			var countId = $(this).parents('tr').attr('id')
			StockCount.DeleteCountItems(countId);
			StockCount.DeleteCount(countId);
			localStorage.setItem('ActiveTabID' , 'view');
			location.reload();
		}
	});

	// Search by dropdown change
	$('#dd-search-by div a').on('click', function(e) {
		e.preventDefault();
		$('#search-results').empty();

		if($(this).index() === 0) {
			$('#search-sku').show();
			searchType = 'searchByName'; 
		} else if($(this).index() === 1) {
			$('#search-sku').hide();
			searchType = 'searchByMinimum';
			var itemsMinLevel = ItemSearch.ByCountMinimum();
			CreateItemSelectionElements(itemsMinLevel.Results, 'minLevels');
		}
	});

	$('#search-sku button').on('click', function() {
		var searchStr = $('#search-sku input').val();
		if(searchStr === '') {
			alert('Please enter a value in the search bar!!');
		} else {
			var searchArray = searchStr.split(' ');
			// SearchForItems(searchArray);
			var itemsSearchBarResults = ItemSearch.BySkuUsingSearchBar(searchArray, 'count');
			// SQLQuery($customQuery);

			if(itemsSearchBarResults.Results.length === 0) {
				alert('No search results!!!');
			} else {
				CreateItemSelectionElements(itemsSearchBarResults.Results, 'searchBar');
			}
		}
	});

	$('#select-all input').on('click', function() {
		if($(this).is(':checked')) {
			$('#search-results .item-container input').attr('checked', true);
		} else {
			$('#search-results .item-container input').attr('checked', false);
		}
	});

	// Move items to the count sheet screen when the add button is clicked
	$('#btn-add-to-count',).on('click', function() {
		$('#search-results .item-container').each(function(i) {
			if($(this).find('input').is(':checked')) {
				var storedElement = $(this)[0].outerHTML;
				$(this).hide();
				$('#count-sheet-items').append(storedElement);
			}
		});
		$('#count-sheet-items input').attr('checked', false);
	});

	// Move items to the item sheet screen when the remove button is clicked
	$('#btn-remove-from-count',).on('click', function() {
		$('#count-sheet-items .item-container').each(function(i) {
			if($(this).find('input').is(':checked')) {
				var divClass = $(this).attr('class');
				var classArray = divClass.split(' ');
				divClass = classArray[2];
				$('.' + divClass).show();
				$(this).remove();
			}
		});
	});

	$('#btn-save').on('click', function() {
		var itemCheck = $('#count-sheet-items').find('.item-container').length;
		var itemArray = [];
		var readyToSave = 0;
		let recurring = 0;
		let recurringInWeeks = 0;

		if (itemCheck === 0 || !$('#count-description').val()) {
			alert('Please check the description has a value and that you have selected some items!!');
		} else if(searchType === 'searchByName') {
			$('#count-sheet-items .item-container').each(function(i) {
				itemArray.push({linnworksID: $(this).attr('linnworksid'), productSKU: $(this).attr('sku'), productCode: $(this).attr('productCode'), itemType: $(this).attr('itemType'), itemCode: $(this).attr('itemCode'), itemColour: $(this).attr('itemColour'), orderNo: $(this).attr('orderNo'), expected: $(this).attr('expected'), lastCounted: $(this).attr('last-counted'), countRowId: $(this).attr('count-row-id')});
			});
			 readyToSave = 1;
		} else if(searchType === 'searchByMinimum') {
			$('#count-sheet-items .item-container p').each(function(i) {
				itemArray.push({linnworksID: $(this).attr('linnworksid'), productSKU: $(this).attr('sku'), productCode: $(this).attr('productCode'), itemType: $(this).attr('itemType'), itemCode: $(this).attr('itemCode'), itemColour: $(this).attr('itemColour'), orderNo: $(this).attr('orderNo'), expected: $(this).attr('expected'), lastCounted: $(this).attr('last-counted'), countRowId: $(this).attr('count-row-id')});
			});
			readyToSave = 1;
		}

		if($('#isRecurring').is(':checked')) {
			recurring = 1;
			if($('#recurringInWeeks').val() === '') {
				alert('Please enter when you would like this to recur!!');
				$('#recurringInWeeks').css('border', '1px solid red');
				readyToSave = 0;
			} else {
				recurringInWeeks = $('#recurringInWeeks').val();
				readyToSave = 1;
			}
		}

		if(readyToSave == 1) {
			StockCount.AddStockCount($('#count-description').val(), recurring, recurringInWeeks, itemArray);
			localStorage.setItem('ActiveTabID' , 'create');
			// location.reload();
		}
	});

	// Allow the user to count the item more than once
	$(document).on('click', '#item-count-modal .count-qty .add-count', function() {
		var countTotal;
		var numberInput = $(this).parent().find('input');
		if(numberInput.val() !== '' && !isNaN(parseInt(numberInput.val()))) {
			$(this).parents('tr').find('.count-tally span').append('<i>' + numberInput.val() + '</i>, ');
			countTotal = parseInt($(this).parents('tr').find('.count-tally .total').html()) + parseInt(numberInput.val());
			numberInput.val('');
			$(this).parent().find('input').focus();
			$(this).parents('tr').find('.count-tally .total').html(countTotal);
		} else {
			alert('Please enter a valid number!');
		}
	});

	// Check to see if any of the quantities have been entered and update them in the database and in Linnworks
	$('#save-changes').on('click', function() {
		var count = 0;
		var totalQty;
		// var inputCount = $('#item-count-modal tbody tr td input').length;
		var updateArray = [];
		var linnworksStockArray = [];
		$('#item-count-modal tbody tr td.count-qty').each(function(i) {
			totalQty = parseInt($(this).parents('tr').find('.count-tally .total').html());

			if($(this).parents('tr').find('.item-tally span i').length === 0) {
				count++;
			} else {
				updateArray.push({StockItemID: $(this).parent().attr('linnworksid'), sku: $(this).parent().find('.linnworks-sku').attr('sku'), itemID: $(this).parent().attr('itemID'), lastChecked: $(this).parent().attr('lastCounted'), countRowID: $(this).parent().attr('countRowId'), countNo: totalQty});
				// updateArray.push({StockItemID: '', sku: '', itemID: $(this).parent().attr('itemID'), lastCounted: $(this).parent().attr('lastCounted'), countNo: totalQty});
			}
		});

		if(updateArray.length > 0) {
			var linnworksRequest = Stock.UpdateLevels(updateArray);
			
			if(linnworksRequest.status === 200) {
				StockCount.UpdateStockItemQty(updateArray);
				Stock.InventoryExtendedProperty('LastCounted', updateArray);	
			} else {
				alert('Your request has failed \nStatus Code: ' + linnworksRequest.status + '\nPlease contact your administrator');
			}
		}

		if(count == 0) {
			StockCount.UpdateStockCount($('#item-count-modal .modal-header h5').html(), 'complete');
			localStorage.setItem('ActiveTabID' , 'view');
			location.reload();
		}

		$('#item-count-modal').modal('hide');
	});

	// Create search result elements
	function CreateItemSelectionElements(itemArray, type) {
		let itemStr = '';
		let previousGroupName = '';
		let currentGroupName = '';

		switch (type) {
			case 'minLevels': 
				$.each(itemArray, function(i) {
					currentGroupName = itemArray[i].GroupByColour;

					if(itemArray[i].Size === 'One Size') {
						currentGroupName = itemArray[i].GroupByCode;
					}

					if(previousGroupName !== currentGroupName) {
						if(i > 0) { itemStr += '</div>'; }
						itemStr += '<div class="form-check item-container item-container-' + i + '">';
						itemStr += '<label class="form-check-label"><input type="checkbox" class="form-check-input"> ';
						if(itemArray[i].Size == 'One Size') { itemStr += ' ' + itemArray[i].GroupByCode; } else if(itemArray[i].Size !== 'One Size') { itemStr += ' ' + itemArray[i].GroupByColour; }
						itemStr += '</label>';
					}

					if(itemArray[i].Type == 'Acc') {
						itemStr += '<p linnworksID="' + itemArray[i].pkStockItemID + '" sku="' + itemArray[i].ItemNumber + '" productCode="' + itemArray[i].ProductCode + '" itemType="' + itemArray[i].Type + '" itemCode="' + itemArray[i].Code + '" itemColour="' + itemArray[i].Colour + '" orderNo="' + itemArray[i].OrderNo + '"  expected="' + itemArray[i].Expected + '" last-counted="' + itemArray[i].LastCountedDate + '" count-row-id="' + itemArray[i].RowID + '" hidden>' + itemArray[i].ItemNumber + '</p>';
					} else {
						itemStr += '<p linnworksID="' + itemArray[i].pkStockItemID + '" sku="' + itemArray[i].ItemNumber + '" productCode="' + itemArray[i].ProductCode + '" itemType="' + itemArray[i].Type + '" itemCode="' + itemArray[i].Code + '" itemColour="' + itemArray[i].Colour + '" orderNo="' + itemArray[i].OrderNo + '"  expected="' + itemArray[i].Expected + '" last-counted="' + itemArray[i].LastCountedDate + '" count-row-id="' + itemArray[i].RowID + '" hidden>' + itemArray[i].ProductCode + '</p>';
					}	

					previousGroupName = itemArray[i].GroupByColour;

					if(itemArray[i].Size === 'One Size') {
						previousGroupName = itemArray[i].GroupByCode;
					}
				});
				itemStr += '</div>';
			break;

			case 'searchBar':
				$.each(itemArray, function(i) {
					itemStr += '<div class="form-check item-container item-container-' + i + '" linnworksID="' + itemArray[i].pkStockItemID + '" sku="' + itemArray[i].ItemNumber + '" productCode="' + itemArray[i].ProductCode + '" itemType="' + itemArray[i].Type + '" itemCode="' + itemArray[i].Code + '" itemColour="' + itemArray[i].Colour + '" orderNo="' + itemArray[i].OrderNo + '"  expected="' + itemArray[i].Expected + '" last-counted="' + itemArray[i].LastCountedDate + '" count-row-id="' + itemArray[i].RowID + '">';
					itemStr += '<label class="form-check-label">';
					itemStr += '<input type="checkbox" class="form-check-input">';
					if(itemArray[i].ProductCode !== '') {
						itemStr += ' ' + itemArray[i].ProductCode;
					} else {
						itemStr += ' ' + itemArray[i].ItemNumber;
					}
					itemStr += '</label>';
					itemStr += '</div>';
				});
			break;
		}
		$('#search-results').html(itemStr);
	}

	function CreatePrintSheet(data) {
		var printStr = '';
		var countId = data[0].count_id.toString();

		printStr += '<div id="count-print-sheet">';
		printStr += '<span id="count-print-id">100024</span>';
		printStr += '<table >';
		printStr += '<thead>';
		printStr += '<tr><th>Item Name</th><th>Count</th></tr>';
		printStr += '</thead>';
		printStr += '<tbody>';

		$.each(data, function(i) {
			if(!data[i].count) {
				printStr += '<tr>';
				printStr += '<td>' + data[i].productCode + '</td>';
				printStr += '<td></td>';
				printStr += '</tr>';
			}
		});
		printStr += '</tbody>';
		printStr += '</table>';
		printStr += '</div>';
		// printStr += '<script src="http://warehouse.app/js/app/stock-count/stock-count.js"></script>';

		return printStr;
	}

	function CreateItemCountModal(data) {
		$('#save-changes').show();

		var itemModalStr = '';
		var counter = data.length;
		expectedTotal = 0;
		countedTotal = 0;
		countCompleted = false;

		$.each(data, function(i) {
			itemModalStr += '<tr class="item-row" linnworksID="' + data[i].linnworksID + '" itemID="' + data[i].id + '" lastCounted="' + data[i].last_count_date + '" countRowId="' + data[i].countRowId + '">';
			itemModalStr += '<td class="linnworks-sku" sku="' + data[i].productSKU + '">' + data[i].productCode + '</td>';
			itemModalStr += '<td class="expected-quantity hidden-lg-down">' + data[i].expected + '</td>';

			expectedTotal += parseInt(data[i].expected);

			if (data[i].count !== null) {
				itemModalStr += '<td class="counted-qty">' + data[i].count + '</td>';
				itemModalStr += '<td class="count-tally">-</td>';

				countedTotal += parseInt(data[i].count);

				counter--;
			} else {
				itemModalStr += '<td class="count-qty"><input type="number" class="form-control do-not-print"><i class="fal fa-plus-square add-count do-not-print"></i></td>';
				itemModalStr += '<td class="count-tally"><p class="total do-not-print">0</p><hr class=" do-not-print"><p class="item-tally do-not-print"><span class=" do-not-print"></span></p></td>';
			}
			if (data[i].userID !== null) {
				itemModalStr += '<td class="count-userId hidden-md-down">' + data[i].userID + '</td>';
			} else {
				itemModalStr += '<td class="count-userId hidden-md-down">-</td>';
			}
			if (data[i].bookedIn !== null) {
				itemModalStr += '<td class="count-booked-date hidden-md-down">' + moment(data[i].bookedIn).format('D/M/YYYY') + '</td>';
			} else {
				itemModalStr += '<td class="count-booked-date hidden-md-down">-</td>';
			}
			itemModalStr += '</tr>';
		});

		if(counter === 0) {
			$('#save-changes').hide();
			itemModalStr += '<tr id="table-totals"><td>Totals:</td>';
			itemModalStr += '<td class="expected-total">' + expectedTotal + '</td>';
			itemModalStr += '<td class="count-total">' + countedTotal + '</td>';
			itemModalStr += '<td class="hidden-md-down total-difference">-</td>';
			itemModalStr += '<td class="hidden-md-down">-</td>';
			itemModalStr += '<td class="hidden-md-down">-</td>';
			itemModalStr += '</tr>';

			countCompleted = true;
		}

		return itemModalStr;
	}

	function createItemList(countID) {
		getItemsComplete = false;

		$.get( '/stock-count/get-items/' + countID, function(data) {
			$('#item-count-modal .modal-body tbody').html(CreateItemCountModal(data));

			if(countCompleted === true) {
				$('#item-count-modal table #tally').html('Diff');
				$('#item-count-modal tbody tr').each(function(i) {
					var expected = parseInt($(this).find('.expected-quantity').html());
					var counted = parseInt($(this).find('.counted-qty').html());
					var difference = counted - expected;
					var totalDifference = countedTotal - expectedTotal;

					$(this).find('.count-tally').html(difference);
					$(this).find('.total-difference').html(totalDifference);

					if(difference > 0) {
						$(this).find('.count-tally').css('color', 'green');
					} else if (difference < 0) {
						$(this).find('.count-tally').css('color', 'red');
					}

					if(totalDifference > 0) {
						$(this).find('.total-difference').css('color', 'green');
					} else if(totalDifference < 0) {
						$(this).find('.total-difference').css('color', 'red');
					}
				});
			} else {
				$('#item-count-modal tbody tr').each(function(i) {
					if($(this).find('input').length === 0) {
						$(this).addClass('do-not-print');
					}
				});
			}

			RefreshExpected();
		}).done(function() {
			$('#item-count-modal').modal('show');
			
			if(printSheet) {
				//print command
				onPrintFinished(window.print(), countID);
			}
		});
	}

	function printList() {
		$.get( '/stock-count/get-items/10041', function(data) {
			var printStr = '';
		// $.get( '/stock-count/get-items/' + countID, function(data) {

			$('#print-header h3').html(data[0].count_id);

			$.each(data, function(i) {
				printStr += '<div class="item-rows">';
				printStr += '<div class="item-col">';
				printStr += data[i].productCode;
				printStr += '</div>';
				printStr += '<div class="tally-col">';
				printStr += '</div>';
				printStr += '<div class="total-col">';
				printStr += '</div>';
				printStr += '</div>';
			});

			$('.table-head').after(printStr);
		});
	}

	// Refreshes the expected if it has not been counted yet
	function RefreshExpected() {
		var itemArrayExpected = [];
		var updateExpectedArray = [];
		$('#item-count-modal tbody tr.item-row').each(function(i) {
			itemArrayExpected.push($(this).attr('linnworksid'));
		});
		
		var expectedUpdate = StockItemInfo.GetLevelsByStockItemID(itemArrayExpected);

		$.each(expectedUpdate.Results, function(i) {
			$('#item-count-modal tbody tr').each(function(j) {
				if(expectedUpdate.Results[i].pkStockItemID === $(this).attr('linnworksid') && $(this).find('.count-qty input').length > 0) {
					$(this).find('.expected-quantity').html(expectedUpdate.Results[i].Expected);
					updateExpectedArray.push({itemID: $(this).attr('itemID'), expected: expectedUpdate.Results[i].Expected});
				}
			});
		});

		if(!countCompleted) {
			StockCount.UpdateExpectedQty(updateExpectedArray);
		}	
	}

	// function CreatePrintSheet(data) {
	// 	var printStr = '';
	// 	var countId = data[0].count_id.toString();

	// 	$('#print-count-sheet #barcode').barcode(countId, 'code39', {
	// 		barWidth: 2,
	// 		barHeight: 30,
	// 		showHRI: false,
	// 		output: 'css'
	// 	}); 
		
	// 	$.each(data, function(i) {
	// 		if(!data[i].count) {
	// 			printStr += '<tr>';
	// 			printStr += '<td>' + data[i].productCode + '</td>';
	// 			printStr += '<td></td>';
	// 			printStr += '</tr>';
	// 		}
	// 	});

	// 	return printStr;
	// }
});