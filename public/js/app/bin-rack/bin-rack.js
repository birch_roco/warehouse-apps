$(document).ready(function() {
	var cardCount;
	var cardIndex;
	var itemType;
	var itemName;
	var currentItemText;
	var newItemText;
	var $updateArray = [];

	// $('#bin-rack-changes').modal('show');	

	// Add new card
	$('#btn-add-new').on('click', function() {
		$('#card-wrapper').append(CreateNewCard());
	});

	// Delete Card
	$(document).on('click', '.delete-card', function() {
		$(this).parents('.card').remove();
	});

	// Set the buttons text tot the value selected in the dropdown
	$(document).on('click', '.dropdown-menu a', function(e) {
		e.preventDefault();
		$(this).parents('.dropdown').find('button').html($(this).html());
	});

	// Set location by radio button
	$(document).on('change', '.locations-form input', function() {
		if($(this).val() === 'default') {
			$(this).parents('.card').attr('product-location', '00000000-0000-0000-0000-000000000000')
		} else if($(this).val() === 'reFill') {
			$(this).parents('.card').attr('product-location', '3cbb5984-d3a8-4a7d-960e-88112125fdab')
		}
	});

	// Set the aisle count depending on which area has been clicked
	$(document).on('click', '.dd-stock-area div a', function() {
		var aisleCount;

		if($(this).html() === 'Green') {
			aisleCount = 30;
		} else if($(this).html() === 'Orange' || $(this).html() === 'Red') {
			aisleCount = 14;
		} else {
			$(this).parents('.card-block').find('.dd-stock-aisle button').html('Aisle No');
			$(this).parents('.card-block').find('.dd-stock-aisle button').attr('disabled', true);
			$(this).parents('.card-block').find('.tb-new-bay-number').attr('disabled', false);
		}

		if($(this).html() !== 'Blue') {
			$(this).parents('.card-block').find('.dd-stock-aisle .dropdown-menu').empty();
			$(this).parents('.card-block').find('.dd-stock-aisle button').attr('disabled', false);
			$(this).parents('.card-block').find('.tb-new-bay-number').attr('disabled', true);

			for (i=1; i<=aisleCount; i++) {
				$(this).parents('.card-block').find('.dd-stock-aisle .dropdown-menu').append('<a class="dropdown-item" name="' + i + '">' + i + '</a>');
			}			
		}
	});

	// Choose the aisle number
	$(document).on('click', '.dd-stock-area div a', function() {
		$(this).parents('.card-block').find('.tb-new-bay-number').attr('disabled', false);		
	});

	// Search for product by name
	$(document).on('focusin', '.tb-item-name', function() {
		currentItemText = $(this).val();
	});
	$(document).on('focusout', '.tb-item-name', function() {
		newItemText = $(this).val();
		cardIndex = $(this).parents('.card').index();

		if(newItemText !== currentItemText) {
			var itemArray = ItemSearch.TypeByName($(this).val());
			$(this).parents('.card').attr('product-name', $(this).val());
			$(this).parents('.card').find('.dd-colour-btn').html('Colour Select');
			$(this).parents('.card').find('.colour-selector').empty();

			if(itemArray.Results.length === 0) {
				alert('No items returned, please try again!');
			} else {
				if(itemArray.Results.length === 1) {
					$(this).parents('.card').attr('product-type', itemArray.Results[0].Type);
					SetColourDropdown(cardIndex, itemArray.Results[0].Type, $(this).val());
				} else {
					$('#clashingModal .modal-body').empty();
					$('#clashingModal .modal-body').append(ClashingModel(itemArray.Results));
					itemName = $(this).val();
				}
			}
		}
	});

	// Choose the item type when there is a clashing item type
	$(document).on('click', '#clashingModal .modal-body button', function() {
		$('.card').eq(cardIndex).attr('product-type', $(this).attr('name'));
		itemType = $(this).attr('name');
		$('#clashingModal').modal('hide');
		SetColourDropdown(cardIndex, itemType, itemName);
	});

	// Search for all sizes and set the drop down list
	$(document).on('click', '.colour-selector a', function() {
		$(this).parents('.card').attr('product-colour', $(this).html());
		$(this).parents('.card').find('.cb-range-selector').attr('disabled', false);
		itemType = $(this).parents('.card').attr('product-type');
		itemName = $(this).parents('.card').attr('product-name');

		var itemSizeArray = ItemSearch.SizeByNameTypeColour(itemType, itemName, $(this).html());
		$('.start-size-selector, .end-size-selector').empty();
		$('.start-size-selector, .end-size-selector').html(SetSizeDropdown(itemSizeArray));

		$(this).parents('.card').find('.dd-start-size-selector').attr('disabled', false);
	});

	$(document).on('click', '#btn-range', function() {
		if(!$(this).hasClass('checked')) {
			$(this).removeClass('btn-danger');
			$(this).addClass('checked btn-success');
			$(this).parents('.card').attr('range', true);
		} else {
			$(this).removeClass('checked btn-success');
			$(this).addClass('btn-danger');
			$(this).parents('.card').attr('range', false);
		}
		$(this).parents('.card').find('.dd-end-size-selector').toggle();
	});

	$(document).on('click', '.start-size-selector a', function() {
		$(this).parents('.card').attr('product-start-size', $(this).html());
	});

	$(document).on('click', '.end-size-selector a', function() {
		$(this).parents('.card').attr('product-end-size', $(this).html());
	});

	// Save the data from the inputs to an arrasy and search for all the products, then display on a modal
	$('#btn-update').on('click', function() {
		var confirmationTableStr = '';
		$updateArray.length = 0;

		$('.card').each(function(i) {
			var newBinRack = $(this).find('.dd-stock-area button').html();
			var productLocation = $(this).attr('product-location');
			if($(this).find('.dd-stock-area button').html() !== 'Blue') {
				newBinRack += ' - Aisle ' + $(this).find('.dd-stock-aisle button').html();
			}
			if($(this).find('.tb-new-bay-number').val() !== '') {
				newBinRack += ' - Bay ' + $(this).find('.tb-new-bay-number').val();
			}	

			var itemArray = ItemSearch.IdSkuByTypeNameColour($(this).attr('product-type'), $(this).attr('product-name'), $(this).attr('product-colour'), $(this).attr('product-location'));

			if($(this).attr('range') === 'true') {
				var startIndex = LoopArrayToReturnIndex(itemArray.Results, $(this).attr('product-start-size'));
				var endIndex = LoopArrayToReturnIndex(itemArray.Results, $(this).attr('product-end-size'));

				for(i=startIndex; i<=endIndex; i++) {
					$updateArray.push({ID: itemArray.Results[i].pkStockItemID, SKU: itemArray.Results[i].ItemNumber, Location: productLocation, CurrentBinRack: itemArray.Results[i].BinRackNumber, NewBinRack: newBinRack});
				}
			} else {
				var itemData = itemArray.Results[LoopArrayToReturnIndex(itemArray.Results, $(this).attr('product-start-size'))];
				$updateArray.push({ID: itemData.pkStockItemID, SKU: itemData.ItemNumber, Location: productLocation, CurrentBinRack: itemData.BinRackNumber, NewBinRack: newBinRack});
			}
		});
		
		$.each($updateArray, function(i) {
			confirmationTableStr += '<tr id="' + $updateArray[i].ID + '" locationID="' + $updateArray[i].Location + '">';
			confirmationTableStr += '<td>' + $updateArray[i].SKU + '</td>';
			if($updateArray[i].Location === '00000000-0000-0000-0000-000000000000') {
				confirmationTableStr += '<td>Default</td>';
			} else {
				confirmationTableStr += '<td>Re-fill</td>';
			}
			confirmationTableStr += '<td>' + $updateArray[i].CurrentBinRack + '</td>';
			confirmationTableStr += '<td>' + $updateArray[i].NewBinRack + '</td>';
			confirmationTableStr += '</tr>';
		});
		$('#bin-rack-changes tbody').empty();
		$('#bin-rack-changes tbody').append(confirmationTableStr);
		$('#bin-rack-changes').modal('show');
	});

	$('#bin-rack-changes #print-btn').on('click', function() {
		$('.printable').printThis({
			debug: false,               // show the iframe for debugging
			importStyle: true,         // import style tags
			// loadCSS: ['Users/johnbirch-evans/OneDrive/Roco%20Clothing/Website/Bin%20Rack%20App/Versions/1.0.1/css/style.css', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ'],  // path to additional css file - us an array [] for multiple
			// removeInline: true,       // remove all inline styles from print elements
			header: null,               // prefix to html
			footer: null,               // postfix to html
			base: true,                // preserve the BASE tag, or accept a string for the URL
			formValues: true,           // preserve input/form values
			canvas: false,              // copy canvas elements (experimental)
			removeScripts: false        // remove script tags from print content
		});
	});

	$('#bin-rack-changes #confirm-btn').on('click', function() {
		var ajaxNewBinArray = [];
		var ajaxUpdateBinArray = [];
		var locationName = '';

		$.each($updateArray, function(i) {
			if($updateArray[i].Location === '00000000-0000-0000-0000-000000000000') {
				locationName = 'Default';
			} else if($updateArray[i].Location === '3cbb5984-d3a8-4a7d-960e-88112125fdab') {
				locationName = 'Re-fill';
			}

			ajaxUpdateBinArray.push({"StockLocationId": $updateArray[i].Location, "LocationName": locationName, "BinRack": $updateArray[i].NewBinRack, "StockItemId": $updateArray[i].ID});
		});

		if(ajaxUpdateBinArray.length > 0) {
			var ajaxStr = JSON.stringify(ajaxUpdateBinArray);
			Stock.BinRack(ajaxStr , 'update');
		}

		$('#bin-rack-changes').modal('hide');	
		location.reload();	
	});

	function CreateNewCard() {
		var newCardHtml;

		newCardHtml = '<div class="card">';
		newCardHtml += '<div class="card-header">';
		newCardHtml += '<div class="row justify-content-between">';
		newCardHtml += '<div class="col-6">';
		newCardHtml += '<form class="locations-form">';
		newCardHtml += '<label><input type="radio" name="rb-location" value="default" /> Default</label>';
		newCardHtml += '<span class="separator"></span>';
		newCardHtml += '<label><input type="radio" name="rb-location" value="reFill" /> Re-Fill</label>';
		newCardHtml += '</form>';
		newCardHtml += '</div>';
		newCardHtml += '<div class="col-1 text-right delete-card">';
		newCardHtml += '<i class="fal fa-times-circle" aria-hidden="true"></i>';
		newCardHtml += '</div>';
		newCardHtml += '</div>';
		newCardHtml += '</div>';
		newCardHtml += '<div class="card-block">';
		newCardHtml += '<div class="row">';
		newCardHtml += '<div class="col col-6 item-name">';
		newCardHtml += '<div class="input-group input-group-lg">';
		newCardHtml += '<input type="text" class="form-control tb-item-name" name="search-name" placeholder="Product Name...">';
		newCardHtml += '</div>';
		newCardHtml += '</div>';
		newCardHtml += '<div class="col-lg-3">';
		newCardHtml += '<div class="dropdown">';
		newCardHtml += '<button class="btn btn-secondary dropdown-toggle dd-colour-btn btn-lg" type="button" name="search-colour" id="dd-colour-selector' + cardCount + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>';
		newCardHtml += 'Colour Select';
		newCardHtml += '</button>';
		newCardHtml += '<div class="dropdown-menu colour-selector" aria-labelledby="dd-colour-selector' + cardCount + '" id="dd-colour-select' + cardCount + '">';
		newCardHtml += '</div>';
		newCardHtml += '</div>';
		newCardHtml += '</div>';
		newCardHtml += '<div class="btn-group col-lg-3" data-toggle="buttons">';
		newCardHtml += '<button type="button" class="btn btn-danger btn-lg" id="btn-range">Range?</button>';
		newCardHtml += '</div>';
		newCardHtml += '</div>';
		newCardHtml += '<div class="row mt-3">';
		newCardHtml += '<div class="col col-3">';
		newCardHtml += '<div class="dropdown m-x-0">';
		newCardHtml += '<button class="btn btn-secondary dropdown-toggle dd-start-size-selector btn-lg" type="button" name="search-size-start" id="dd-start-size-selector' + cardCount + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>';
		newCardHtml += 'Size Select';
		newCardHtml += '</button>';
		newCardHtml += '<div class="dropdown-menu start-size-selector" aria-labelledby="dd-start-size-select' + cardCount + '" id="dd-start-size-select' + cardCount + '">';
		newCardHtml += '</div>';
		newCardHtml += '</div>';
		newCardHtml += '</div>';
		newCardHtml += '<div class="col col-3">';
		newCardHtml += '<div class="dropdown hide-element">';
		newCardHtml += '<button class="btn btn-secondary dropdown-toggle dd-end-size-selector btn-lg" type="button" name="search-size-end" id="dd-end-size-selector' + cardCount + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="display: none;">';
		newCardHtml += 'Size Select';
		newCardHtml += '</button>';
		newCardHtml += '<div class="dropdown-menu end-size-selector" aria-labelledby="dd-end-size-select' + cardCount + '" id="dd-end-size-select' + cardCount + '">';
		newCardHtml += '</div>';
		newCardHtml += '</div>';
		newCardHtml += '</div>';
		newCardHtml += '</div>';
		newCardHtml += '<div class="row mt-3">';
		newCardHtml += '<div class="col col-3">';
		newCardHtml += '<div class="dropdown dd-stock-area">';
		newCardHtml += '<button class="btn btn-secondary dropdown-toggle btn-lg" type="button" id="btn-area' + cardCount + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
		newCardHtml += 'Area';
		newCardHtml += '</button>';
		newCardHtml += '<div class="dropdown-menu" aria-labelledby="btn-area' + cardCount + '">';
		newCardHtml += '<a class="dropdown-item" href="#" name="blue">Blue</a>';
		newCardHtml += '<a class="dropdown-item" href="#" name="green">Green</a>';
		newCardHtml += '<a class="dropdown-item" href="#" name="orange">Orange</a>';
		newCardHtml += '<a class="dropdown-item" href="#" name="red">Red</a>';
		newCardHtml += '</div>';
		newCardHtml += '</div>';
		newCardHtml += '</div>';
		newCardHtml += '<div class="col col-3">';
		newCardHtml += '<div class="dropdown dd-stock-aisle">';
		newCardHtml += '<button class="btn btn-secondary dropdown-toggle btn-lg" type="button" id="btn-aisle' + cardCount + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>';
		newCardHtml += 'Aisle No';
		newCardHtml += '</button>';
		newCardHtml += '<div class="dropdown-menu" aria-labelledby="btn-aisle' + cardCount + '">';
		newCardHtml += '</div>';
		newCardHtml += '</div>';
		newCardHtml += '</div>';
		newCardHtml += '<div class="col col-6 tb-bay-number input-group-lg">';
		newCardHtml += '<input type="text" class="form-control tb-new-bay-number" name="bay-number"" aria-label="Text input with dropdown button" placeholder="Bay No.." disabled>';
		newCardHtml += '</div>';
		newCardHtml += '</div>';
		newCardHtml += '</div>';
		newCardHtml += '</div>';

		return newCardHtml;
	}

	function ClashingModel(itemArray) {
		$('#clashingModal').modal('show');
		var clashinHtml = '<h5>Please select one of the following...</h5>';
		clashinHtml += '<div class="container">';
		
		$.each(itemArray, function( i ) {
			clashinHtml += '<div class="row justify-content-center clash-row"><button type="button" class="btn btn-secondary" name="' + itemArray[i].Type + '">' + itemArray[i].Type + '</button></div>';
		});
		
		clashinHtml += '</div>'

		return clashinHtml;
	}

	function SetColourDropdown(cardIndex, itemType, itemName) {
		var itemColourArray = ItemSearch.ColourByNameType(itemType, itemName);
			
		$.each(itemColourArray.Results, function( i ) {
			$('.card').eq(cardIndex).find('.colour-selector').append('<a class="dropdown-item" name="' + itemColourArray.Results[i].Colour + '">' + itemColourArray.Results[i].Colour + '</a>');
		});
		$('.card').eq(cardIndex).find('.dd-colour-btn').attr('disabled', false);
	}

	function SetSizeDropdown(sizeArray) {
		var sizeStr = '';
		$.each(sizeArray.Results, function( i ) {
			sizeStr += '<a class="dropdown-item" name="' + sizeArray.Results[i].Size + '">' + sizeArray.Results[i].Size + '</a>';
		});
		return sizeStr;
	}

	function LoopArrayToReturnIndex(itemArray, size) {
		var arrayIndex = 0;
		$.each(itemArray, function(i) {
			if(itemArray[i].Size === size) {
				arrayIndex = i;
			}
		});

		return arrayIndex;
	}
});