var mytoken = $('[name=csrf-token]').attr('content');

function AjaxRequest(ajaxData) {
	$.ajax({
		headers: {'X-CSRF-TOKEN': mytoken},
		url: '/api-linnworks',
		method: 'post',
		data: ajaxData,
		async: false,
		complete: function (data) {
			resp = data.responseText;
		}
	});
	return JSON.parse(resp);
}



var Ajax = {
	// Used to send custom queries to Linnworks
	SQLQuery: function(userData) {
		return AjaxRequest({'userInput': 'script', 'userData': userData, 'linnworksURL': 'Dashboards/ExecuteCustomScriptQuery'});
	}
};

var ApiPurchaseOrders = {
	// Search for a purchase order using the pkPurchaseID, Use a custom query to search by poNumber
	Search: function(pkPurchaseID) {
		return AjaxRequest({'userInput': 'pkPurchaseId', 'userData': pkPurchaseID, 'linnworksURL': 'PurchaseOrder/Get_PurchaseOrder'});
	},
	GetNotes: function(pkPurchaseID) {
		return AjaxRequest({'userInput': 'pkPurchaseId', 'userData': pkPurchaseID, 'linnworksURL': 'PurchaseOrder/Get_PurchaseOrderNote'});
	}
};