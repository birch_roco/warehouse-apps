var $clashingItems = false;
var $itemType = [];

// Checks the results for clashing item names and displays the item types via a modal
function CheckForClashingItems() {
	$itemType = [];

	$.each($ajaxResults.Results, function( i ) {
		$.each($ajaxResults.Results, function( j ) {
			if ($ajaxResults.Results[i].Type !== $ajaxResults.Results[j].Type) {
				$itemType.push($ajaxResults.Results[i].Type);
				$itemType.push($ajaxResults.Results[j].Type);
				$clashingItems = true;
			}
		});
	});


	$.unique($itemType);
	if ($clashingItems) {

		var clashinStr;

		$('#clashingModal').modal('show');

		clashinStr = '<div class="container">'
		
		$.each($itemType, function( i ) {
			clashinStr += '<div class="row justify-content-center clash-row"><button type="button" class="btn btn-secondary" name="' + $itemType[i] + '">' + $itemType[i] + '</button></div>';
		});
		
		clashinStr += '</div>'

		$('#clashingModal > div > div > div.modal-body').append(clashinStr);	
	}
}

function SetColourDropdown() {


	$('#clashingModal > div > div > div.modal-body').empty();
	$('#clashingModal > div > div > div.modal-body').html('<h5>Please select one of the following...</h5>');

	CheckForClashingItems();

	if (!$clashingItems) {
		$productType = $ajaxResults.Results[0].Type;
	}

	if (!$clashingItems) {
		$('#dd-colour-select').html('');
		$.each($ajaxResults.Results, function( i ) {
			$('#dd-colour-select').append('<a class="dropdown-item" name="' + $ajaxResults.Results[i].Colour + '">' + $ajaxResults.Results[i].Colour + '</a>');
		});
	}
}

function SetSizeDropdown() {

	$('#dd-size-select').html('');

	$.each($ajaxResults.Results.Results, function( i ) {
		$('#dd-size-select').append('<a class="dropdown-item" name="' + $ajaxResults.Results.Results[i].Size + '">' + $ajaxResults.Results.Results[i].Size + '</a>');
	});
}