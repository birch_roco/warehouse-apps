var $searchBy;
var $productData;

var $productType;
var $productName;
var $productColour;
var $productSize;
/*** Barcode Search ***/

// Choose whether to search by barcode or manually
$(document).on('change', '#search-by-form input', function() {
	$('#search-by-form input').attr('checked', false);
	$(this).attr('checked', 'checked');
	$searchBy = $(this).val();
	$('#location-table tbody').empty();
	$('#location-table').attr('hidden', true);

	if($('#search-button').attr('hidden', false)) {
		$('#search-button').attr('hidden', true);	
	}

	$('#barcode-search input, #product-name').val('');

	if ($(this).val() === 'barcode') {
		$('#manual-search').attr('hidden', true);
		$('#barcode-search').attr('hidden', false);
	} else if ($(this).val() === 'manual') {
		$('#barcode-search').attr('hidden', true);
		$('#manual-search').attr('hidden', false);
	}
});

// When input has been detected, display search button
$(document).on('change', '#barcode-search input', function() {
	if($('#search-button').attr('hidden')) {
		$('#search-button').attr('hidden', false);
	}
});


/*** Manual Search ***/

// Searches for a product by name and returns the colour
$(document).on('focusout', '#product-name', function() {
	if($(this).val() != $productName){
		$productName = $(this).val();
		SearchByName($productName);

		SQLQuery($customQuery);
		SetColourDropdown();
	}
});

$(document).on('click', '#clashingModal > div > div > div.modal-body > div > div > button', function() {
	$productType = $(this).attr('name');

	$('#clashingModal').modal('hide');

		// Waits for the modal to finish transitioning
		// $('#clashingModal').on('hidden.bs.modal', function (e) {
		$clashingItems = false;
  		SearchByNameAndType($productType, $productName);
  		SQLQuery($customQuery);

		SetColourDropdown();
	// });
});

$(document).on('click', '#dd-colour-select a', function(e) {
	e.preventDefault();
	var btnTxt = $(this).html();
	$('#dd-colour button').html(btnTxt);
	$productColour = $(this).html();

	if($('#search-button').attr('hidden')) {
		$('#search-button').attr('hidden', false);
	}
});



/*** Search button ***/

$(document).on('click', '#search-button', function() {
	$('#location-table tbody').empty();

	if($searchBy === 'barcode') {
		var barcode = $('#barcode-search input').val();
		GetLocationByBarcode(barcode);
	} else if($searchBy === 'manual') {
		GetLocationByProductName($productType, $productName, $productColour);
	}

	SQLQuery($customQuery);

	$.each($ajaxResults.Results, function( i ) {
		var tblString = '<tr>';
		tblString += '<td>' + $ajaxResults.Results[i].ItemNumber + '</td>';
		tblString += '<td>' + $ajaxResults.Results[i].defBinRack + '</td>';
		tblString += '<td>' + $ajaxResults.Results[i].rfBinRack + '</td>';
		tblString += '</tr>';
		$('#location-table tbody').append(tblString);

		$('#location-table').attr('hidden', false);
	});

	
});