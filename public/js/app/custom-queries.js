
// Variables for SQL custom search
var $sqlSelect;
var $sqlTable;
var $sqlSearchParameter; 
var $sqlJoiningTables;
var $sqlOrderBy;
var $customQuery;

function CustomQuery($sqlSelect, $sqlTable, $sqlJoinTables, $sqlSearchParams) {
	$customQuery = "SELECT " + $sqlSelect + " ";
	$customQuery += "FROM " + $sqlTable + " ";
	$customQuery += $sqlJoinTables + " ";
	$customQuery += "WHERE " + $sqlSearchParams;
}

function CustomQuery2($sqlSelect, $sqlTable, $sqlJoinTables, $sqlSearchParams) {
	$customQuery = "SELECT " + $sqlSelect + " ";
	$customQuery += "FROM " + $sqlTable + " ";
	$customQuery += $sqlJoinTables + " ";
	$customQuery += "WHERE " + $sqlSearchParams;

	return Ajax.SQLQuery($customQuery);
}

function BarcodesPOSearch(poNumber) {
	$sqlSelect = "si.BarcodeNumber, sup.SupplierName, itsu.SupplierCode, st.Type, sn.Name, sc.Code, scr.Colour, sz.Size, pi.Delivered, pi.Quantity - pi.Delivered AS 'Due', '' AS 'DeliveredNew', '' AS 'BookedIn'";
    $sqlTable = "Purchase pur";
    $sqlJoiningTables = "INNER JOIN PurchaseItem pi ON pur.pkPurchaseID = pi.fkPurchasId INNER JOIN StockItem si ON pi.fkStockItemId = si.pkStockItemID INNER JOIN ItemSupplier itsu ON pi.fkStockItemId = itsu.fkStockItemId INNER JOIN Supplier sup ON pur.fkSupplierId = sup.pkSupplierID FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyst', siep.ProperyName, siep.ProperyValue AS 'Type' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.ItemType') st ON si.pkStockItemID = st.fkeyst FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysn', siep.ProperyName, siep.ProperyValue AS 'Name' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.name') sn ON si.pkStockItemID = sn.fkeysn FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysc', siep.ProperyName, siep.ProperyValue AS 'Code' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.code') sc ON si.pkStockItemID = sc.fkeysc FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyscr', siep.ProperyName, siep.ProperyValue AS 'Colour' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.colour') scr ON si.pkStockItemID = scr.fkeyscr FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyson', siep.ProperyName, siep.ProperyValue AS 'OrderNo' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.OrderNo') son ON si.pkStockItemID = son.fkeyson FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysz', siep.ProperyName, siep.ProperyValue AS 'Size' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.Size') sz ON si.pkStockItemID = sz.fkeysz ";
    $sqlSearchParameter = " itsu.isDefault = 1 AND pur.ExternalInvoiceNumber = '" + poNumber + "' ORDER BY st.Type, sc.Code, scr.Colour, son.OrderNo";

    CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
}

function ReferenceData(barcodeNumber) {
	$sqlSelect = "st.Type + ' - ' + sn.Name + ' - ' + scr.Colour + ' - ' + sz.Size AS 'ReferenceName'";
	$sqlTable = "StockItem si";
	$sqlJoiningTables = "FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyst', siep.ProperyName, siep.ProperyValue AS 'Type' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.ItemType') st ON si.pkStockItemID = st.fkeyst FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysn', siep.ProperyName, siep.ProperyValue AS 'Name' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.name') sn ON si.pkStockItemID = sn.fkeysn FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysc', siep.ProperyName, siep.ProperyValue AS 'Code' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.code') sc ON si.pkStockItemID = sc.fkeysc FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyscr', siep.ProperyName, siep.ProperyValue AS 'Colour' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.colour') scr ON si.pkStockItemID = scr.fkeyscr FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyson', siep.ProperyName, siep.ProperyValue AS 'OrderNo' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.OrderNo') son ON si.pkStockItemID = son.fkeyson FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysz', siep.ProperyName, siep.ProperyValue AS 'Size' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.Size') sz ON si.pkStockItemID = sz.fkeysz";
	$sqlSearchParameter = "si.bLogicalDelete = 0 AND si.bContainsComposites = 0 AND si.BarcodeNumber = '" + barcodeNumber + "'";

    CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
}

function GetLocationID(locationName) {
	$sqlSelect 	= "pkStockLocationId, Location";
	$sqlTable 	= "StockLocation";
	$sqlJoiningTables = "";
	$sqlSearchParameter = "Location = '" + locationName + "'";

	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
}

function SearchByName(productName) {
	$sqlSelect = "st.Type, sn.Name, scr.Colour";
	$sqlTable = "StockItem si";
	$sqlJoiningTables = "FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyst', siep.ProperyName, siep.ProperyValue AS 'Type' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.ItemType') st ON si.pkStockItemID = st.fkeyst FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysn', siep.ProperyName, siep.ProperyValue AS 'Name' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.name') sn ON si.pkStockItemID = sn.fkeysn FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysc', siep.ProperyName, siep.ProperyValue AS 'Code' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.code') sc ON si.pkStockItemID = sc.fkeysc FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyscr', siep.ProperyName, siep.ProperyValue AS 'Colour' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.colour') scr ON si.pkStockItemID = scr.fkeyscr FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyson', siep.ProperyName, siep.ProperyValue AS 'OrderNo' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.OrderNo') son ON si.pkStockItemID = son.fkeyson FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysz', siep.ProperyName, siep.ProperyValue AS 'Size' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.Size') sz ON si.pkStockItemID = sz.fkeysz";
	$sqlSearchParameter = "si.bLogicalDelete = 0 AND si.bContainsComposites = 0 AND sn.Name = '" + productName + "' GROUP BY st.Type, sn.Name, scr.Colour ORDER BY st.Type, scr.Colour ASC";

	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
}

function SearchByNameAndType(productType, productName) {
	$sqlSelect = "st.Type, sn.Name, scr.Colour";
	$sqlTable = "StockItem si";
	$sqlJoiningTables = "FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyst', siep.ProperyName, siep.ProperyValue AS 'Type' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.ItemType') st ON si.pkStockItemID = st.fkeyst FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysn', siep.ProperyName, siep.ProperyValue AS 'Name' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.name') sn ON si.pkStockItemID = sn.fkeysn FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysc', siep.ProperyName, siep.ProperyValue AS 'Code' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.code') sc ON si.pkStockItemID = sc.fkeysc FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyscr', siep.ProperyName, siep.ProperyValue AS 'Colour' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.colour') scr ON si.pkStockItemID = scr.fkeyscr FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyson', siep.ProperyName, siep.ProperyValue AS 'OrderNo' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.OrderNo') son ON si.pkStockItemID = son.fkeyson FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysz', siep.ProperyName, siep.ProperyValue AS 'Size' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.Size') sz ON si.pkStockItemID = sz.fkeysz";
	$sqlSearchParameter = "si.bLogicalDelete = 0 AND si.bContainsComposites = 0 AND st.Type = '" + productType + "'  AND sn.Name = '" + $productName + "' GROUP BY st.Type, sn.Name, scr.Colour ORDER BY st.Type, scr.Colour ASC";

	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
}

function SearchByColour(productType, productName, productColour, locationID) {
	$sqlSelect = "si.pkStockItemID, si.ItemNumber, sz.Size, bin.BinRack";
	$sqlTable = "StockItem si";
	$sqlJoiningTables = "FULL OUTER JOIN (SELECT sl.fkStockItemId AS 'fkbin', sl.BinRackNumber AS 'BinRack' FROM ItemLocation sl WHERE sl.fkLocationId = '" + locationID + "') bin ON si.pkStockItemID = bin.fkbin FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyst', siep.ProperyName, siep.ProperyValue AS 'Type' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.ItemType') st ON si.pkStockItemID = st.fkeyst FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysn', siep.ProperyName, siep.ProperyValue AS 'Name' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.name') sn ON si.pkStockItemID = sn.fkeysn FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysc', siep.ProperyName, siep.ProperyValue AS 'Code' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.code') sc ON si.pkStockItemID = sc.fkeysc FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyscr', siep.ProperyName, siep.ProperyValue AS 'Colour' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.colour') scr ON si.pkStockItemID = scr.fkeyscr FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyson', siep.ProperyName, siep.ProperyValue AS 'OrderNo' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.OrderNo') son ON si.pkStockItemID = son.fkeyson FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysz', siep.ProperyName, siep.ProperyValue AS 'Size' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.Size') sz ON si.pkStockItemID = sz.fkeysz";
	$sqlSearchParameter = "si.bLogicalDelete = 0 AND si.bContainsComposites = 0 AND st.Type = '" + productType + "' AND sn.Name = '" + productName + "' AND scr.Colour = '" + productColour + "' ORDER BY son.OrderNo ASC";

	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
}

function GetLocationByBarcode(barcodeNumber) {
	$sqlSelect = "si.ItemNumber, si.BarcodeNumber, def.defBinRack, rf.rfBinRack";
	$sqlTable = "StockItem si";
	$sqlJoiningTables = "FULL OUTER JOIN (SELECT si.pkStockItemID AS defStockID, il.BinRackNumber AS defBinRack FROM StockItem si INNER JOIN ItemLocation il ON si.pkStockItemID = il.fkStockItemId WHERE fkLocationId = '00000000-0000-0000-0000-000000000000') def ON si.pkStockItemID = def.defStockID FULL OUTER JOIN (SELECT si.pkStockItemID AS rfStockID, il.BinRackNumber AS rfBinRack FROM StockItem si INNER JOIN ItemLocation il ON si.pkStockItemID = il.fkStockItemId WHERE fkLocationId = '3cbb5984-d3a8-4a7d-960e-88112125fdab') rf ON si.pkStockItemID = rf.rfStockID";
	$sqlSearchParameter = "si.bLogicalDelete = 0 AND si.bContainsComposites = 0 AND si.BarcodeNumber = '" + barcodeNumber + "'";

	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
}

function GetLocationByProductName(type, name, colour) {
	$sqlSelect = "si.ItemNumber, def.defBinRack, rf.rfBinRack";
	$sqlTable = "StockItem si";
	$sqlJoiningTables = "FULL OUTER JOIN (SELECT si.pkStockItemID AS defStockID, il.BinRackNumber AS defBinRack FROM StockItem si INNER JOIN ItemLocation il ON si.pkStockItemID = il.fkStockItemId WHERE fkLocationId = '00000000-0000-0000-0000-000000000000') def ON si.pkStockItemID = def.defStockID FULL OUTER JOIN (SELECT si.pkStockItemID AS rfStockID, il.BinRackNumber AS rfBinRack FROM StockItem si INNER JOIN ItemLocation il ON si.pkStockItemID = il.fkStockItemId WHERE fkLocationId = '3cbb5984-d3a8-4a7d-960e-88112125fdab') rf ON si.pkStockItemID = rf.rfStockID FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyst', siep.ProperyName, siep.ProperyValue AS 'Type' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.ItemType') st ON si.pkStockItemID = st.fkeyst FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysn', siep.ProperyName, siep.ProperyValue AS 'Name' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.name') sn ON si.pkStockItemID = sn.fkeysn FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysc', siep.ProperyName, siep.ProperyValue AS 'Code' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.code') sc ON si.pkStockItemID = sc.fkeysc FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyscr', siep.ProperyName, siep.ProperyValue AS 'Colour' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.colour') scr ON si.pkStockItemID = scr.fkeyscr FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyson', siep.ProperyName, siep.ProperyValue AS 'OrderNo' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.OrderNo') son ON si.pkStockItemID = son.fkeyson FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysz', siep.ProperyName, siep.ProperyValue AS 'Size' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.Size') sz ON si.pkStockItemID = sz.fkeysz";
	$sqlSearchParameter = "si.bLogicalDelete = 0 AND si.bContainsComposites = 0 AND st.Type = '" + type + "' AND sn.Name = '" + name + "' AND scr.Colour = '" + colour + "' ORDER BY son.OrderNo ASC";

	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
}

function GetProductNameFromId(productID) {
	$sqlSelect = "ItemNumber";
	$sqlTable = "StockItem";
	$sqlJoiningTables = "";
	$sqlSearchParameter = "pkStockItemID = '" + productID + "'";

	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
}

function ReturnAllLinnworksUsers() {
	$sqlSelect = "UserName";
	$sqlTable = "Users";
	$sqlJoiningTables = "";
	$sqlSearchParameter = "UserName LIKE '%@%' ORDER BY UserName ASC";

	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
}

function ReturnAllPackers() {
	$sqlSelect = "EmailAddress";
	$sqlTable = "system.permissions_audit";
	$sqlJoiningTables = "";
	$sqlSearchParameter = "GroupName = 'Packer' AND (EmailAddress LIKE '%rococlothing.co.uk%' OR EmailAddress LIKE '%gmail%')";
	$sqlSearchParameter += " GROUP BY EmailAddress";
	
	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
}

function PackerRates(startDate, endDate, userName) {
	$sqlSelect = "olh.UpdatedBy, CONVERT(VARCHAR(19),olh.DateStamp, 126) AS Time";
	$sqlTable = "Order_LifeHistory olh";
	$sqlJoiningTables = "";
	$sqlSearchParameter = "olh.fkOrderHistoryTypeId = 'ORDER_PROCESSED' AND olh.DateStamp BETWEEN '" + startDate + " 00:00:01' AND '" + endDate + " 23:59:59' AND olh.UpdatedBy = '" + userName + "' ORDER BY olh.DateStamp ASC";

	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
}

function OpenPurchaseOrders() {
	$sqlSelect = "pur.pkPurchaseID, sl.Location, sup.SupplierName, CONVERT(varchar, pur.QuotedDeliveryDate, 103) as ExpectedDate, CONVERT(varchar, pur.QuotedDeliveryDate, 127) as ISODate, pur.SupplierReferenceNumber, pur.ExternalInvoiceNumber";
	$sqlTable = "purchase pur";
	$sqlJoiningTables = "INNER JOIN Supplier sup ON pur.fkSupplierID = sup.pkSupplierID INNER JOIN StockLocation sl ON pur.fkLocationId = sl.pkStockLocationId";
	$sqlSearchParameter = "pur.Status = 'OPEN' ORDER BY sl.Location, pur.QuotedDeliveryDate ASC";
	
	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
}

function OpenPurchaseOrdersByLocation(location) {
	if(location === 'default') {
		addtionalInfo = "AND sl.Location = 'default' AND sup.SupplierName <> 'Jonathan Cohen'";
	} else if(location === 'jc') {
		addtionalInfo = "AND sl.Location = 'Jonathan Cohen'";
	} else if(location === 'jcToDef') {
		addtionalInfo = "AND sl.Location = 'default' AND sup.SupplierName = 'Jonathan Cohen'";
	}
	$sqlSelect = "pur.ExternalInvoiceNumber, pur.SupplierReferenceNumber, CONVERT(varchar, pur.QuotedDeliveryDate, 103) as ExpectedDate, CONVERT(varchar, pur.QuotedDeliveryDate, 127) as ISODate, sup.SupplierName, sl.Location, si.ItemNumber, pi.Quantity, st.Type, sn.Name, sc.Code, scr.Colour, sz.Size, (st.Type + ' - ' + sc.Code + ' - ' + scr.Colour) AS 'GroupName'";
	$sqlTable = "Purchase Pur";
	$sqlJoiningTables = "INNER JOIN PurchaseItem pi ON pur.pkPurchaseID = pi.fkPurchasId INNER JOIN StockItem si ON pi.fkStockItemId = si.pkStockItemID INNER JOIN StockLocation sl ON pur.fkLocationId = sl.pkStockLocationId INNER JOIN Supplier sup ON pur.fkSupplierID = sup.pkSupplierID FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyst', siep.ProperyName, siep.ProperyValue AS 'Type' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.ItemType') st ON si.pkStockItemID = st.fkeyst FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysn', siep.ProperyName, siep.ProperyValue AS 'Name' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.name') sn ON si.pkStockItemID = sn.fkeysn FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysc', siep.ProperyName, siep.ProperyValue AS 'Code' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.code') sc ON si.pkStockItemID = sc.fkeysc FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyscr', siep.ProperyName, siep.ProperyValue AS 'Colour' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.colour') scr ON si.pkStockItemID = scr.fkeyscr FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyson', siep.ProperyName, siep.ProperyValue AS 'OrderNo' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.OrderNo') son ON si.pkStockItemID = son.fkeyson FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysz', siep.ProperyName, siep.ProperyValue AS 'Size' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.Size') sz ON si.pkStockItemID = sz.fkeysz";
	$sqlSearchParameter = "pur.Status = 'OPEN' " + addtionalInfo + " ORDER BY pur.QuotedDeliveryDate, pur.ExternalInvoiceNumber, sup.SupplierName, st.Type, sc.Code, scr.Colour, son.OrderNo ASC";

	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
}

function SkuQuantityByPONumber(poNumber) {
	$sqlSelect = "si.ItemNumber, pi.Quantity, st.Type, sn.Name, sc.Code, scr.Colour, sz.Size, (st.Type + ' - ' + sc.Code + ' - ' + scr.Colour) AS 'GroupName'";
	$sqlTable = "Purchase Pur";
	$sqlJoiningTables = "INNER JOIN PurchaseItem pi ON pur.pkPurchaseID = pi.fkPurchasId INNER JOIN StockItem si ON pi.fkStockItemId = si.pkStockItemID FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyst', siep.ProperyName, siep.ProperyValue AS 'Type' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.ItemType') st ON si.pkStockItemID = st.fkeyst FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysn', siep.ProperyName, siep.ProperyValue AS 'Name' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.name') sn ON si.pkStockItemID = sn.fkeysn FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysc', siep.ProperyName, siep.ProperyValue AS 'Code' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.code') sc ON si.pkStockItemID = sc.fkeysc FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyscr', siep.ProperyName, siep.ProperyValue AS 'Colour' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.colour') scr ON si.pkStockItemID = scr.fkeyscr FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeyson', siep.ProperyName, siep.ProperyValue AS 'OrderNo' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.OrderNo') son ON si.pkStockItemID = son.fkeyson FULL OUTER JOIN (SELECT siep.fkStockItemId AS 'fkeysz', siep.ProperyName, siep.ProperyValue AS 'Size' FROM StockItem_ExtendedProperties siep WHERE siep.ProperyName = 'Search.Size') sz ON si.pkStockItemID = sz.fkeysz";
	$sqlSearchParameter = "pur.ExternalInvoiceNumber = '" + poNumber + "' ORDER BY st.Type, sc.Code, scr.Colour, son.OrderNo";

	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
}

function PurchaseOrderByItemName(itemSearchArray) {
	$sqlSelect = "si.ItemNumber, pi.Quantity, pur.ExternalInvoiceNumber, sl.Location, sup.SupplierName, CONVERT(varchar, pur.QuotedDeliveryDate, 103) as ExpectedDate";
	$sqlTable = "Purchase Pur";
	$sqlJoiningTables = "INNER JOIN PurchaseItem pi ON pur.pkPurchaseID = pi.fkPurchasId INNER JOIN StockItem si ON pi.fkStockItemId = si.pkStockItemID INNER JOIN StockLocation sl ON pur.fkLocationId = sl.pkStockLocationId INNER JOIN Supplier sup ON pur.fkSupplierID = sup.pkSupplierID";
	$sqlSearchParameter = "pur.Status = 'OPEN'";

	$.each(itemSearchArray, function(i) {
		$sqlSearchParameter += " AND si.ItemNumber LIKE '%" + itemSearchArray[i] + "%'";
	});

	$sqlSearchParameter += " ORDER BY sl.Location, pur.QuotedDeliveryDate ASC";

	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
}

function SearchForItems(itemSearchArray) {


	$.each(itemSearchArray, function(i) {
		$sqlSearchParameter += " AND si.ItemNumber LIKE '%" + itemSearchArray[i] + "%'";
	});

	$sqlSearchParameter += " ORDER BY si.ItemNumber ASC";

	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
}

var ItemSearch =
{
	BySkuUsingSearchBar: function(itemSearchArray) {
		var sqlSearchParameter = '';
		$.each(itemSearchArray, function(i) { sqlSearchParameter += " AND si.ItemNumber LIKE '%" + itemSearchArray[i] + "%'"; });
		return CustomQuery2("si.pkStockItemID, si.ItemNumber", "StockItem si", "", "si.bLogicalDelete = 0 AND si.bContainsComposites = 0" + sqlSearchParameter + " ORDER BY si.ItemNumber ASC")
	},
	ByCountMinimum: function() {
		return CustomQuery2("si.pkStockItemID, si.ItemNumber, sl.Quantity", "StockItem si", "INNER JOIN StockItem_ExtendedProperties siep ON si.pkStockItemID = siep.fkStockItemId INNER JOIN StockLevel sl ON si.pkStockItemID = sl.fkStockItemId", "si.bLogicalDelete = 0 AND si.bContainsComposites = 0 AND siep.ProperyName = 'CountMinLevel' AND (siep.ProperyValue > sl.Quantity) ORDER BY si.ItemNumber ASC");
	}
};

var StockLevel =
{
	DefaultCohen: function() {
		return CustomQuery2(
			"JC.SKU, JC.unitCost, JC.QuantityJC, (Def.QuantityDefault - Def.InOrderBook) AS 'AvailableDefault', '' AS Quantity",
			"(SELECT vsl.ItemNumber AS 'SKU', si.PurchasePrice AS 'unitCost', vsl.Quantity AS 'QuantityJC' FROM View_StockLevel vsl INNER JOIN StockItem si ON vsl.pkStockItemID = si.pkStockItemID INNER JOIN ItemSupplier isup ON vsl.pkStockItemID = isup.fkStockItemID INNER JOIN Supplier sup ON isup.fkSupplierId = sup.pkSupplierID WHERE sup.SupplierName = 'Jonathan Cohen'  AND vsl.Location = 'Jonathan Cohen') JC",
			"LEFT JOIN (SELECT vsl.ItemNumber AS 'SKU', vsl.Quantity AS 'QuantityDefault', vsllob.InOrderBook AS 'InOrderBook' FROM View_StockLevel vsl INNER JOIN StockItem si ON vsl.pkStockItemID = si.pkStockItemID INNER JOIN ItemSupplier isup ON vsl.pkStockItemID = isup.fkStockItemID INNER JOIN Supplier sup ON isup.fkSupplierId = sup.pkSupplierID INNER JOIN View_StockLevelLessOrderBook vsllob ON si.pkStockItemID = vsllob.pkstockitemid WHERE sup.SupplierName = 'Jonathan Cohen'  AND vsl.Location = 'Default') Def ON Def.SKU = JC.SKU",
			"(Def.QuantityDefault - Def.InOrderBook) <= 5 AND JC.QuantityJC > 0 ORDER BY JC.SKU ASC");
	}
};






// function OpenPurchaseOrdersLocations() {
// 	$sqlSelect = "sl.Location";
// 	$sqlTable = "purchase pur";
// 	$sqlJoiningTables = "INNER JOIN StockLocation sl ON pur.fkLocationId = sl.pkStockLocationId";
// 	$sqlSearchParameter = "pur.Status = 'OPEN' GROUP BY sl.Location ORDER BY sl.Location ASC";

// 	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
// }

// function OpenPurchaseOrdersSuppliers() {
// 	$sqlSelect = "sup.SupplierName";
// 	$sqlTable = "purchase pur";
// 	$sqlJoiningTables = "INNER JOIN Supplier sup ON pur.fkSupplierID = sup.pkSupplierID";
// 	$sqlSearchParameter = "pur.Status = 'OPEN' GROUP BY sup.SupplierName ORDER BY sup.SupplierName ASC";

// 	CustomQuery($sqlSelect, $sqlTable, $sqlJoiningTables, $sqlSearchParameter);
// }

