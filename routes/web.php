<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Auth::routes();

Route::get('/stock-count/recurring-counts', 'CountController@recurring');

Route::group(['middleware' => 'auth'], function () {
    // Only authenticated users may enter...
    Route::get('home', 'HomeController@index')->name('Dashboard');
    Route::get('/barcode-generator', 'WebController@barcodeGen')->name('Barcode Generator');
    Route::get('/barcode-generator-new', 'WebController@barcodeGenNew')->name('Barcode Generator');
    Route::get('/stock-location', 'WebController@stockLocation')->name('Stock Location');
    Route::get('/bin-rack-change', 'WebController@binRacks')->name('Change Bin Rack');
    Route::get('/packer-rates', 'WebController@packRates')->name('Packer Rates');

    Route::get('/stock-count', 'WebController@stockCount')->name('Stock Count');
    Route::post('/stock-count/create', 'CountController@store');
    Route::post('/stock-count/update-count', 'CountController@updateCount');
    Route::post('/stock-count/update-items', 'CountController@updateItems');
    Route::post('/stock-count/delete-count', 'CountController@destroy');
    Route::post('/stock-count/delete-items', 'CountController@destroy');
    Route::get('/stock-count/get-items/{id}', 'CountController@show');

    Route::get('/measurements', 'WebController@measurements')->name('Measurements');
    Route::post('/measurements/create', 'MeasurementsController@store');
    Route::post('/measurements/update-group', 'MeasurementsController@updateGroup');
    Route::post('/measurements/update-items', 'MeasurementsController@updateItems');
    Route::post('/measurements/delete-group', 'MeasurementsController@destroy');
    Route::post('/measurements/delete-items', 'MeasurementsController@destroy');
    Route::get('/measurements/get-items/{id}', 'MeasurementsController@show');
    
    Route::get('/purchase-orders', 'WebController@purchaseOrders')->name('Purchase Orders');
    Route::get('/cohen-stock-check', 'WebController@cohenStockCheck')->name('Cohen Stock Check');

    // Route::post('/notes', 'NotesController@store');

    Route::post('/custom-query', 'WebController@customQuery');
    Route::post('/api-linnworks', 'WebController@apiLinnworks');
    Route::post('/api-linnworks-request', 'WebController@apiLinnworksRequest');
    // Route::get('stock', 'AppsController@index')->name('stock');
});


